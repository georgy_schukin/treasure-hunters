/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package ru.ssd.probing.client;

public final class R {
    public static final class anim {
        public static final int sign_in_button=0x7f040000;
    }
    public static final class array {
        public static final int radius_list_entries=0x7f060001;
        public static final int radius_list_values=0x7f060000;
    }
    public static final class attr {
    }
    public static final class drawable {
        public static final int chest_icon=0x7f020000;
        public static final int flag_icon=0x7f020001;
        public static final int flag_leader_icon=0x7f020002;
        public static final int google=0x7f020003;
        public static final int gui_button_background=0x7f020004;
        public static final int gui_button_background_normal=0x7f020005;
        public static final int gui_button_background_pressed=0x7f020006;
        public static final int gui_change_radius_icon=0x7f020007;
        public static final int gui_chat_icon=0x7f020008;
        public static final int gui_exit_icon=0x7f020009;
        public static final int gui_leave_group_icon=0x7f02000a;
        public static final int gui_lfg_icon=0x7f02000b;
        public static final int gui_put_flag_icon=0x7f02000c;
        public static final int gui_put_treasure_icon=0x7f02000d;
        public static final int gui_scan_icon=0x7f02000e;
        public static final int gui_start_group_icon=0x7f02000f;
        public static final int gui_zoom_in_icon=0x7f020010;
        public static final int gui_zoom_out_icon=0x7f020011;
        public static final int icon=0x7f020012;
        public static final int player_icon=0x7f020013;
        public static final int player_leader_icon=0x7f020014;
        public static final int sign_in_button=0x7f020015;
        public static final int signal_icon_1=0x7f020016;
        public static final int signal_icon_2=0x7f020017;
        public static final int signal_icon_3=0x7f020018;
        public static final int signal_icon_4=0x7f020019;
        public static final int treasure=0x7f02001a;
        public static final int treasure_chest_opened_icon=0x7f02001b;
        public static final int treasure_map_icon=0x7f02001c;
        public static final int treasure_small=0x7f02001d;
    }
    public static final class id {
        public static final int chat_clear=0x7f090006;
        public static final int chat_edit=0x7f090004;
        public static final int chat_item_message=0x7f090008;
        public static final int chat_list=0x7f090007;
        public static final int chat_panel=0x7f090003;
        public static final int chat_send=0x7f090005;
        public static final int gplus_profile=0x7f090000;
        public static final int gui_change_radius=0x7f090014;
        public static final int gui_chat=0x7f090011;
        public static final int gui_exit=0x7f090015;
        public static final int gui_leave_group=0x7f090010;
        public static final int gui_lfg=0x7f09000c;
        public static final int gui_put_flag=0x7f09000f;
        public static final int gui_put_treasure=0x7f09000e;
        public static final int gui_scan=0x7f09000d;
        public static final int gui_start_group=0x7f09000b;
        public static final int gui_zoom_in=0x7f090012;
        public static final int gui_zoom_out=0x7f090013;
        public static final int maplayout=0x7f090009;
        public static final int mapview=0x7f09000a;
        public static final int menu_item_group_settings=0x7f090017;
        public static final int menu_item_settings=0x7f090016;
        public static final int sign_in=0x7f090002;
        public static final int sign_in_background=0x7f090001;
    }
    public static final class layout {
        public static final int authorization=0x7f030000;
        public static final int chat=0x7f030001;
        public static final int chat_elem=0x7f030002;
        public static final int map=0x7f030003;
        public static final int stub=0x7f030004;
    }
    public static final class menu {
        public static final int menu=0x7f080000;
    }
    public static final class string {
        public static final int app_name=0x7f070000;
        public static final int chat_clear_button_title=0x7f07003a;
        public static final int chat_send_button_title=0x7f070039;
        public static final int dlg_cancel=0x7f070025;
        public static final int dlg_change_leader_message=0x7f070031;
        public static final int dlg_change_leader_title=0x7f070030;
        public static final int dlg_flag_remove_message=0x7f070029;
        public static final int dlg_flag_remove_title=0x7f070028;
        public static final int dlg_group_invitation_message=0x7f07002d;
        public static final int dlg_group_invitation_title=0x7f07002c;
        public static final int dlg_leave_group_message=0x7f07002f;
        public static final int dlg_leave_group_title=0x7f07002e;
        /** string name="menu_item_put_flag_title">Put flag</string>
    <string name="menu_item_put_treasure_title">Put treasure</string>
    <string name="menu_item_start_group_title">Start group</string>
    <string name="menu_item_leave_group_title">Leave group</string>
    <string name="menu_item_start_scan_title">Scan</string>
    <string name="menu_item_change_leader_title">Change leader</string
 Dialog strings 
         */
        public static final int dlg_ok=0x7f070024;
        public static final int dlg_scan_message=0x7f07002b;
        public static final int dlg_scan_title=0x7f07002a;
        public static final int dlg_treasure_found_message=0x7f070027;
        public static final int dlg_treasure_found_title=0x7f070026;
        public static final int group_settings_title=0x7f070002;
        public static final int menu_item_group_settings_title=0x7f070023;
        public static final int menu_item_settings_title=0x7f070022;
        public static final int pref_app_server_title=0x7f070004;
        public static final int pref_app_title=0x7f070003;
        /** string name="pref_lfg_key">lfg</string>
    <string name="pref_lfg_title">Look for a group</string>
    <string name="pref_lfg_summary">Turn on looking for group mode</string
string name="pref_scan_key">scan</string>
    <string name="pref_scan_title">Ready to scan</string>
    <string name="pref_scan_summary">Turn on ready to scan mode</string
         */
        public static final int pref_centering_key=0x7f070016;
        public static final int pref_centering_summary=0x7f070018;
        public static final int pref_centering_title=0x7f070017;
        public static final int pref_chat_messages_key=0x7f07000a;
        public static final int pref_chat_messages_size_key=0x7f07000b;
        public static final int pref_chat_messages_val_key=0x7f07000c;
        public static final int pref_google_id_key=0x7f070006;
        public static final int pref_group_auto_key=0x7f07001c;
        public static final int pref_group_auto_summary=0x7f07001e;
        public static final int pref_group_auto_title=0x7f07001d;
        public static final int pref_group_interest_key=0x7f07001f;
        public static final int pref_group_interest_summary=0x7f070021;
        public static final int pref_group_interest_title=0x7f070020;
        public static final int pref_group_name_key=0x7f070019;
        public static final int pref_group_name_summary=0x7f07001b;
        public static final int pref_group_name_title=0x7f07001a;
        public static final int pref_is_in_group_key=0x7f070009;
        public static final int pref_is_leader_key=0x7f070008;
        /** string name="pref_cat1_key">cat1</string>
    <string name="pref_cat2_key">cat2</string
         */
        public static final int pref_name_key=0x7f070005;
        public static final int pref_probe_id_key=0x7f070007;
        public static final int pref_radius_key=0x7f07000d;
        public static final int pref_radius_summary=0x7f07000f;
        public static final int pref_radius_title=0x7f07000e;
        public static final int pref_server_name_key=0x7f070010;
        public static final int pref_server_name_summary=0x7f070012;
        public static final int pref_server_name_title=0x7f070011;
        public static final int pref_server_port_key=0x7f070013;
        public static final int pref_server_port_summary=0x7f070015;
        public static final int pref_server_port_title=0x7f070014;
        public static final int settings_title=0x7f070001;
        public static final int toast_connect=0x7f070032;
        public static final int toast_connect_fail=0x7f070033;
        public static final int toast_group_assign=0x7f070037;
        public static final int toast_group_invitation=0x7f070036;
        public static final int toast_group_left=0x7f070038;
        public static final int toast_network_fail=0x7f070034;
        public static final int toast_treasure_put=0x7f070035;
    }
    public static final class xml {
        public static final int group_preferences=0x7f050000;
        public static final int preferences=0x7f050001;
    }
}
