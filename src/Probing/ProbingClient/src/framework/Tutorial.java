package framework;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class Tutorial
{
    public static final String    TAG    = "GNUC-TUTORIAL";
    public static Context        cx        = null;

    //
    public static void background(final Runnable r)
    {
        new Thread()
        {
            @Override
            public void run()
            {
                r.run();
            }
        }.start();
    }

    public static class Pref
    {
        public static String    OAUTH_GO_OAUTH_CODE        = "";
        public static String    OAUTH_GO_TOKEN                = "";
        public static String    OAUTH_GO_REFRESH_TOKEN    = "";

        public static void read()
        {
            SharedPreferences p = cx.getSharedPreferences(TAG, Activity.MODE_PRIVATE);
            OAUTH_GO_OAUTH_CODE = p.getString("OAUTH_GO_OAUTH_CODE", OAUTH_GO_OAUTH_CODE);
            OAUTH_GO_TOKEN = p.getString("OAUTH_GO_TOKEN", OAUTH_GO_TOKEN);
            OAUTH_GO_REFRESH_TOKEN = p.getString("OAUTH_GO_REFRESH_TOKEN", OAUTH_GO_REFRESH_TOKEN);
        }

        public static void write()
        {
            SharedPreferences.Editor pe = cx.getSharedPreferences(TAG, Activity.MODE_PRIVATE).edit();
            pe.putString("OAUTH_GO_OAUTH_CODE", OAUTH_GO_OAUTH_CODE);
            pe.putString("OAUTH_GO_TOKEN", OAUTH_GO_TOKEN);
            pe.putString("OAUTH_GO_REFRESH_TOKEN", OAUTH_GO_REFRESH_TOKEN);
            //
            pe.commit();
        }
    }
}