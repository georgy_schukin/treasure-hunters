package ru.ssd.probing.client.clientaux.items;

import android.graphics.drawable.Drawable;
import com.google.android.maps.GeoPoint;

/**
 * © Georgy Schukin
 * Item to represent treasure on the map
 */

public class TreasureItem extends MapIconItem {
    public TreasureItem(GeoPoint position, String title) {
        super(position, title);
    }

    public TreasureItem(GeoPoint position, String title, Drawable icon) {
        super(position, title, icon);
    }
}
