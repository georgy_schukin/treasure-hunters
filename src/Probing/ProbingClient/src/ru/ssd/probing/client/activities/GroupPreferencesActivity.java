package ru.ssd.probing.client.activities;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import ru.ssd.probing.client.R;

/**
 * Group settings
 */

public class GroupPreferencesActivity extends PreferenceActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.group_preferences);
    }
}