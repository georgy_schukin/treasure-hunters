package ru.ssd.probing.client.clientaux.prefs;

import android.app.Activity;
import android.content.SharedPreferences;
import ru.ssd.probing.client.R;
import ru.ssd.probing.common.model.ProbeProperties;

/**
 * © Georgy Schukin
 * Shared preferences for group
 */

public class ProbingAppPreferences extends ProbingPreferences
{
    private int scanRadiusIndex;
    private boolean centerOnMe;
    //private boolean lookingForGroup;
    //private boolean readyToScan;
    private String serverName;
    private int serverPort;

    public ProbingAppPreferences(Activity activity, SharedPreferences sharedPreferences) {
        super(activity, sharedPreferences);
        update();
    }

    public ProbingAppPreferences(ProbingAppPreferences appPreferences) {
        super(appPreferences.getActivity(), appPreferences.getSharedPreferences());
        this.scanRadiusIndex = appPreferences.getScanRadiusIndex();
        this.centerOnMe = appPreferences.isCenterOnMe();
        //this.lookingForGroup = appPreferences.isLookingForGroup();
        //this.readyToScan = appPreferences.isReadyToScan();
        this.serverName = appPreferences.getServerName();
        this.serverPort = appPreferences.getServerPort();
    }

    public final int getScanRadiusIndex() {
        return scanRadiusIndex;
    }

    public final boolean isCenterOnMe() {
        return centerOnMe;
    }

    /*public final boolean isLookingForGroup() {
        return lookingForGroup;
    }

    public final boolean isReadyToScan() {
        return readyToScan;
    }*/

    public final String getServerName() {
        return serverName;
    }

    public final int getServerPort() {
        return serverPort;
    }

    public final void setScanRadiusIndex(int scanRadiusIndex) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(activity.getString(R.string.pref_radius_key), Integer.toString(scanRadiusIndex));
        edit.commit();
        this.scanRadiusIndex = scanRadiusIndex;
    }

    /*public final void setLookingForGroup(boolean lookingForGroup) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean(activity.getString(R.string.pref_lfg_key), lookingForGroup);
        edit.commit();
        this.lookingForGroup = lookingForGroup;
    } */

    public void update() {
         scanRadiusIndex = Integer.parseInt(sharedPreferences.getString(activity.getString(R.string.pref_radius_key),
                 Integer.toString(ProbeProperties.DEFAULT_SCAN_RADIUS_INDEX))); // get radius from preferences
         centerOnMe = sharedPreferences.getBoolean(activity.getString(R.string.pref_centering_key), true);
         //lookingForGroup = sharedPreferences.getBoolean(activity.getString(R.string.pref_lfg_key), true);
         //readyToScan = sharedPreferences.getBoolean(activity.getString(R.string.pref_scan_key), true);
         serverName = sharedPreferences.getString(activity.getString(R.string.pref_server_name_key), "ssd1.sscc.ru");
         serverPort = Integer.parseInt(sharedPreferences.getString(activity.getString(R.string.pref_server_port_key),
                 Integer.toString(6893)));
    }
}
