package ru.ssd.probing.client.clientaux.managers;

import ru.ssd.probing.client.clientaux.overlays.MapItemOverlay;

/**
 * © Georgy Schukin
 * Manager of game objects (with ability to place object items on linked overlay)
 */

public interface ObjectManager {
    public void updateOverlay(MapItemOverlay overlay);
}
