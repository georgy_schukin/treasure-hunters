package ru.ssd.probing.client.communication;

import android.util.Log;
import ru.ssd.probing.common.model.*;
import ru.ssd.probing.common.protocol.ServerResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
 * To read from server via socket and call handlers
 */

public class ProbingServerCommunicatorReader implements Runnable {
    private BufferedReader reader;
    private boolean isWorking = false;
    private List<ServerCommunicatorListener> listeners = new ArrayList<ServerCommunicatorListener>();

    public ProbingServerCommunicatorReader() {}

    public void init(Socket clientSocket) throws IOException {
        try {
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), "UTF-8")); // read data from socket
            //isWorking = true;
        }
        catch (IOException e) {
            Log.e("Comm read error", e.getMessage(), e);
            throw e; // throw further
        }
    }

    @Override
    public void run() {
        isWorking = true;
        try {
            while (isWorking) {
                try{
                    if(reader.ready()) {
                        String tag = read();
                        if(tag != null && tag.length() > 0) {
                            Log.d("Read", tag);
                            onNewMessage(tag);
                        }
                    } else {
                        Thread.sleep(1000);
                    }
                }
                catch (IllegalArgumentException e) {
                    Log.e("Comm read error", e.getMessage(), e);
                }
            }
        }
        catch (Exception e) {
            Log.e("Comm read error", e.getMessage(), e);
        }

    }

    public void stop() {
        isWorking = false;
    }

    public final void close() {
        try {
            if(reader != null)
                reader.close();
        }
        catch (Exception e) {
            Log.e("Comm read error", e.getMessage(), e);
        }
    }

    protected void onNewMessage(String tag) throws IllegalArgumentException {
        if(tag.equals("FlagPut")) onFlagPut();
        if(tag.equals("FlagRemoved")) onFlagRemoved();
        else if(tag.equals("LFGPlayer")) onLFGPlayer();
        else if(tag.equals("NewTeammate")) onNewTeammate();
        else if(tag.equals("TeammatePositionChanged")) onTeammatePositionChanged();
        else if(tag.equals("TeammateScanRadiusChanged")) onTeammateScanRadiusChanged();
        else if(tag.equals("TeammateScanReadyStateChanged")) onTeammateScanReadyStateChanged();
        else if(tag.equals("TeammateLeft")) onTeammateLeft();
        else if(tag.equals("LeaderChanged")) onLeaderChanged();
        else if(tag.equals("GroupScanStarted")) onGroupScanStarted();
        else if(tag.equals("FoundSignals")) onFoundSignals();
        else if(tag.equals("GroupAssigned")) onGroupAssigned();
        else if(tag.equals("TreasureFound")) onTreasureFound();
        else if(tag.equals("InvitationReceived")) onInvitationReceived();
        else if(tag.equals("LeftGroup")) onLeftGroup();
        else if(tag.equals("Response")) onResponse();
        else throw new IllegalArgumentException("Unknown tag " + tag);
    }

    protected void onFlagPut() {
        int latitude = toInt(read());
        int longitude = toInt(read());
        int color = toInt(read());
        boolean leader =  toBoolean(read());
        Flag flag = new Flag(new Color(color), new Position(latitude, longitude), leader);
        for(ServerCommunicatorListener l : listeners)
            l.onFlagPut(flag);
    }

    protected void onFlagRemoved() {
        int latitude = toInt(read());
        int longitude = toInt(read());
        Position flagPosition = new Position(latitude, longitude);
        for(ServerCommunicatorListener l : listeners)
            l.onFlagRemoved(flagPosition);
    }

    protected void onLFGPlayer() {
        String googleId = read();
     //   String probeId = read();
        String name = read();
        int latitude = toInt(read());
        int longitude = toInt(read());
        boolean isLFG = toBoolean(read());
        Player player = new Player(name,
                new Position(latitude, longitude), googleId, "");
        for(ServerCommunicatorListener l : listeners)
            l.onLFGPlayer(player, isLFG);
    }

    protected void onNewTeammate() {
        String googleId = read();
        String name = read();
        int latitude = toInt(read());
        int longitude = toInt(read());
        int scanRadiusIndex = toInt(read());
        int scanStrength = toInt(read());
        Player player = new Player(name,
                new Position(latitude, longitude), googleId, "", new ProbeProperties(scanRadiusIndex, scanStrength));
        for(ServerCommunicatorListener l : listeners)
            l.onNewTeammate(player);
    }

    protected void onTeammatePositionChanged() {
        String googleId = read();
        int latitude = toInt(read());
        int longitude = toInt(read());
        Position position = new Position(latitude, longitude);
        for(ServerCommunicatorListener l : listeners)
            l.onTeammatePositionChanged(googleId, position);
    }

    protected void onTeammateScanRadiusChanged() {
        String googleId = read();
        int scanRadiusIndex = toInt(read());
        for(ServerCommunicatorListener l : listeners)
            l.onTeammateScanRadiusChanged(googleId, scanRadiusIndex);
    }

    protected void onTeammateScanReadyStateChanged() {
        String googleId = read();
        boolean readyToScan = toBoolean(read());
        for(ServerCommunicatorListener l : listeners)
            l.onTeammateScanReadyStateChanged(googleId, readyToScan);
    }

    protected void onTeammateLeft() {
        String googleId = read();
        for(ServerCommunicatorListener l : listeners)
            l.onTeammateLeft(googleId);
    }

    protected void onLeaderChanged() {
        String googleId = read();
        for(ServerCommunicatorListener l : listeners)
            l.onLeaderChanged(googleId);
    }

    protected void onGroupScanStarted() {
        for(ServerCommunicatorListener l : listeners)
            l.onGroupScanStarted();
    }

    protected void onFoundSignals() {
        int numOfSignals = toInt(read());
        //Log.d("Signals", numOfSignals + " received");
        List<Signal> signals = new ArrayList<Signal>();
        while (numOfSignals > 0) {
            int strength = toInt(read());
            int latitude = toInt(read());
            int longitude = toInt(read());
            signals.add(new Signal(strength, new Position(latitude, longitude)));
            numOfSignals--;
        }
        for(ServerCommunicatorListener l : listeners)
            l.onFoundSignals(signals);
    }

    //GroupAssigned
    //string (group name)
    //0|1 (automaticallyAcceptingNewMembers)
    //0|1 (interestedInNewMembers)
    //google_id (leader)
    //integer (total number of players in the group with leader ???)
    //google_id
    //name (string)
    //latitude
    //longitude
    //radius index
    //scan strength
    //...
    //integer (number of flags)
    //latitude
    //longitude
    //integer (color)
    // is leader flag (bool)
    //...
    protected void onGroupAssigned() {
        String groupName = read();
        boolean isAccepting = toBoolean(read());
        boolean isInterested = toBoolean(read());
        String leaderId = read();
        int numOfPlayers = toInt(read());
        Group group = new Group(groupName);
            group.setProperties(new GroupProperties(isAccepting, isInterested));
        while (numOfPlayers > 0) {
            String googleId = read();
            String name = read();
            int latitude = toInt(read());
            int longitude = toInt(read());
            int scanRadiusIndex = toInt(read());
            int scanStrength = toInt(read());
            Player player = new Player(name, new Position(latitude, longitude), googleId, "",
                    new ProbeProperties(scanRadiusIndex, scanStrength));
            group.addPlayer(player);
            numOfPlayers--;
        }
        int numOfFlags = toInt(read());
        while (numOfFlags > 0) {
            int latitude = toInt(read());
            int longitude = toInt(read());
            int color = toInt(read());
            boolean leader = toBoolean(read());
            group.addFlag(new Flag(new Color(color), new Position(latitude, longitude), leader));
            numOfFlags--;
        }
        for(Player p : group.getPlayers()) { // search for leader
            if(p.getGoogleId().equals(leaderId)) {
                group.changeLeader(p);
                break;
            }
        }
        for(ServerCommunicatorListener l : listeners)
            l.onGroupAssigned(group);
    // TODO: check
    }

    //Treasure
    //latitude
    //longitude
    //integer (treasure type)
    //integer (buffer length)
    //byte stream
    protected void onTreasureFound() {
        int latitude = toInt(read());
        int longitude = toInt(read());
        //int type = toInt(read());
        //int bufLength = toInt(read());
        String data = read();
        //byte data[] = new byte[bufLength];
        //TODO: fix!
        Treasure treasure = new Treasure(data, new Position(latitude, longitude));
        for(ServerCommunicatorListener l : listeners)
            l.onTreasureFound(treasure);
    }

    //InvitationReceived
    //groupName
    //memberAmount (integer)
    //name
    //...
    protected void onInvitationReceived() {
        String groupName = read();
        int numOfPlayers = toInt(read());
        Set<String> playerNames = new HashSet<String>();
        while (numOfPlayers > 0) {
            String playerName = read();
            playerNames.add(playerName);
            numOfPlayers--;
        }
        for(ServerCommunicatorListener l : listeners)
            l.onInvitationReceived(groupName, playerNames);
    }

    protected void onLeftGroup() {
        for(ServerCommunicatorListener l : listeners)
            l.onLeftGroup();
    }

    //Response
    //integer (error code)
    protected void onResponse() {
        int errorCode = toInt(read());
        ServerResponse response = new ServerResponse(errorCode);
        for(ServerCommunicatorListener l : listeners)
            l.onResponse(response);
    }

    public void addListener(ServerCommunicatorListener listener) {
        listeners.add(listener);
    }

    public void removeListener(ServerCommunicatorListener listener) {
        listeners.remove(listener);
    }

    protected String read() {
        try {
            return reader.readLine();
        }
        catch (IOException e) {
            Log.e("Comm read io error", e.getMessage(), e);
            return "";
        }
    }

    protected int toInt(String s) {
        try {
            return Integer.parseInt(s);
        }
        catch (NumberFormatException e) {
            Log.e("Comm read int parse error", e.getMessage(), e);
            return 0;
        }
    }

    protected boolean toBoolean(String s) {
        return !s.equals("0");
    }
}
