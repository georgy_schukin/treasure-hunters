package ru.ssd.probing.client.clientaux.items;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;

/**
 * © Georgy Schukin
 * Item that can be drawn as icon
 */

public class MapIconItem extends MapItem {
    protected Drawable icon;

    public MapIconItem(GeoPoint position, String title) {
        super(position, title);
    }

    public MapIconItem(GeoPoint position, String title, Drawable icon) {
        super(position, title);
        this.icon = icon;
        this.borderRect = MapItem.centerBorderRect(icon);
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
        this.borderRect = MapItem.centerBorderRect(icon);
    }

    public final Drawable getIcon() {
        return icon;
    }

    public int getIconIndex(int size) { // for icon overlay
        return 0;
    }

    @Override
    public void draw(Canvas canvas, MapView mapView, boolean shadow) {
        if(!shadow) {
            if(icon != null) {
                Point pos = mapView.getProjection().toPixels(getPosition(), null);
                Rect borderRect = getBorderRect();
                Rect iconRect = new Rect(pos.x + borderRect.left, pos.y + borderRect.top,
                        pos.x + borderRect.right, pos.y + borderRect.bottom);
                icon.setBounds(iconRect);
                icon.draw(canvas);
            }
        }
    }
}
