package ru.ssd.probing.client.chat;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import ru.ssd.probing.client.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for list items
 */

public class ChatArrayAdapter extends ArrayAdapter<String> {
    private List<String> messages = new ArrayList<String>();
    private LayoutInflater layoutInflater;

    public ChatArrayAdapter(Context context, int textViewResourceId, List<String> messages) {
        super(context, textViewResourceId, messages);
        this.messages = messages;
        layoutInflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        try {
            if (v == null) {
                v = layoutInflater.inflate(R.layout.chat_elem, parent, false); // inflate item layout
            }

            String message = messages.get(position);
            if (message != null) {
                TextView textView = (TextView)v.findViewById(R.id.chat_item_message);
                if (textView != null) {
                    textView.setText(message);
                }
            }
        }
        catch (Exception e) {
            Log.e("Chat adapter error", e.getMessage(), e);
        }
        return v;
    }
}


