package ru.ssd.probing.client.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import framework.Tutorial;
import ru.ssd.probing.client.R;

/**
 * Main activity
 */

public class MainActivity extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authorization);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Tutorial.cx = this;

        try {
            ImageButton signInButton = (ImageButton)findViewById(R.id.sign_in);
            Animation signInButtonAnimation = AnimationUtils.loadAnimation(this, R.anim.sign_in_button);

            signInButton.clearAnimation();
            signInButton.startAnimation(signInButtonAnimation);
        }
        catch (Exception e) {
            Log.e("Auth error", e.getMessage(), e);
        }
    }

    public void onGoogleClick(View v) {
        try
        {
            startActivity(new Intent(this, AuthorizationActivity.class));
        }
        catch (Exception e)
        {
            Log.e("Merde", e.getMessage(), e);
        }
    }
}
