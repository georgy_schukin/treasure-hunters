package ru.ssd.probing.client.chat;

import android.util.Log;

import java.io.BufferedReader;

/**
 * Chat reader thread
 */

public class ChatReader implements Runnable
{
    private ChatListener chatListener;
    private BufferedReader reader;
    private boolean isWorking;

    public ChatReader(BufferedReader reader, ChatListener chatListener)
    {
        this.reader = reader;
        this.chatListener = chatListener;
    }

    @Override
    public void run()
    {
        isWorking = true;
        while (isWorking) {
            try {
                if(reader.ready()) {
                    String message = reader.readLine();
                    if(message != null && message.length() > 0) {
                        chatListener.onChatUpdate(message);
                    }
                } else {
                    Thread.sleep(100);
                }
            }
            catch (Exception e)
            {
                Log.e("Chat reader error", e.getMessage(), e);
            }
        }
    }

    public void stop() {
        isWorking = false;
    }

}
