package ru.ssd.probing.client.activities;

import android.accounts.NetworkErrorException;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.android.maps.*;
import ru.ssd.probing.client.R;
import ru.ssd.probing.client.chat.ChatActivity;
import ru.ssd.probing.client.clientaux.items.FlagItem;
import ru.ssd.probing.client.clientaux.items.MapItem;
import ru.ssd.probing.client.clientaux.items.PlayerItem;
import ru.ssd.probing.client.clientaux.items.SignalItem;
import ru.ssd.probing.client.clientaux.managers.ProbingClientManager;
import ru.ssd.probing.client.clientaux.overlays.MapIconItemOverlay;
import ru.ssd.probing.client.clientaux.overlays.MapItemOverlay;
import ru.ssd.probing.client.clientaux.prefs.ProbingAppPreferences;
import ru.ssd.probing.client.clientaux.prefs.ProbingGroupPreferences;
import ru.ssd.probing.client.clientaux.prefs.ProbingPlayerPreferences;
import ru.ssd.probing.client.communication.ProbingServerCommunicator;
import ru.ssd.probing.client.communication.ServerCommunicatorListener;
import ru.ssd.probing.common.model.*;
import ru.ssd.probing.common.protocol.ServerResponse;

import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * © Georgy Schukin
 * Map rendering and interaction activity
 */

public class ProbingMapActivity extends MapActivity
        implements LocationListener, ServerCommunicatorListener, MapItemOverlay.OverlayListener /*,Player.Listener, Group.Listener*/
{
    private boolean isDestroyed = false;

    private MapController mapController;

    private ProbingPlayerPreferences playerPreferences;
    private ProbingAppPreferences appPreferences;
    private ProbingGroupPreferences groupPreferences;

    private ProbingClientManager gameManager;

    private MapIconItemOverlay playerOverlay;
    private MapIconItemOverlay flagOverlay;
    private MapIconItemOverlay signalOverlay;
    //private MapIconItemOverlay treasureOverlay;

    private final ProbingServerCommunicator serverCommunicator = new ProbingServerCommunicator();

    private class Toaster implements Runnable // to show short message
    {
        protected Context context;
        protected String message;

        public Toaster(Context context, String message) {
            this.context = context;
            this.message = message;
        }

        @Override
        public void run() {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    private class AppDialog extends Toaster // to show dialog
    {
        protected String title;
        protected Drawable icon;
        protected boolean useCancel = false;
        protected DialogInterface.OnClickListener listener;

        public AppDialog(Context context, String title, String message, Drawable icon) {
            super(context, message);
            this.title = title;
            this.icon = icon;
        }

        public AppDialog(Context context, String title, String message, Drawable icon, boolean useCancel,
                         DialogInterface.OnClickListener listener) {
            super(context, message);
            this.title = title;
            this.icon = icon;
            this.useCancel = useCancel;
            this.listener = listener;
        }

        @Override
        public void run() {
            AlertDialog.Builder dlg = new AlertDialog.Builder(context);
            dlg.setTitle(title);
            dlg.setMessage(message);
            dlg.setIcon(icon);
            dlg.setPositiveButton(getString(R.string.dlg_ok), listener);
            if(useCancel) dlg.setNegativeButton(getString(R.string.dlg_cancel), listener);
            dlg.create();
            dlg.show();
        }
    }

    private class AppProgressDialog extends Toaster
    {
        protected String title;
        protected ProgressDialog dlg;

        public AppProgressDialog(Context context, String title, String message) {
            super(context, message);
            this.title = title;
        }

        @Override
        public void run() {
            dlg = ProgressDialog.show(context, title, message, true, true);
        }

        public void stop() {
            if(dlg != null)
                dlg.dismiss();
        }
    }

    private final Handler serverHandler = new Handler(); // to checks server connection
    private AppProgressDialog scanDialog;

    private enum TapMode {
        NONE,
        ADD_FLAG,
        REMOVE_FLAG,
        ADD_TREASURE,
        CHANGE_LEADER
    }

    private TapMode tapMode = TapMode.NONE;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.map);

        MapView mapView = (MapView)findViewById(R.id.mapview);
        mapView.setBuiltInZoomControls(false); // disable built in zoom buttons

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);  // load preferences
        playerPreferences = new ProbingPlayerPreferences(this, sharedPreferences);
        appPreferences = new ProbingAppPreferences(this, sharedPreferences);
        groupPreferences = new ProbingGroupPreferences(this, sharedPreferences);

        //playerPreferences.setLeader(false);
        //playerPreferences.setInGroup(false);
        //appPreferences.setScanRadiusIndex(ProbeProperties.DEFAULT_SCAN_RADIUS_INDEX);

        Position ownerPosition = new Position((int)(37*1E6), (int)(-122*1E6));
        String ownerName = playerPreferences.getPlayerName();
        String googleId = playerPreferences.getGoogleId();
        String probeId = playerPreferences.getProbeId();

        Player ownerPlayer = new Player(ownerName, ownerPosition, googleId, probeId);
            ownerPlayer.setProbeProperties(new ProbeProperties(appPreferences.getScanRadiusIndex(),
                    ProbeProperties.DEFAULT_SCAN_STRENGTH));
            ownerPlayer.setLFG(false);

        gameManager = new ProbingClientManager(ownerPlayer);

        ArrayList<Drawable> playerIcons = new ArrayList<Drawable>();
            playerIcons.add(getResources().getDrawable(R.drawable.player_icon));
            playerIcons.add(getResources().getDrawable(R.drawable.player_leader_icon));
        playerOverlay = new MapIconItemOverlay(gameManager.getPlayerManager(), mapView, playerIcons); // create overlay with items

        ArrayList<Drawable> flagIcons = new ArrayList<Drawable>();
            flagIcons.add(getResources().getDrawable(R.drawable.flag_icon));
            flagIcons.add(getResources().getDrawable(R.drawable.flag_leader_icon));
        flagOverlay = new MapIconItemOverlay(gameManager.getFlagManager(), mapView, flagIcons);

        ArrayList<Drawable> signalIcons = new ArrayList<Drawable>();
            signalIcons.add(getResources().getDrawable(R.drawable.signal_icon_1));
            signalIcons.add(getResources().getDrawable(R.drawable.signal_icon_2));
            signalIcons.add(getResources().getDrawable(R.drawable.signal_icon_3));
            signalIcons.add(getResources().getDrawable(R.drawable.signal_icon_4));
            signalIcons.add(getResources().getDrawable(R.drawable.chest_icon));
        signalOverlay = new MapIconItemOverlay(gameManager.getSignalManager(), mapView, signalIcons);

        //Drawable chestIcon = getResources().getDrawable(R.drawable.chest_icon);
        //treasureOverlay = new MapIconItemOverlay(gameManager.getTreasureManager(), mapView, chestIcon);

        playerOverlay.addListener(this);
        signalOverlay.addListener(this); // listen for clicks on signals
        flagOverlay.addListener(this);

        List<Overlay> mapOverlays = mapView.getOverlays();
            mapOverlays.clear();
            mapOverlays.add(flagOverlay);
            mapOverlays.add(signalOverlay);
            //mapOverlays.add(treasureOverlay);
            mapOverlays.add(playerOverlay);

        scanDialog = new AppProgressDialog(this, getString(R.string.dlg_scan_title),
                getString(R.string.dlg_scan_message));

        mapController = mapView.getController();
        mapController.setZoom(16);

        serverCommunicator.addListener(this); // listen for server

        isDestroyed = false;

        onServerCheck(); //  start connect + check for connection
        //onServerConnect();
    }

    @Override
    protected boolean isRouteDisplayed() {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateGUI();
    }

    @Override
    public void onStart() { // when activity is started (after create or restart)
        super.onStart();
        try {
            //onServerCheck(); // run server

            Player ownerPlayer = gameManager.getOwnerPlayer();

            LocationManager locationManager = (LocationManager)this.getSystemService(LOCATION_SERVICE); // get service
            /*Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER); // get location from GRS
            if(location == null)
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER); // try to get location from network
            if(location != null) {
                Position position = new Position((int)(location.getLatitude() * 1E6),
                        (int)(location.getLongitude() * 1E6));
                serverCommunicator.changePosition(position);
                //ownerPlayer.setPosition()); // update position
            }*/
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onStop() { // when activity is stopped
        super.onStop();
        LocationManager locationManager = (LocationManager)this.getSystemService(LOCATION_SERVICE);
        locationManager.removeUpdates(this); // don't listen for location wjile stopped
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isDestroyed = true;
        serverCommunicator.stop(); // disconnect from server
    }

    /*******************************************************************************************************
     * Server connection handling
     *******************************************************************************************************/

    protected boolean isNetworkOK() { // check if network is available
        ConnectivityManager connectivityManager =
                (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        return  ((connectivityManager.getActiveNetworkInfo() != null)
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected());
    }

    protected void onServerCheck() { // check for server connection
        try {
            if(isDestroyed) return; // activity is destroyed - don't check anymore
            if(!isNetworkOK()) { // network connection is lost
                onServerConnectionLost(); // reset game state
                showToast(getString(R.string.toast_network_fail));
            }
            else if(!serverCommunicator.isOpen()) { // connection with server is lost
                onServerConnectionLost(); // reset game state
                onServerConnect(); // try to reconnect
            }
            serverHandler.postDelayed(new Runnable() { // new check in future
                @Override
                public void run() {
                    onServerCheck();
                }
            }, 20000);
        }
        catch (Exception e) {
            Log.e("Server error", e.getMessage(), e);
        }
    }

    protected void onServerConnect() { // connect/reconnect to server
        try {
            if(!isNetworkOK()) {
                throw new NetworkErrorException(getString(R.string.toast_network_fail));
            }

            if(serverCommunicator.isOpen()) { // check if need to reconnect
                onServerConnectionLost(); // stop previous communicator
            }

            Player ownerPlayer = gameManager.getOwnerPlayer();

            final String serverName = appPreferences.getServerName();
            final int serverPort = appPreferences.getServerPort();
            serverCommunicator.connect(serverName, serverPort, ownerPlayer); // start comm (reader and writer) (exception if failed)

            int scanRadiusIndex = ownerPlayer.getProbeProperties().getScanRadiusIndex();
            //serverCommunicator.newPlayer(ownerPlayer); // inform about us
            if(scanRadiusIndex != ProbeProperties.DEFAULT_SCAN_RADIUS_INDEX)
                serverCommunicator.changeScanRadiusIndex(scanRadiusIndex); // inform about scan

            showToast(getString(R.string.toast_connect) + " " + serverName + ":" + serverPort);
        }
        catch (NetworkErrorException e) {
            showToast(getString(R.string.toast_connect_fail) + ": " + e.getMessage());
        }
        catch (UnknownHostException e) {
            showToast(getString(R.string.toast_connect_fail) + ": " + e.getMessage());
        }
        catch (IOException e) {
            showToast(getString(R.string.toast_connect_fail) + ": " + e.getMessage());
        }
        catch (Exception e) {
            Log.e("Server error", e.getMessage(), e);
        }
    }

    protected void onServerConnectionLost() {
        try {
            scanDialog.stop();
            serverCommunicator.stop(); // stop communicator
            onGroupLeave();
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    protected void update() {
        try {
            playerOverlay.update();
            flagOverlay.update();
            signalOverlay.update();
            updateGUI();
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    protected void onGroupLeave() {
        try {
            gameManager.reset(); // reset
            playerPreferences.setInGroup(gameManager.isInGroup());
            playerPreferences.setLeader(gameManager.isLeader());
            update();
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    /**********************************************************************************************
     * Location handling
     **********************************************************************************************/

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
        LocationManager locationManager = (LocationManager)this.getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(provider, 0, 0, this);
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            int latitude = (int)(location.getLatitude() * 1E6);
            int longitude = (int)(location.getLongitude() * 1E6);
            Position prevPosition = gameManager.getOwnerPlayer().getPosition();
            if((latitude != prevPosition.getLatitude()) || (longitude != prevPosition.getLongitude())) {
                Position position = new Position(latitude, longitude);
                serverCommunicator.changePosition(position);
                onTeammatePositionChanged(gameManager.getOwnerPlayer().getGoogleId(), position); // hack to show pos without server
            }
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    /****************************************************************************************************
     * Settings handling
     ****************************************************************************************************/

    private void onAppPreferencesChanged() {
        ProbingAppPreferences prev = new ProbingAppPreferences(appPreferences); // get previous settings
        appPreferences.update(); // load new values

        if(!prev.getServerName().equals(appPreferences.getServerName()) ||
                (prev.getServerPort() != appPreferences.getServerPort())) {
            onServerConnect(); // reconnect
        }

        int scanRadiusIndex = appPreferences.getScanRadiusIndex();
        if(prev.getScanRadiusIndex() != scanRadiusIndex) {
            serverCommunicator.changeScanRadiusIndex(scanRadiusIndex); // inform server
        }
    }

    private void onGroupPreferencesChanged() {
        ProbingGroupPreferences prev = new ProbingGroupPreferences(groupPreferences); // get previous settings
        groupPreferences.update(); // load new values

        boolean isAccepting = groupPreferences.isAutomaticallyAcceptingNewMembers();
        boolean isInterested = groupPreferences.isInterestedInNewMembers();
        if((prev.isAutomaticallyAcceptingNewMembers() != isAccepting) ||
                (prev.isInterestedInNewMembers() != isInterested)) {
            serverCommunicator.changeGroupProperties(new GroupProperties(isAccepting, isInterested));
            //gameManager.getOwnerGroup().setProperties(new GroupProperties(isAccepting, isInterested));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1: onAppPreferencesChanged(); return;
            case 2: onGroupPreferencesChanged(); return;
            default: super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_settings: {
                startActivityForResult(new Intent(this, AppPreferencesActivity.class), 1);
                break;
            }
            case R.id.menu_item_group_settings: {
                startActivityForResult(new Intent(this, GroupPreferencesActivity.class), 2);
                break;
            }
        }
        return true;
    }

    /*****************************************************************************************
     * GUI handling
     *****************************************************************************************/

    protected void showDialog(String title, String message, Drawable icon) { // show simple OK dialog
        runOnUiThread(new AppDialog(this, title, message, icon));
    }

    protected void showDialog(String title, String message, Drawable icon, boolean useCancel,
                              DialogInterface.OnClickListener listener) { // show OK/Cancel dialog
        runOnUiThread(new AppDialog(this, title, message, icon, useCancel, listener));
    }

    protected void showToast(String message) { // show pop-up message
        runOnUiThread(new Toaster(this, message));
    }

    public void onGUIUpdate() {
        ImageButton startGroupButton = (ImageButton)findViewById(R.id.gui_start_group);
        ImageButton scanButton = (ImageButton)findViewById(R.id.gui_scan);
        ImageButton putTreasureButton = (ImageButton)findViewById(R.id.gui_put_treasure);
        ImageButton putFlagButton = (ImageButton)findViewById(R.id.gui_put_flag);
        ImageButton leaveGroupButton = (ImageButton)findViewById(R.id.gui_leave_group);
        ImageButton changeRadiusButton = (ImageButton)findViewById(R.id.gui_change_radius);
        ImageButton lfgButton = (ImageButton)findViewById(R.id.gui_lfg);

        lfgButton.setSelected(gameManager.getOwnerPlayer().isLFG());
        scanButton.setSelected(gameManager.getOwnerPlayer().isReadyToScan());

        boolean isInGroup = gameManager.isInGroup();
        boolean isLeader = gameManager.isLeader();

        if(!isInGroup) {
            tapMode = TapMode.NONE;
            putFlagButton.setSelected(false);
            putTreasureButton.setSelected(false);
        }

        startGroupButton.setVisibility(isInGroup ? View.GONE : View.VISIBLE);
        scanButton.setVisibility(!isInGroup ? View.GONE : View.VISIBLE);
        putTreasureButton.setVisibility(!(isInGroup && isLeader) ? View.GONE : View.VISIBLE);
        putFlagButton.setVisibility(!isInGroup ? View.GONE : View.VISIBLE);
        leaveGroupButton.setVisibility(!isInGroup ? View.GONE : View.VISIBLE);
        changeRadiusButton.setVisibility(!isInGroup ? View.GONE : View.VISIBLE);
        lfgButton.setVisibility(isInGroup ? View.GONE : View.VISIBLE);
    }

    protected void updateGUI() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                onGUIUpdate();
            }
        });
    }

    public void onStartGroupButtonClicked(View v) {
        try {
            serverCommunicator.createGroup();
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    public void onLeaveGroupButtonClicked(View v) {
        try {
            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if(i == DialogInterface.BUTTON_POSITIVE)
                        serverCommunicator.leaveGroup();
                }
            };
            showDialog(getString(R.string.dlg_leave_group_title),
                   getString(R.string.dlg_leave_group_message) + " " + gameManager.getOwnerGroup().getName() + "?",
                   getResources().getDrawable(R.drawable.gui_leave_group_icon),
                   true, listener); // ask
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    public void onScanButtonClicked(View v) {
        try {
            boolean isReadyToScan = !gameManager.getOwnerPlayer().isReadyToScan();
            serverCommunicator.changeScanState(isReadyToScan);
            onTeammateScanReadyStateChanged(gameManager.getOwnerPlayer().getGoogleId(), isReadyToScan);
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    public void onPutTreasureButtonClicked(View v) {
        if(v.isSelected()) { // deselect
            v.setSelected(false);
            tapMode = TapMode.NONE;
        } else { // select
            v.setSelected(true);
            findViewById(R.id.gui_put_flag).setSelected(false);
            tapMode = TapMode.ADD_TREASURE;
        }
    }

    public void onPutFlagButtonClicked(View v) {
        if(v.isSelected()) { // deselect
            v.setSelected(false);
            tapMode = TapMode.NONE;
        } else { // select
            v.setSelected(true);
            findViewById(R.id.gui_put_treasure).setSelected(false);
            tapMode = TapMode.ADD_FLAG;
        }
    }

    public void onChangeRadiusButtonClicked(View v) {
        try {
            Player ownerPlayer = gameManager.getOwnerPlayer();
            int oldScanRadiusIndex = ownerPlayer.getProbeProperties().getScanRadiusIndex();
            int newScanRadiusIndex = (oldScanRadiusIndex + 1)%(ProbeProperties.maxPossibleScanRadiusIndex() + 1); // new radius
            serverCommunicator.changeScanRadiusIndex(newScanRadiusIndex);
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    public void onLFGButtonClicked(View v) {
        try {
            boolean isLFG = !gameManager.getOwnerPlayer().isLFG();
            serverCommunicator.changeLFGState(isLFG);
            gameManager.getOwnerPlayer().setLFG(isLFG);
            updateGUI();
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    public void onChatButtonClicked(View v) {
        startActivity(new Intent(this, ChatActivity.class));
    }

    public void onZoomInButtonClicked(View v) {
        mapController.zoomIn();
    }

    public void onZoomOutButtonClicked(View v) {
        mapController.zoomOut();
    }

    public void onExitButtonClicked(View v) {
        try {
            onServerConnectionLost(); // force server disconnect
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    /****************************************************************************************************
     * Map taps handling
     ****************************************************************************************************/

    private void onTapPutTreasure(GeoPoint geoPoint) {
        try {
            Position position = new Position(geoPoint.getLatitudeE6(), geoPoint.getLongitudeE6());
            Treasure treasure = new Treasure(0, position);
            serverCommunicator.putTreasure(treasure);
            showToast(getString(R.string.toast_treasure_put));
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    private void onTapPutFlag(GeoPoint geoPoint) {
        try {
            serverCommunicator.putFlag(new Position(geoPoint.getLatitudeE6(), geoPoint.getLongitudeE6()));
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    private void onTapFlag(FlagItem flagItem) {
        try {
            if((flagItem.isLeaderFlag() && gameManager.isLeader()) || !flagItem.isLeaderFlag()) {// can't delete leader flag
                final Position flagPosition =
                        new Position(flagItem.getPosition().getLatitudeE6(), flagItem.getPosition().getLongitudeE6());
                DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) { // if ok is pressed
                        if(i == DialogInterface.BUTTON_POSITIVE) {
                            serverCommunicator.removeFlag(flagPosition);
                        }
                    }
                };
                showDialog(getString(R.string.dlg_flag_remove_title),
                        getString(R.string.dlg_flag_remove_message),
                        getResources().getDrawable(R.drawable.flag_icon),
                        true, listener); // show flag remove dialog
            }
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    private void onTapPlayer(PlayerItem playerItem) {
        try {
            if(gameManager.isLeader()) { // leader can invite players
                final String playerName = playerItem.getTitle(); // title is a name!
                final Player player = gameManager.getPlayerManager().getPlayerByName(playerName); // get by name
                if((player != null) && !player.equals(gameManager.getOwnerPlayer())) { // another player
                    if(!gameManager.getPlayerManager().isInGroup(player)) { // player is not in group
                        serverCommunicator.invitePlayerToGroup(playerName); // send invite
                        showToast(getString(R.string.toast_group_invitation) + " " + playerName);
                    } else { // change leadership
                        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if(i == DialogInterface.BUTTON_POSITIVE) {
                                    serverCommunicator.changeLeader(player);
                                }
                            }
                        };
                        showDialog(getString(R.string.dlg_change_leader_title),
                                getString(R.string.dlg_change_leader_message) + " " + player.getName() + "?",
                                null, true, listener);
                    }
                }
            }
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    private void onTapSignal(SignalItem signalItem) {
        try {
            Signal signal = gameManager.getSignalManager().getSignal(signalItem.getPosition()); // get real signal
            if(signal == null)
                throw new NullPointerException("Failed to get signal");
            if(signal.getStrength() >= 100) { // treasure is found!!!
                serverCommunicator.getTreasure(signal.getPosition()); // inform server
            }
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public boolean onTap(MapItemOverlay sender, GeoPoint geoPoint) { // on free point tap handler
        if((sender == signalOverlay) && (tapMode == TapMode.ADD_TREASURE)) { // for signal overlay
            onTapPutTreasure(geoPoint); // add treasure
            return true; // finish handling for all further overlays
        } else if((sender == flagOverlay) && (tapMode == TapMode.ADD_FLAG)) { // for flag overlay
            onTapPutFlag(geoPoint); // add flag
            return true; // finish handling for all further overlays
        }
        return false; // continue handling
    }

    @Override
    public boolean onTap(MapItemOverlay sender, MapItem item) { // on item tap handler
        if((sender == signalOverlay) && (item instanceof SignalItem)) { // signal was tapped
            onTapSignal((SignalItem) item);
            return true; // finish handling for further overlays
        } else if((sender == playerOverlay) && (item instanceof PlayerItem)) { // player was tapped
            onTapPlayer((PlayerItem) item);
            return true; // finish handling for further overlays
        } else if((sender == flagOverlay) && (item instanceof FlagItem)) { // flag was tapped
            onTapFlag((FlagItem) item);
            return true; // finish handling for further overlays
        }
        return false; // continue handling
    }

    /***************************************************************************************************
    * Server communication listeners handling
    ****************************************************************************************************/

    @Override
    public void onFlagPut(Flag flag) {
        try {
            gameManager.onFlagPut(flag);
            flagOverlay.update();
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onLFGPlayer(Player player, boolean isLFG) {
        try {
            gameManager.onLFGPlayer(player, isLFG);
            playerOverlay.update();
            if(isLFG) {
               showToast(player.getName() + " is looking for a group");
            }
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onNewTeammate(Player player) {
        try {
            gameManager.onNewTeammate(player);
            playerOverlay.update();
            showToast(player.getName() + " is in the group " + gameManager.getOwnerGroup().getName() + " now");
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onTeammatePositionChanged(String teammateGoogleId, Position newPosition) {
        try {
            gameManager.onTeammatePositionChanged(teammateGoogleId, newPosition);
            playerOverlay.update();
            if(teammateGoogleId.equals(gameManager.getOwnerPlayer().getGoogleId())) { // it's me
                if(appPreferences.isCenterOnMe())
                    mapController.animateTo(new GeoPoint(newPosition.getLatitude(), newPosition.getLongitude()));
            }
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onTeammateScanRadiusChanged(String teammateGoogleId, int scanRadiusIndex) {
        try {
            gameManager.onTeammateScanRadiusChanged(teammateGoogleId, scanRadiusIndex);
            if(teammateGoogleId.equals(gameManager.getOwnerPlayer().getGoogleId())) { // it's me
                appPreferences.setScanRadiusIndex(scanRadiusIndex); // change prefs
            }
            playerOverlay.update();
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onTeammateScanReadyStateChanged(String teammateGoogleId, boolean readyToScan) {
        try {
            gameManager.onTeammateScanReadyStateChanged(teammateGoogleId, readyToScan);
            playerOverlay.update();
            updateGUI();
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onTeammateLeft(String teammateGoogleId) {
        try {
            final String playerName = gameManager.getPlayerManager().getPlayer(teammateGoogleId).getName();
            gameManager.onTeammateLeft(teammateGoogleId);
            playerOverlay.update();
            showToast(playerName + " has left the group " + gameManager.getOwnerGroup().getName());
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onLeaderChanged(String teammateGoogleId) {
        try {
            gameManager.onLeaderChanged(teammateGoogleId);
            playerPreferences.setLeader(gameManager.isLeader());
            update();
            Player player = gameManager.getPlayerManager().getPlayer(teammateGoogleId);
            String str = (gameManager.isLeader() ? "You're" : player.getName() + " is") + " the group leader now";
            showToast(str);
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onGroupScanStarted() {
        try {
            //gameManager.onGroupScanStarted();
            playerOverlay.update();
            runOnUiThread(scanDialog); // run progress dlg
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onFoundSignals(List<Signal> signals) {
        try {
            scanDialog.stop();
            int signalNum = 0, treasureNum = 0;
            for(Signal s : signals) {
                if(s.getStrength() == 100) treasureNum++;
                else signalNum++;
            }
            String str = "Found ";
            if(signals.size() == 0)
                str += "nothing";
            else {
                if (signalNum == 0) str += Integer.toString(treasureNum) + " treasures";
                else if(treasureNum == 0) str += Integer.toString(signalNum) + " signals";
                else str += Integer.toString(signalNum) + " signals and "
                            + Integer.toString(treasureNum) + " treasures";
            }
            gameManager.onFoundSignals(signals);
            signalOverlay.update();
            showToast(str);
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onGroupAssigned(Group group) {
        try {
            gameManager.onGroupAssigned(group);
            playerPreferences.setInGroup(gameManager.isInGroup()); // set in group
            playerPreferences.setLeader(gameManager.isLeader());
            update(); // update everything on map screen
            showToast(getString(R.string.toast_group_assign) + " " + group.getName());
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onTreasureFound(Treasure treasure) {
        try {
            gameManager.onTreasureFound(treasure);

            String text = getString(R.string.dlg_treasure_found_message);
            if(treasure.getType() == Treasure.TreasureType.TEXT) {
                ByteBuffer buf = treasure.getContents();
                byte[] arr = new byte[buf.remaining()];
                buf.get(arr);
                text = new String(arr);
            }
            showDialog(getString(R.string.dlg_treasure_found_title),
                    text, getResources().getDrawable(R.drawable.treasure_chest_opened_icon)); // show dialog
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onInvitationReceived(String groupName, Set<String> memberNames) { // we received an invitation
        try {
            final String gName = groupName;
            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if(i == DialogInterface.BUTTON_POSITIVE) { // invitation accepted
                        serverCommunicator.acceptInvitation(gName);
                    }
                }
            };
            showDialog(getString(R.string.dlg_group_invitation_title),
                    getString(R.string.dlg_group_invitation_message) + " " + gName,
                    null, true, listener); // show invitation dialog
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onLeftGroup() {
        final String groupName = gameManager.getOwnerGroup().getName();
        onGroupLeave(); // reset all
        showToast(getString(R.string.toast_group_left) + " " + groupName);
    }

    @Override
    public void onFlagRemoved(Position flagPosition) {
        try {
            gameManager.onFlagRemoved(flagPosition);
            flagOverlay.update();
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }

    @Override
    public void onResponse(ServerResponse response) {
        try {
            Log.d("Server response: ", Integer.toString(response.getErrorCode()));
            if(response.getErrorCode() != ServerResponse.ErrorCodes.NO_ERROR) {
                showToast("Response: error " + Integer.toString(response.getErrorCode()));
            }
        }
        catch (Exception e) {
            Log.e("Error", e.getMessage(), e);
        }
    }
}
