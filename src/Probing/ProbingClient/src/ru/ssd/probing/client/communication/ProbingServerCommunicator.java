package ru.ssd.probing.client.communication;

import android.util.Log;
import ru.ssd.probing.common.model.*;

import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;

/**
 * Georgy Schukin
 * Server communicator interface for android client
 */

public class ProbingServerCommunicator implements ServerCommunicator {
    //private String hostName;
    //private int hostPort;
    private Socket clientSocket;

    protected final LinkedList<String> msgQueue = new LinkedList<String>(); // strings to send (write)

    private final ProbingServerCommunicatorReader reader = new ProbingServerCommunicatorReader(); // reading part (from server)
    private final ProbingServerCommunicatorWriter writer = new ProbingServerCommunicatorWriter(msgQueue); // writing part (to server)

    private Thread readerThread;
    private Thread writerThread;

    public ProbingServerCommunicator() {}

    public synchronized final void connect(String hostName, int hostPort, Player player) throws Exception {
        try {
            synchronized (this) {
                stopCommunicator(); // stop previous comm
                clientSocket = new Socket(hostName, hostPort);

                reader.init(clientSocket);
                writer.init(clientSocket);

                writer.erase(); // erase all previous messages!!!!

                readerThread = new Thread(reader); // start separate reader thread
                writerThread = new Thread(writer);
                readerThread.start(); // start reader
                writerThread.start(); // start writer

                newPlayer(player); // send new player as first message
            }
        }
        /*catch (IOException e) {
            Log.e("Comm error", e.getMessage(), e);
            throw e;
        }
        catch (InterruptedException e) {
            Log.e("Comm error", e.getMessage(), e);
            throw e;
        } */
        catch (Exception e) {
            Log.e("Comm error", e.getMessage(), e);
            throw e;
        }
    }

    public synchronized final boolean isOpen() {
        synchronized (this) {
            return (clientSocket != null) && clientSocket.isConnected() && !clientSocket.isClosed();
        }
    }

    public synchronized final void stop() {
        try {
            synchronized (this) {
                stopCommunicator();
            }
        }
        catch (Exception e) {
            Log.e("Comm error", e.getMessage(), e);
        }
    }

    protected final void stopCommunicator() throws InterruptedException, IOException {
        if(writerThread != null) { // there is previous writer
            writer.stop();
            writerThread.join();  // wait for writer
            writer.close();
            writerThread = null;
        }
        if(readerThread != null) { // there is previous reader
            reader.stop();
            readerThread.join(); // wait for reader
            reader.close();
            readerThread = null;
        }
        if(clientSocket != null) {
            clientSocket.close(); // close previous socket
            clientSocket = null;
        }
    }

    /*
    * Communication with server
     */

    public void newPlayer(Player player) {
        try {
            writer.newPlayer(player);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    public void newGroup(Group group) {
        try {
            writer.newGroup(group);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void changeLFGState(boolean isLFG) {
        try {
            writer.changeLFGState(isLFG);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void changePosition(Position newPosition) {
        try {
            writer.changePosition(newPosition);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void changeGroupProperties(GroupProperties newProperties) {
        try {
            writer.changeGroupProperties(newProperties);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void putFlag(Position flagPosition) {
        try {
            writer.putFlag(flagPosition);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void removeFlag(Position flagPosition) {
        try {
            writer.removeFlag(flagPosition);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void changeScanState(boolean readyToScan) {
        try {
            writer.changeScanState(readyToScan);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void changeScanRadiusIndex(int scanRadiusIndex) {
        try {
            writer.changeScanRadiusIndex(scanRadiusIndex);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void kickMember(Player member) {
        try {
            writer.kickMember(member);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void changeLeader(Player newLeader) {
        try {
            writer.changeLeader(newLeader);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void leaveGroup() {
        try {
            writer.leaveGroup();
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void getTreasure(Position treasurePosition) {
        try {
            writer.getTreasure(treasurePosition);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void putTreasure(Treasure treasure) {
        try {
            writer.putTreasure(treasure);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void acceptInvitation(String groupName) {
        try {
            writer.acceptInvitation(groupName);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void createGroup() {
        try {
            writer.createGroup();
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void invitePlayerToGroup(String playerName) {
        try {
            writer.invitePlayerToGroup(playerName);
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    @Override
    public void addListener(ServerCommunicatorListener listener) {
        reader.addListener(listener);
    }

    @Override
    public void removeListener(ServerCommunicatorListener listener) {
        reader.removeListener(listener);
    }
}
