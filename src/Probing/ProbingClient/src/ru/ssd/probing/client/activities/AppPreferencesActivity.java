package ru.ssd.probing.client.activities;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import ru.ssd.probing.client.R;
import ru.ssd.probing.client.clientaux.prefs.ProbingPlayerPreferences;
import ru.ssd.probing.common.model.ProbeProperties;

/**
 * © Georgy Schukin
 * Activity to show and set personal settings
 */

public class AppPreferencesActivity extends PreferenceActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        ListPreference radiusPref = (ListPreference)findPreference(getString(R.string.pref_radius_key));

        int sz = ProbeProperties.possibleRadius.size();

        String entries[] = new String[sz];
        String values[] = new String[sz];

        for(int i = 0;i < sz;i++) {
            entries[i] = Integer.toString(ProbeProperties.possibleRadius.get(i));
            values[i] = Integer.toString(i);
        }

        radiusPref.setEntries(entries);
        radiusPref.setEntryValues(values);
    }

    @Override
    protected void onStart() {
        super.onStart();

        ListPreference radiusPref = (ListPreference)findPreference(getString(R.string.pref_radius_key));
        ProbingPlayerPreferences playerPreferences = new ProbingPlayerPreferences(this, PreferenceManager.getDefaultSharedPreferences(this));

        if(playerPreferences.isInGroup()) { // if player is in group
            radiusPref.setEnabled(true);
        } else {
            radiusPref.setEnabled(false);
        }
    }
}