package ru.ssd.probing.client.clientaux.items;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;

/**
 * © Georgy Schukin
 * Generic item on the map
 */

public class MapItem {
    protected GeoPoint position; // position of the item on the screen
    protected String title; // name of the item
    protected Rect borderRect;

    public MapItem(GeoPoint position, String title) {
        this.position = position;
        this.title = title;
    }

    public final void setPosition(GeoPoint position) {
        this.position = position;
    }

    public final void setTitle(String title) {
        this.title = title;
    }

    public final void setBorderRect(Rect borderRect) {
        this.borderRect = borderRect;
    }

    public final GeoPoint getPosition() {
        return position;
    }

    public final String getTitle() {
        return title;
    }

    public final Rect getBorderRect() {
        return borderRect;
    }

    public static Rect centerBorderRect(int width, int height) {
        return new Rect(-width/2, -height/2, width/2, height/2);
    }

    public static Rect centerBorderRect(Drawable icon) {
        if(icon instanceof BitmapDrawable) {
            BitmapDrawable bitmap = (BitmapDrawable)icon;
            return centerBorderRect(bitmap.getBitmap().getWidth(), bitmap.getBitmap().getHeight());
        } else {
            return centerBorderRect(icon.getMinimumWidth(), icon.getMinimumHeight());
        }
    }

    public static Rect centerBottomBorderRect(int width, int height) {
        return new Rect(-width/2, -height, width/2, 0);
    }

    public static Rect centerBottomBorderRect(Drawable icon) {
        if(icon instanceof BitmapDrawable) {
            BitmapDrawable bitmap = (BitmapDrawable)icon;
            return centerBottomBorderRect(bitmap.getBitmap().getWidth(), bitmap.getBitmap().getHeight());
        } else {
            return centerBottomBorderRect(icon.getMinimumWidth(), icon.getMinimumHeight());
        }
    }

    public static Rect centerLeftBottomBorderRect(int width, int height) {
        return new Rect(0, -height, width, 0);
    }

    public static Rect centerLeftBottomBorderRect(Drawable icon) {
        if(icon instanceof BitmapDrawable) {
            BitmapDrawable bitmap = (BitmapDrawable)icon;
            return centerLeftBottomBorderRect(bitmap.getBitmap().getWidth(), bitmap.getBitmap().getHeight());
        } else {
            return centerLeftBottomBorderRect(icon.getMinimumWidth(), icon.getMinimumHeight());
        }
    }

    public void draw(Canvas canvas, MapView mapView, boolean shadow) {}
}
