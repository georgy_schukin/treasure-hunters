package ru.ssd.probing.client.clientaux.prefs;

import android.app.Activity;
import android.content.SharedPreferences;

/**
 * © Georgy Schukin
 * Shared preferences for activities
 */

public class ProbingPreferences
{
    protected Activity activity;
    protected SharedPreferences sharedPreferences;

    public ProbingPreferences(Activity activity, SharedPreferences sharedPreferences) {
        this.activity = activity;
        this.sharedPreferences = sharedPreferences;
    }

    public Activity getActivity() {
        return activity;
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }
}

