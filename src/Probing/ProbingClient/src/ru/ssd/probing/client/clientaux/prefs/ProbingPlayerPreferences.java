package ru.ssd.probing.client.clientaux.prefs;

import android.app.Activity;
import android.content.SharedPreferences;
import ru.ssd.probing.client.R;

/**
 * © Georgy Schukin
 * Shared preferences for player
 */

public class ProbingPlayerPreferences extends ProbingPreferences
{
    private String playerName = "";
    private String googleId = "";
    private String probeId = "";
    private boolean isLeader;
    private boolean isInGroup;

    public ProbingPlayerPreferences(Activity activity, SharedPreferences sharedPreferences) {
        super(activity, sharedPreferences);
        update();
    }

    public final void setPlayerName(String playerName) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(getActivity().getString(R.string.pref_name_key), playerName);
        edit.commit();
        this.playerName = playerName;
    }

    public final void setGoogleId(String googleId) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(getActivity().getString(R.string.pref_google_id_key), googleId);
        edit.commit();
        this.googleId = googleId;
    }

    public final void setProbeId(String probeId) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(getActivity().getString(R.string.pref_probe_id_key), probeId);
        edit.commit();
        this.probeId = probeId;
    }

    public final void setLeader(boolean leader) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean(getActivity().getString(R.string.pref_is_leader_key), leader);
        edit.commit();
        this.isLeader = leader;
    }

    public final void setInGroup(boolean inGroup) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putBoolean(getActivity().getString(R.string.pref_is_in_group_key), inGroup);
        edit.commit();
        this.isInGroup = inGroup;
    }

    public final String getPlayerName() {
        return playerName;
    }

    public final String getGoogleId() {
        return googleId;
    }

    public final String getProbeId() {
        return probeId;
    }

    public final boolean isLeader() {
        return isLeader;
    }

    public final boolean isInGroup() {
        return isInGroup;
    }

    public void update() {
        playerName = sharedPreferences.getString(activity.getString(R.string.pref_name_key), "Me");
        googleId = sharedPreferences.getString(activity.getString(R.string.pref_google_id_key), "12345");
        probeId = sharedPreferences.getString(activity.getString(R.string.pref_probe_id_key), "12345");
        isLeader = sharedPreferences.getBoolean(activity.getString(R.string.pref_is_leader_key), false);
        isInGroup = sharedPreferences.getBoolean(activity.getString(R.string.pref_is_in_group_key), false);
    }
}



