package ru.ssd.probing.client.chat;

/**
 * Chat listener
 */

public interface ChatListener {
    public void onChatUpdate(String message);
}
