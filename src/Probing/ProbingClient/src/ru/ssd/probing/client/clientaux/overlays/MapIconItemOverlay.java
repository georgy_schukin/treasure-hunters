package ru.ssd.probing.client.clientaux.overlays;

/*
* © Georgy Schukin
 * Overlay of icon items
 */

import android.graphics.drawable.Drawable;
import com.google.android.maps.MapView;
import ru.ssd.probing.client.clientaux.items.MapIconItem;
import ru.ssd.probing.client.clientaux.items.MapItem;
import ru.ssd.probing.client.clientaux.managers.ObjectManager;

import java.util.ArrayList;
import java.util.List;

public class MapIconItemOverlay extends MapItemOverlay {
    protected List<Drawable> icons = new ArrayList<Drawable>();

    public MapIconItemOverlay(ObjectManager manager, MapView mapView, Drawable defaultIcon) {
        super(manager, mapView);
        icons.add(defaultIcon);
    }

    public MapIconItemOverlay(ObjectManager manager, MapView mapView, List<Drawable> defaultIcons) {
        super(manager, mapView);
        icons.addAll(defaultIcons);
    }

    @Override
    public synchronized void addItem(MapItem item) {
        if(item instanceof MapIconItem) {
            MapIconItem mapIconItem = (MapIconItem)item;
            if(mapIconItem.getIcon() == null) { // icon is empty
                int iconIndex = mapIconItem.getIconIndex(icons.size());
                if(iconIndex >= 0 && iconIndex < icons.size())
                    mapIconItem.setIcon(icons.get(iconIndex)); // set icon for item
            }
        }
        super.addItem(item);
    }
}
