package ru.ssd.probing.client.communication;

import ru.ssd.probing.common.model.*;
import ru.ssd.probing.common.protocol.ServerResponse;

import java.util.List;
import java.util.Set;

/**
 * © Ruslan Mustakov
 */


public interface ServerCommunicatorListener {
    //FlagPut
    //latitude
    //longitude
    //integer (color)
    //0|1 (leader)
    public void onFlagPut(Flag flag);

    //LFGPlayer
    //google_id
    //name (string)
    //latitude
    //longitude
    //0|1
    public void onLFGPlayer(Player player, boolean isLFG);

    //TeammatePositionChanged
    //google_id
    //latitude
    //longitude
    public void onTeammatePositionChanged(String teammateGoogleId, Position newPosition);

    //TeammateScanRadiusChanged
    //google_id
    //integer (scanRadiusIndex)
    public void onTeammateScanRadiusChanged(String teammateGoogleId, int scanRadiusIndex);

    //TeammateScanReadyStateChanged
    //google_id
    //0|1
    public void onTeammateScanReadyStateChanged(String teammateGoogleId, boolean readyToScan);

    //TeammateLeft
    //google_id
    public void onTeammateLeft(String teammateGoogleId);

    //LeaderChanged
    //google_id
    public void onLeaderChanged(String teammateGoogleId);

    //GroupScanStarted
    public void onGroupScanStarted();

    //FoundSignals
    //integer (number of signals)
    //strength(int)
    //latitude
    //longitude
    //...
    public void onFoundSignals(List<Signal> signals);

    //GroupAssigned
    //string (group name)
    //0|1 (automaticallyAcceptingNewMembers)
    //0|1 (interestedInNewMembers)
    //google_id (leader)
    //integer (total number of players in the group with leader)
    //google_id
    //name (string)
    //latitude
    //longitude
    //radius index
    //scan strength
    //...
    //integer (number of flags)
    //latitude
    //longitude
    //integer (color)
    //...
    public void onGroupAssigned(Group group);

    //Treasure
    //latitude
    //longitude
    //integer (treasure type)
    //integer (buffer length)
    //byte stream
    public void onTreasureFound(Treasure treasure);

    //Response
    //integer (error code)
    public void onResponse(ServerResponse response);

    //NewTeammate
    //google_id
    //nick_name
    //latitude
    //longitude
    //radius index
    //scan strength
    public void onNewTeammate(Player player);

    //InvitationReceived
    //groupName
    //memberCount (integer)
    //name
    //...
    public void onInvitationReceived(String groupName, Set<String> memberNames);

    //LeftGroup
    public void onLeftGroup();

    //FlagRemoved
    //latitude
    //longitude
    public void onFlagRemoved(Position flagPosition);

}
