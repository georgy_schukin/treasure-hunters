package ru.ssd.probing.client.clientaux.managers;

import com.google.android.maps.GeoPoint;
import ru.ssd.probing.client.clientaux.items.FlagItem;
import ru.ssd.probing.client.clientaux.overlays.MapItemOverlay;
import ru.ssd.probing.common.model.Flag;
import ru.ssd.probing.common.model.Position;

import java.util.ArrayList;

/**
 * © Georgy Schukin
 * Class to hold and manage flags data
 */

public class FlagManager implements ObjectManager{
    private ArrayList<Flag> flags = new ArrayList<Flag>();

    public FlagManager() {}

    public synchronized final void addFlag(Flag flag) {
        flags.add(flag);
    }

    public synchronized final void removeFlag(Flag flag) {
        flags.remove(flag);
    }

    public synchronized final void clearFlags() {
        flags.clear();
    }

    public synchronized final Flag getFlag(GeoPoint geoPoint) { // get flag by position
        for(Flag f : flags) {
            if((f.getPosition().getLatitude() == geoPoint.getLatitudeE6()) &&
                    (f.getPosition().getLongitude() == geoPoint.getLongitudeE6()))
                return f;
        }
        return null;
    }

    public synchronized final Flag getFlag(Position position) { // get flag by position
        for(Flag f : flags) {
            if((f.getPosition().getLatitude() == position.getLatitude()) &&
                    (f.getPosition().getLongitude() == position.getLongitude()))
                return f;
        }
        return null;
    }

    @Override
    public synchronized void updateOverlay(MapItemOverlay overlay) {
        for(Flag flag : flags) {
            GeoPoint pos = new GeoPoint(flag.getPosition().getLatitude(), flag.getPosition().getLongitude());
            overlay.addItem(new FlagItem(pos, "", flag.isLeaderFlag()));
        }
    }
}
