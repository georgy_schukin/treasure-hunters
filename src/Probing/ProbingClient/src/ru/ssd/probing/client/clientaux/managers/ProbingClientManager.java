package ru.ssd.probing.client.clientaux.managers;

/*
 * Manager of all game objects for client app
 */

import ru.ssd.probing.common.model.*;

import java.util.List;

public class ProbingClientManager {
    private final PlayerManager playerManager;
    private final FlagManager flagManager;
    private final SignalManager signalManager;
    private final TreasureManager treasureManager;

    public ProbingClientManager(Player ownerPlayer) {
        playerManager = new PlayerManager(ownerPlayer);
        flagManager = new FlagManager();
        signalManager = new SignalManager();
        treasureManager = new TreasureManager();
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    public FlagManager getFlagManager() {
        return flagManager;
    }

    public SignalManager getSignalManager() {
        return signalManager;
    }

    public TreasureManager getTreasureManager() {
        return treasureManager;
    }

    public Player getOwnerPlayer() {
        return playerManager.getOwnerPlayer();
    }

    public Group getOwnerGroup() {
        return playerManager.getGroup();
    }

    public void reset() { // reset game state
        playerManager.setGroup(new Group(""), false); // set empty group
        flagManager.clearFlags();
        signalManager.clearSignals();
        getOwnerPlayer().setLFG(false);
        getOwnerPlayer().setReadyToScan(false);
    }

    public boolean isInGroup() {
        return playerManager.isInGroup(getOwnerPlayer());
    }

    public boolean isLeader() {
        return playerManager.isLeader(getOwnerPlayer());
    }

    public void onFlagPut(Flag flag) {
        flagManager.addFlag(flag);
        getOwnerGroup().addFlag(flag);
    }

    public void onFlagRemoved(Position flagPosition) {
        Flag flag = flagManager.getFlag(flagPosition);
        if(flag != null) {
            flagManager.removeFlag(flag);
            getOwnerGroup().removeFlag(flag);
        }
    }

    public void onLFGPlayer(Player player, boolean isLFG) {
        if(!player.equals(getOwnerPlayer())) {
            if(isLFG)
                playerManager.addPlayer(player);
            else
                playerManager.removePlayer(player);
        } else {
            getOwnerPlayer().setLFG(isLFG);
        }
    }

    public void onNewTeammate(Player player) {
        playerManager.addPlayerToGroup(player);
    }

    public void onTeammatePositionChanged(String teammateGoogleId, Position newPosition) {
        Player player = playerManager.getPlayer(teammateGoogleId);
        player.setPosition(newPosition);
    }

    public void onTeammateScanRadiusChanged(String teammateGoogleId, int scanRadiusIndex) {
        Player player = playerManager.getPlayer(teammateGoogleId);
        int scanStrength = player.getProbeProperties().getScanStrength();
        player.setProbeProperties(new ProbeProperties(scanRadiusIndex, scanStrength));
    }

    public void onTeammateScanReadyStateChanged(String teammateGoogleId, boolean readyToScan) {
        Player player = playerManager.getPlayer(teammateGoogleId);
        player.setReadyToScan(readyToScan);
    }

    public void onTeammateLeft(String teammateGoogleId) {
        Player player = playerManager.getPlayer(teammateGoogleId);
        if(!player.equals(getOwnerPlayer()))
            playerManager.removePlayer(player);
        else
            playerManager.removePlayerFromGroup(player);
    }

    public void onLeaderChanged(String teammateGoogleId) {
        Player player = playerManager.getPlayer(teammateGoogleId);
        playerManager.getGroup().changeLeader(player);
    }

    /*public void onGroupScanStarted() {
        playerManager.startScanning();
    }*/

    public void onFoundSignals(List<Signal> signals) {
        signalManager.clearSignals();
        signalManager.addSignals(signals);
    }

    public void onGroupAssigned(Group group) {
        playerManager.setGroup(group, true);
        //getOwnerPlayer().setLFG(false);
    }

    public void onTreasureFound(Treasure treasure) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    /*public void leaveGroup() {
        playerManager.setGroup(new Group(""), false); // set empty group
    }*/

    /*public void onResponse(ServerResponse response) {
        //To change body of implemented methods use File | Settings | File Templates.
    } */
}
