package ru.ssd.probing.client.clientaux.items;

import android.graphics.drawable.Drawable;
import com.google.android.maps.GeoPoint;

/**
 * © Georgy Schukin
 * Signal item on the map
 */

public class SignalItem extends MapIconItem {
    protected int strength;

    public SignalItem(GeoPoint position, String title, int strength) {
        super(position, title);
        this.strength = strength;
    }

    public SignalItem(GeoPoint position, String title, Drawable icon, int strength) {
        super(position, title, icon);
        this.strength = strength;
    }

    public int getStrength() {
        return strength;
    }

    @Override
    public int getIconIndex(int size) {
        if(size <= 1) return size - 1;
        if(strength == 100) return size - 1; // return last (treasure)
        return (int)(((float)strength/100.0f)*(size - 1)); // get signal icon according to strength
    }
}
