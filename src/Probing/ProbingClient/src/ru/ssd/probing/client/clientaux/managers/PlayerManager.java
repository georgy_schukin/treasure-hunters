package ru.ssd.probing.client.clientaux.managers;

import com.google.android.maps.GeoPoint;
import ru.ssd.probing.client.clientaux.items.PlayerItem;
import ru.ssd.probing.client.clientaux.overlays.MapItemOverlay;
import ru.ssd.probing.common.model.Group;
import ru.ssd.probing.common.model.Player;

import java.util.HashSet;
import java.util.Set;

/**
 * © Georgy Schukin
 * Class to hold and manage players datas
 */

public class PlayerManager implements ObjectManager {
    private Player ownerPlayer; // owner of device
    private Group ownerGroup = new Group(""); // group of device owner player
    private Set<Player> players = new HashSet<Player>(); // all players (group + lfg)
    //private boolean groupIsScanning = false;

    public PlayerManager(Player ownerPlayer) throws IllegalArgumentException {
        if(ownerPlayer == null)
            throw new IllegalArgumentException("Owner player is null!");
        if(ownerPlayer.getGoogleId().length() == 0)
            throw new IllegalArgumentException("Owner player id is empty!");
        this.ownerPlayer = ownerPlayer;
        players.add(ownerPlayer); // add to players
    }

    public final Player getOwnerPlayer() {
        return ownerPlayer;
    }

    public synchronized final void reset() {
        players.clear(); // delete all players
        ownerGroup = new Group(""); // delete group
        players.add(ownerPlayer); // leave only me as player
    }

    public synchronized final boolean isInGroup(Player player) {
        return ownerGroup.getPlayers().contains(player); // check for other player
    }

    public synchronized final boolean isLeader(Player player) {
        return player.equals(ownerGroup.getLeader());
    }

    public synchronized final void setGroup(Group group, boolean includeMe) { // set new group
        players.removeAll(ownerGroup.getPlayers()); // remove old players from players list
        ownerGroup = group; // change group
        for(Player p : ownerGroup.getPlayers()) // search and remove main player
            if(p.getGoogleId().equals(ownerPlayer.getGoogleId()) &&
                    p != ownerGroup.getLeader()) { // not leader
                ownerGroup.removePlayer(p);
                break;
            }
        if(includeMe) {
            ownerGroup.addPlayer(ownerPlayer); // add us to group
            if(ownerGroup.getLeader() == null) { // there is no leader - we are the leader then
                ownerGroup.changeLeader(ownerPlayer);
            }
        }
        players.add(ownerPlayer);
        players.addAll(ownerGroup.getPlayers()); // add players from new group to players list
    }

    public synchronized final Group getGroup() {
        return ownerGroup;
    }

    public synchronized final void addPlayer(Player player) {
         players.add(player);
    }

    public synchronized final void addPlayerToGroup(Player player) {
        ownerGroup.addPlayer(player);
        players.add(player);
    }

    public synchronized final Player getPlayer(String playerGoogleId) throws IllegalArgumentException {
        for(Player p : players) {
            if(p.getGoogleId().equals(playerGoogleId))
                return p;
        }
        throw new IllegalArgumentException("Player with id " + playerGoogleId + " doesn't exist!");
    }

    public synchronized final Player getPlayerByName(String playerName) throws IllegalArgumentException {
        for(Player p : players) {
            if(p.getName().equals(playerName))
                return p;
        }
        throw new IllegalArgumentException("Player with name " + playerName + " doesn't exist!");
    }

    public synchronized final void removePlayer(Player player) throws IllegalArgumentException { // delete player
        if(player.equals(ownerPlayer))
            throw new IllegalArgumentException("Can't delete owner player!");
        ownerGroup.removePlayer(player); // remove from group (if is in group)
        players.remove(player); // remove from players list
    }

    public synchronized final void removePlayerFromGroup(Player player) { // remove only from group
        ownerGroup.removePlayer(player);
    }

    @Override
    public synchronized void updateOverlay(MapItemOverlay overlay) {
        Player leader = ownerGroup.getLeader();
        for(Player player : players) { // add players to overlay
            GeoPoint pos = new GeoPoint(player.getPosition().getLatitude(), player.getPosition().getLongitude());
            int scanRadius = player.getProbeProperties().getScanRadius();
            boolean isOwner = player.equals(ownerPlayer);
            boolean isLeader = player.equals(leader);
            boolean isInGroup = isInGroup(player);
            boolean isLFG = player.isLFG();
            PlayerItem.Mode mode;
            if(isInGroup) {
                mode = player.isReadyToScan() ? PlayerItem.Mode.SCAN_READY : PlayerItem.Mode.IN_GROUP;
            } else
                mode = isLFG ? PlayerItem.Mode.LFG : PlayerItem.Mode.NEW;

            overlay.addItem(new PlayerItem(pos, player.getName(), ownerGroup.getName(),
                    scanRadius, isOwner, isLeader, mode));
        }
    }
}
