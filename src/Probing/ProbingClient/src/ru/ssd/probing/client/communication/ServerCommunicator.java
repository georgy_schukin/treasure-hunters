package ru.ssd.probing.client.communication;

import ru.ssd.probing.common.model.GroupProperties;
import ru.ssd.probing.common.model.Player;
import ru.ssd.probing.common.model.Position;
import ru.ssd.probing.common.model.Treasure;

/**
 * © Ruslan Mustakov
 */


public interface ServerCommunicator {


    //communication protocol described in the comments
    //the first message client sends to server is:
    //newPlayer
    //google_id
    //probe_id
    //latitude
    //longitude


    //newGroup
    //name (string)
    //leader or creator (google_id)
    //0|1 (automaticallyAcceptingNewMembers)
    //0|1 (interestedInNewMembers)

    //changeLFGState
    //0|1
    public void changeLFGState(boolean isLFG);

    //changePosition
    //latitude
    //longitude
    public void changePosition(Position newPosition);

    //changeGroupProperties
    //0|1 (automaticallyAcceptingNewMembers)
    //0|1 (interestedInNewMembers)
    public void changeGroupProperties(GroupProperties newProperties);

    //putFlag
    //latitude
    //longitude
    public void putFlag(Position flagPosition);

    //changeScanState
    //0|1
    public void changeScanState(boolean readyToScan);

    //changeScanRadiusIndex
    //integer value
    public void changeScanRadiusIndex(int scanRadiusIndex);

    //kickMember
    //probe_id (as String)
    public void kickMember(Player member);

    //changeLeader
    //google_id (as String)
    public void changeLeader(Player newLeader);

    //leaveGroup
    public void leaveGroup();

    //getTreasure
    //latitude
    //longitude
    public void getTreasure(Position treasurePosition);

    //putTreasure
    //latitude
    //longitude
    public void putTreasure(Treasure treasure);

    //createGroup
    public void createGroup();

    //invitePlayerToGroup
    //name
    public void invitePlayerToGroup(String playerName);

    //acceptInvitation
    //groupName
    public void acceptInvitation(String groupName);

    //removeFlag
    //latitude
    //longitude
    public void removeFlag(Position flagPosition);

    public void addListener(ServerCommunicatorListener listener);
    public void removeListener(ServerCommunicatorListener listener);
}
