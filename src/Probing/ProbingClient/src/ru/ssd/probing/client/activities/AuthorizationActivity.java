package ru.ssd.probing.client.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import framework.Tutorial;
import ru.ssd.probing.client.R;
import ru.ssd.probing.client.clientaux.prefs.ProbingPlayerPreferences;

import java.net.URLDecoder;

/**
 * © Ruslan Mustakov
 */

public class AuthorizationActivity extends Activity
{
    private class MyProgressDialog implements Runnable
    {
        protected ProgressDialog progressDialog;
        protected final Context context;
        protected final String message;

        public MyProgressDialog(Context context, String message) {
            this.context = context;
            this.message = message;
        }

        @Override
        public void run() {
            stop();
            progressDialog = ProgressDialog.show(context, "", message);
        }

        public void stop() {
            if(progressDialog != null)
                progressDialog.dismiss();
        }
    }

    private MyProgressDialog progressDialog;

    // Google
    private final static String    goID        = "432528404452-8mo088p8aaab4etjv0j6h26cvsj99fl1.apps.googleusercontent.com"; // You will provide your Client ID  from the Google APIs Console.
    private final static String    goSECRET    = "Xh4dwwu3XfJW0QZ4XGh_vQCO"; // You will provide your Client secret from the Google APIs Console.
    private final static String    goRSPURL    = "http://ssd1.sscc.ru:6896"; // You will provide the redirect  URI from the Google APIs Console.
    private final static String    goCALLBK    = "gnuc-tutorials-oauth://gplus"; // This is a user-defined url handle which will be called by the web-service. Will invoke the application automatically.
    private final static String    goSCOPE    = "https://www.googleapis.com/auth/plus.me"; // Read about scoping @ https://developers.google.com
    private final static String    goTOKEN    = "https://accounts.google.com/o/oauth2/token"; // Read more @ http://code.google.com/apis/accounts/docs/OAuth2.html
    private final static String    goACCESS    = "https://accounts.google.com/o/oauth2/auth"; // Read more @ http://code.google.com/apis/accounts/docs/OAuth2.html
    private final static String    goPLUS    = "https://www.googleapis.com/plus/v1/people/me";  // Read about end-point @ https://developers.google.com

    //private final int AUTH = 1;
    //private final int MAP = 2;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stub);

        Tutorial.cx = this;

        progressDialog = showProgressDialog(this, "Contacting Google...");
        final String authUrl = goACCESS + "?client_id=" + goID + "&redirect_uri=" + goRSPURL + "&scope=" + Uri.encode(goSCOPE) + "&response_type=code";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(authUrl)));
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        if(intent != null) {
            onMapStart(intent);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if(progressDialog != null)
            progressDialog.stop();
    }

    private MyProgressDialog showProgressDialog(Context context, String msg)
    {
        MyProgressDialog dlg = new MyProgressDialog(context, msg);
        runOnUiThread(dlg);
        return dlg;
    }

    private void onMapStart(Intent data) {
        try {
            if(data == null)
                throw new Exception("Empty intent");
            Uri uri = (Uri)data.getData();
            if (uri != null && uri.toString().contains(goCALLBK))
            {
                String displayName = URLDecoder.decode(uri.getQueryParameter("displayname"), "UTF-8");
                String granted = URLDecoder.decode(uri.getQueryParameter("granted"), "UTF-8");

                if(granted.equalsIgnoreCase("granted")) {
                    Toast.makeText(AuthorizationActivity.this, "Authorization granted to " + displayName, Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(AuthorizationActivity.this, "Authorization failed... - "+displayName+"-"+granted, Toast.LENGTH_LONG).show();
                    throw new Exception();
                }

                String googleId = URLDecoder.decode(uri.getQueryParameter("googleid"), "UTF-8");
                String probeId = URLDecoder.decode(uri.getQueryParameter("probeid"), "UTF-8");

                ProbingPlayerPreferences playerPreferences =
                        new ProbingPlayerPreferences(AuthorizationActivity.this,
                                PreferenceManager.getDefaultSharedPreferences(AuthorizationActivity.this)); // get prefs

                playerPreferences.setPlayerName(displayName);
                playerPreferences.setGoogleId(googleId);
                playerPreferences.setProbeId(probeId);

                startActivity(new Intent(this, ProbingMapActivity.class));
            }
        }
        catch (Exception e) {
            Log.e("Auth error", e.getMessage(), e);
        }
    }
}