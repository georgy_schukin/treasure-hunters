package ru.ssd.probing.client.clientaux.items;

import android.graphics.drawable.Drawable;
import com.google.android.maps.GeoPoint;

/**
 * © Georgy Schukin
 * Item to represent flag on the map
 */

public class FlagItem extends MapIconItem {
    private boolean isLeader;

    public FlagItem(GeoPoint position, String title, boolean isLeader) {
        super(position, title);
        this.isLeader = isLeader;
    }

    public boolean isLeaderFlag() {
        return isLeader;
    }

    /*public FlagItem(GeoPoint position, String title, Drawable icon) {
        super(position, title);
        this.icon = icon;
        this.borderRect = MapItem.centerLeftBottomBorderRect(icon);
    }*/

    @Override
    public void setIcon(Drawable icon) {
        this.icon = icon;
        this.borderRect = MapItem.centerLeftBottomBorderRect(icon);
    }

    @Override
    public int getIconIndex(int size) {
        if(size <= 1) return size - 1;
        return isLeader ? 1 : 0;
    }
}
