package ru.ssd.probing.client.chat;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import ru.ssd.probing.client.R;
import ru.ssd.probing.client.clientaux.prefs.ProbingPlayerPreferences;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.LinkedList;

public class ChatActivity extends Activity implements ChatListener {
    //private BufferedReader in;
    private BufferedWriter out;
    //private String nickname;

    private ChatReader chatReader;
    private Thread readerThread;

    private ListView  chatList;
    private EditText  chatEdit;
    private LinkedList<String> messages = new LinkedList<String>();

    private class ChatUpdater implements Runnable
    {
        private final String message;

        public ChatUpdater(String message) {
            this.message = message;
        }
        @Override
        public void run() {
            updateChat(message);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat);

        try {
            ProbingPlayerPreferences prefs = new ProbingPlayerPreferences(this,
                    PreferenceManager.getDefaultSharedPreferences(this));

            chatEdit = (EditText)findViewById(R.id.chat_edit);
            chatList = (ListView)findViewById(R.id.chat_list);

            //TODO: get from preferences
            String nickname = prefs.getPlayerName();
            String hostName = "ssd1.sscc.ru";
            int hostPort = 6895;

            Socket socket = new Socket(hostName, hostPort);

            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            write(nickname); // send nickname

            chatReader = new ChatReader(in, this);
        }
        catch (Exception e) {
            onChatUpdate("Failed to connect to chat");
            Log.e("Chat error", e.getMessage(), e);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            if(readerThread != null) {
                chatReader.stop();
                readerThread.join();
            }
            readerThread = new Thread(chatReader);
            readerThread.setDaemon(true);
            readerThread.start();

            loadMessages();
            chatList.setAdapter(new ChatArrayAdapter(this, R.layout.chat_elem, messages)); // write all previous messages
        }
        catch (Exception e) {
            Log.e("Chat error", e.getMessage(), e);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if(readerThread != null) {
                chatReader.stop();
                readerThread.join();
                readerThread = null;
            }
            saveMessages();
        }
        catch (Exception e) {
            Log.e("Chat error", e.getMessage(), e);
        }
    }

    protected void write(String message) {
        try {
            out.write(message + "\r\n");
            out.flush();
        }
        catch (Exception e) {
            Log.e("Chat write error", e.getMessage(), e);
        }
    }

    protected void updateChat(String message) {
        try {
            messages.addFirst(message); // add message to top
            chatList.setAdapter(new ChatArrayAdapter(this, R.layout.chat_elem, messages));
        }
        catch (Exception e) {
            Log.e("Chat error", e.getMessage(), e);
        }
    }

    public void onChatSendClicked(View v) {
        try {
            String message = chatEdit.getText().toString();
            if(message.length() > 0) {
                write(message);
                chatEdit.setText("");
            }
        }
        catch (Exception e) {
            Log.e("Chat send error", e.getMessage(), e);
        }
    }

    public void onChatClearButtonClicked(View v) {
        try {
            deleteMessages();
            messages.clear();
            chatList.setAdapter(new ChatArrayAdapter(this, R.layout.chat_elem, messages)); // write all previous messages
        }
        catch (Exception e) {
            Log.e("Chat send error", e.getMessage(), e);
        }
    }

    @Override
    public void onChatUpdate(String message) {
        runOnUiThread(new ChatUpdater(message));
    }

    protected void saveMessages() {
        SharedPreferences prefs = getSharedPreferences(getString(R.string.pref_chat_messages_key), MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(getString(R.string.pref_chat_messages_size_key), messages.size());
        for(int i = 0;i < messages.size();i++)
            editor.putString(getString(R.string.pref_chat_messages_size_key) + i, messages.get(i));
        editor.commit();
    }

    protected void loadMessages() {
        SharedPreferences prefs = getSharedPreferences(getString(R.string.pref_chat_messages_key), MODE_PRIVATE);
        int size = prefs.getInt(getString(R.string.pref_chat_messages_size_key), 0);
        for(int i = 0;i < size;i++)
            messages.add(prefs.getString(getString(R.string.pref_chat_messages_size_key) + i, ""));
    }

    protected void deleteMessages() {
        SharedPreferences prefs = getSharedPreferences(getString(R.string.pref_chat_messages_key), MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }
}

