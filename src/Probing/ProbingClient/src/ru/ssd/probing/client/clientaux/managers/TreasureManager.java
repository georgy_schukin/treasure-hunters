package ru.ssd.probing.client.clientaux.managers;

import com.google.android.maps.GeoPoint;
import ru.ssd.probing.client.clientaux.items.TreasureItem;
import ru.ssd.probing.client.clientaux.overlays.MapItemOverlay;
import ru.ssd.probing.common.model.Treasure;

import java.util.ArrayList;
import java.util.List;

public class TreasureManager implements ObjectManager{
    private List<Treasure> treasures = new ArrayList<Treasure>(); // list of treasures

    public TreasureManager() {}

    public final void addTreasure(Treasure treasure) {
        treasures.add(treasure);
    }

    @Override
    public void updateOverlay(MapItemOverlay overlay) {
        for(Treasure treasure : treasures) {
            GeoPoint pos = new GeoPoint(treasure.getPosition().getLatitude(), treasure.getPosition().getLongitude());
            overlay.addItem(new TreasureItem(pos, ""));
        }
    }
}
