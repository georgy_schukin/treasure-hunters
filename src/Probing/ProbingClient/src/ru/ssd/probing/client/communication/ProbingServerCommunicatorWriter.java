package ru.ssd.probing.client.communication;

import android.util.Log;
import ru.ssd.probing.common.model.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.LinkedList;

/*
 * To write to server via socket when server comm function is called
 */

public class ProbingServerCommunicatorWriter implements Runnable {
    private BufferedWriter writer;
    private final LinkedList<String> msgQueue;
    private boolean isWorking = false;
    private Socket socket;

    public ProbingServerCommunicatorWriter(LinkedList<String> msgQueue) {
        this.msgQueue = msgQueue;
    }

    public void init(Socket clientSocket) throws IOException {
        try {
            writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream(), "UTF-8")); // write data to socket
            this.socket = clientSocket;
        }
        catch (IOException e) {
            Log.e("Comm write error", e.getMessage(), e);
            throw e; // throw further
        }
    }

    @Override
    public void run() {
        isWorking = true;
        while (isWorking) {
            String data = null;
            try {
                boolean ok;
                synchronized (msgQueue) {
                    data = msgQueue.poll(); // get new data to send
                    ok = (data != null);
                }
                if(ok) {
                    if(socket.isClosed() || !socket.isConnected())  // check socket
                        throw new IOException("Socket is closed!");
                    writer.write(data); // write to socket
                    writer.flush();
                    Log.d("Write", data);
                    data = null; // set data to null - write is successful
                } else {
                    sleep(1000); // queue is empty - sleep for some time
                }
            }
            catch (Exception e) {
                Log.e("Comm write error", e.getMessage(), e);
            }
            finally {
                if(data != null) { // data not null - error
                    synchronized (msgQueue) {
                        msgQueue.addFirst(data); // add it back to queue head
                    }
                    sleep(500);
                }
            }
        }
    }

    protected void sleep(int msec) {
        try {
            Thread.sleep(msec);
        }
        catch (Exception e) {
            Log.e("Comm write thread error", e.getMessage(), e);
        }
    }

    public final void stop() {
        isWorking = false;
    }

    public final void close() {
        try {
            if(writer != null)
                writer.close();
        }
        catch (Exception e) {
            Log.e("Comm write error", e.getMessage(), e);
        }
    }

    public synchronized final void erase() { // erase all messages in queue
        synchronized (msgQueue) {
           msgQueue.clear();
        }
    }

    public final void newPlayer(Player player) throws Exception {
        String s = put("newPlayer");
        s += put(player.getGoogleId());
        s += put(player.getProbeId());
        //s += put(player.getName());
        s += put(Integer.toString(player.getPosition().getLatitude()));
        s += put(Integer.toString(player.getPosition().getLongitude()));
        s += put(Integer.toString(player.getProbeProperties().getScanRadiusIndex()));
        s += put(Integer.toString(player.getProbeProperties().getScanStrength()));
        write(s);
    }

    public void newGroup(Group group) throws Exception {
        String s = put("newGroup");
        s += put(group.getName());
        s += put(group.getLeader().getGoogleId());
        s += put(fromBoolean(group.getGroupProperties().isAutomaticallyAcceptingNewMembers()));
        s += put(fromBoolean(group.getGroupProperties().isInterestedInNewMembers()));
        write(s);
    }

    public void changeLFGState(boolean isLFG) throws Exception {
        String s = put("changeLFGState");
        s += put(fromBoolean(isLFG));
        write(s);
    }

    public void changePosition(Position newPosition) throws Exception {
        String s = put("changePosition");
        s += put(Integer.toString(newPosition.getLatitude()));
        s += put(Integer.toString(newPosition.getLongitude()));
        write(s);
    }

    public void changeGroupProperties(GroupProperties newProperties) throws Exception {
        String s = put("changeGroupProperties");
        s += put(fromBoolean(newProperties.isAutomaticallyAcceptingNewMembers()));
        s += put(fromBoolean(newProperties.isInterestedInNewMembers()));
        write(s);
    }

    public void putFlag(Position flagPosition) throws Exception {
        String s = put("putFlag");
        s += put(Integer.toString(flagPosition.getLatitude()));
        s += put(Integer.toString(flagPosition.getLongitude()));
        write(s);
    }

    public void removeFlag(Position flagPosition) throws Exception {
        String s = put("removeFlag");
        s += put(Integer.toString(flagPosition.getLatitude()));
        s += put(Integer.toString(flagPosition.getLongitude()));
        write(s);
    }

    public void changeScanState(boolean readyToScan) throws Exception {
        String s = put("changeScanState");
        s += put(fromBoolean(readyToScan));
        write(s);
    }

    public void changeScanRadiusIndex(int scanRadiusIndex) throws Exception {
        String s = put("changeScanRadiusIndex");
        s += put(Integer.toString(scanRadiusIndex));
        write(s);
    }

    public void kickMember(Player member) throws Exception {
        String s = put("kickMember");
        s += put(member.getProbeId());
        write(s);
    }

    public void changeLeader(Player newLeader) throws Exception {
        String s = put("changeLeader");
        s += put(newLeader.getGoogleId());
        write(s);
    }

    public void leaveGroup() throws Exception {
        String s = put("leaveGroup");
        write(s);
    }

    public void getTreasure(Position treasurePosition) throws Exception {
        String s = put("getTreasure");
        s += put(Integer.toString(treasurePosition.getLatitude()));
        s += put(Integer.toString(treasurePosition.getLongitude()));
        write(s);
    }

    public void putTreasure(Treasure treasure) throws Exception {
        String s = put("putTreasure");
        s += put(Integer.toString(treasure.getPosition().getLatitude()));
        s += put(Integer.toString(treasure.getPosition().getLongitude()));
        write(s);
    }

    public void acceptInvitation(String groupName) throws Exception {
        String s = put("acceptInvitation");
        s += put(groupName);
        write(s);
    }

    public void createGroup() throws Exception {
        String s = put("createGroup");
        write(s);
    }

    public void invitePlayerToGroup(String playerName) throws Exception {
        String s = put("invitePlayerToGroup");
        s += put(playerName);
        write(s);
    }

    protected String put(String add) {
        return add + "\r\n";
    }

    protected synchronized void write(String data) throws Exception {
        synchronized (msgQueue) {
            msgQueue.offer(data);
        }
    }

    protected String fromBoolean(boolean b) {
        return b ? "1" : "0";
    }
}
