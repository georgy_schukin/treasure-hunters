package ru.ssd.probing.client.clientaux.overlays;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import ru.ssd.probing.client.clientaux.items.MapItem;
import ru.ssd.probing.client.clientaux.managers.ObjectManager;

import java.util.ArrayList;
import java.util.List;

/**
 * © Georgy Schukin
 * Overlay to draw different custom items over a map
 */

public class MapItemOverlay extends Overlay {
    protected ArrayList<MapItem> mapItems = new ArrayList<MapItem>();
    protected List<OverlayListener> listeners = new ArrayList<OverlayListener>();
    protected ObjectManager manager;
    protected MapView mapView;

    public MapItemOverlay(ObjectManager manager, MapView mapView) {
        this.manager = manager;
        this.mapView = mapView;
    }

    public synchronized void clearItems() {
        mapItems.clear();
    }

    public synchronized void addItem(MapItem item) {
        mapItems.add(item);
    }

    public synchronized void update() {
        clearItems();
        manager.updateOverlay(this);
        mapView.postInvalidate();
    }

    protected synchronized boolean isHit(MapItem mapItem, Point hitPoint, MapView mapView) { // check for hit
        Point itemPoint = mapView.getProjection().toPixels(mapItem.getPosition(), null);
        Rect borderRect = mapItem.getBorderRect();
        Rect itemRect = new Rect(itemPoint.x + borderRect.left, itemPoint.y + borderRect.top,
                    itemPoint.x + borderRect.right, itemPoint.y + borderRect.bottom);
        return  itemRect.contains(hitPoint.x, hitPoint.y);
    }

    protected synchronized MapItem getHitItem(GeoPoint geoPoint, MapView mapView) { // get first hit item
        Point hitPoint = mapView.getProjection().toPixels(geoPoint, null);
        for(MapItem item : mapItems) {
            if(isHit(item, hitPoint, mapView)) return item;
        }
        return null;
    }

    protected synchronized List<MapItem> getHitItems(GeoPoint geoPoint, MapView mapView) {
        List<MapItem> hitItems = new ArrayList<MapItem>();
        Point hitPoint = mapView.getProjection().toPixels(geoPoint, null);
        for(MapItem item : mapItems) {
            if(isHit(item, hitPoint, mapView)) hitItems.add(item);
        }
        return hitItems;
    }

    public void addListener(OverlayListener listener) {
        listeners.add(listener);
    }

    public void removeListener(OverlayListener listener) {
        listeners.remove(listener);
    }

    @Override
    public boolean onTap(GeoPoint geoPoint, MapView mapView) {
        //MapItem hitItem = getHitItem(geoPoint, mapView); // check if item is hit
        List<MapItem> hitItems = getHitItems(geoPoint, mapView); // get all hit items
        boolean res = false;
        if(hitItems.size() > 0) { // something is hit
            for(OverlayListener l : listeners) {
                for(MapItem item : hitItems)
                res = res || l.onTap(this, item);
            }
        } else { // empty point is hit
            for(OverlayListener l : listeners)
                res = res || l.onTap(this, geoPoint);
        }
        return res;
    }

    @Override
    public synchronized void draw(Canvas canvas, MapView mapView, boolean shadow) {
        for(MapItem mapItem : mapItems) {
            mapItem.draw(canvas, mapView, shadow);
        }
    }

    public interface OverlayListener {
        public boolean onTap(MapItemOverlay sender, GeoPoint geoPoint);
        public boolean onTap(MapItemOverlay sender, MapItem item);
    }
}

