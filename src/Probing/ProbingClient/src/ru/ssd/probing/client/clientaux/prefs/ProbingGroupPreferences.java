package ru.ssd.probing.client.clientaux.prefs;

import android.app.Activity;
import android.content.SharedPreferences;
import ru.ssd.probing.client.R;

/**
 * © Georgy Schukin
 * Shared preferences for group
 */

public class ProbingGroupPreferences extends ProbingPreferences
{
    private String groupName;
    private boolean automaticallyAcceptingNewMembers;
    private boolean interestedInNewMembers;

    public ProbingGroupPreferences(Activity activity, SharedPreferences sharedPreferences) {
        super(activity, sharedPreferences);
        update();
    }

    public ProbingGroupPreferences(ProbingGroupPreferences groupPreferences) {
        super(groupPreferences.getActivity(), groupPreferences.getSharedPreferences());
        this.groupName = groupPreferences.getGroupName();
        this.automaticallyAcceptingNewMembers = groupPreferences.isAutomaticallyAcceptingNewMembers();
        this.interestedInNewMembers = groupPreferences.isInterestedInNewMembers();
    }

    public final String getGroupName() {
        return groupName;
    }

    public final boolean isAutomaticallyAcceptingNewMembers() {
        return automaticallyAcceptingNewMembers;
    }

    public final boolean isInterestedInNewMembers() {
        return interestedInNewMembers;
    }

    public void update() {
        groupName = sharedPreferences.getString(activity.getString(R.string.pref_group_name_key), "MyGroup");
        automaticallyAcceptingNewMembers = sharedPreferences.getBoolean(activity.getString(R.string.pref_group_auto_key), true);
        interestedInNewMembers = sharedPreferences.getBoolean(activity.getString(R.string.pref_group_interest_key), true);
    }
}

