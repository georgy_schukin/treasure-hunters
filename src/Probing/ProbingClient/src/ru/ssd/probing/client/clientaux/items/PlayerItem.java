package ru.ssd.probing.client.clientaux.items;

import android.graphics.*;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;

/**
 * © Georgy Schukin
 * Item to represent player on the map
 */

public class PlayerItem extends MapIconItem {
    private int scanRadius; // current scanning radius
    private boolean isOwner; // if this is device owner
    private boolean isLeader; // if this is group leader

    public enum Mode {
        NEW,
        LFG,
        IN_GROUP,
        SCAN_READY
        //SCANNING
    }

    private Mode mode;
    private String groupName = null;

    private Paint radiusPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint radiusBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint titlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint titleBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint radiusTitlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public PlayerItem(GeoPoint position, String title, String groupName, int scanRadius, boolean isOwner, boolean isLeader, Mode mode) {
        super(position, title);
        this.groupName = groupName;
        this.scanRadius = scanRadius;
        this.isOwner = isOwner;
        this.isLeader = isLeader;
        this.mode = mode;
        initPaints();
        //this.borderRect = MapItem.centerBorderRect(20, 20);
    }

    protected final void initPaints() {
        int r = 0, g = 50, b = 0;
        if(title.length() > 0) {
            r = ((int)title.charAt(0)) % 255;
            g = Math.abs(title.hashCode()) % 255;
            b = ((int)title.charAt(title.length() - 1)) % 255;
        }
        radiusPaint.setARGB(80, r, g, b); // use hash to change color
        radiusPaint.setStyle(Paint.Style.FILL);

        radiusBorderPaint.setARGB(100, 0, 0, 50);
        radiusBorderPaint.setStrokeWidth(2);
        radiusBorderPaint.setStyle(Paint.Style.STROKE);

        titlePaint.setARGB(255, 20, 20, 20);
        titlePaint.setTextSize(14);
        titlePaint.setTypeface(isOwner ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
        titlePaint.setStyle(Paint.Style.STROKE);

        titleBorderPaint.setARGB(200, 255, 255, 255);
        titleBorderPaint.setTextSize(14);
        titleBorderPaint.setStrokeWidth(3);
        titleBorderPaint.setTypeface(isOwner ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
        titleBorderPaint.setStyle(Paint.Style.STROKE);

        radiusTitlePaint.setARGB(255, 10, 10, 10);
        radiusTitlePaint.setTextSize(12);
        radiusTitlePaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    public int getIconIndex(int size) {
        if(size <= 1) return 0;
        return isLeader ? 1 : 0;
    }

    protected float getDistance(GeoPoint p1, GeoPoint p2) {  // hell formula to count distance
        final float Rad = 6367450;

        double lat1 = Math.toRadians(((double)p1.getLatitudeE6()/1E6));
        double lat2 = Math.toRadians(((double)p2.getLatitudeE6()/1E6));
        double lon1 = Math.toRadians(((double)p1.getLongitudeE6()/1E6));
        double lon2 = Math.toRadians(((double)p2.getLongitudeE6()/1E6));

        double lat = lat2 - lat1;
        double lon = lon2 - lon1;

        double s1 = Math.sin(lat/2.0);
        double s2 = Math.sin(lon/2.0);
        double c1 = Math.cos(lat1);
        double c2 = Math.cos(lat2);

        double a = s1*s1 + c1*c2*s2*s2;
        double b = 2.0*Math.atan2(Math.sqrt(a), Math.sqrt(1.0 - a));
        return (float)(Rad*b);
        //return (float)(2*Rad*Math.asin(Math.sqrt(a)));
    }

    protected float getRadiusInPixels(MapView mapView, GeoPoint point, float rad) {
        Point p = new Point();
        mapView.getProjection().toPixels(point, p);
        GeoPoint point1 = mapView.getProjection().fromPixels(0, 0);
        GeoPoint point2 = mapView.getProjection().fromPixels(0, 1);

        float d = getDistance(point1, point2);
        return rad/d;
    }

    protected float metersToPixels(MapView mapView, GeoPoint point, float meters) {
        double lat = Math.toRadians(point.getLatitudeE6()/1E6);
        double c = 1.0/Math.cos(lat);
        return (float)(c*mapView.getProjection().metersToEquatorPixels(meters));
    }

    @Override
    public void draw(Canvas canvas, MapView mapView, boolean shadow) {
        Point pos = mapView.getProjection().toPixels(getPosition(), null);
        if(shadow) {
            if((mode == Mode.IN_GROUP) || (mode == Mode.SCAN_READY)) {
            float radius = metersToPixels(mapView, getPosition(), (float)scanRadius);
            //float radius = getRadiusInPixels(mapView, getPosition(), (float)scanRadius);
            RectF scanRect = new RectF(pos.x - radius, pos.y - radius, pos.x + radius, pos.y + radius);
            if((mode == Mode.IN_GROUP) || (mode == Mode.SCAN_READY))
                canvas.drawArc(scanRect, 0, 360, false, radiusPaint);
            if(mode == Mode.SCAN_READY)
                canvas.drawArc(scanRect, 0, 360, false, radiusBorderPaint);
            }
        } else {
            super.draw(canvas, mapView, shadow); // draw icon
            int height = getBorderRect().height()/2;
            String title = getTitle();
                if(mode == Mode.LFG)
                    title += "(?)";
                else if((mode == Mode.IN_GROUP) || (mode == Mode.SCAN_READY))
                    title += "(" + groupName + ")";
            canvas.drawText(title, pos.x + 3, pos.y - height - 3, titleBorderPaint); // draw player name + group
            canvas.drawText(title, pos.x + 3, pos.y - height - 3, titlePaint);
            if((mode == Mode.IN_GROUP) || (mode == Mode.SCAN_READY))
                canvas.drawText(Integer.toString(scanRadius), pos.x + 3, pos.y + height + 3 + radiusTitlePaint.getTextSize(), radiusTitlePaint); // draw radius num
        }
    }
}
