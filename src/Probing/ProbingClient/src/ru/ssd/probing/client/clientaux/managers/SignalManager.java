package ru.ssd.probing.client.clientaux.managers;

import com.google.android.maps.GeoPoint;
import ru.ssd.probing.client.clientaux.items.SignalItem;
import ru.ssd.probing.client.clientaux.overlays.MapItemOverlay;
import ru.ssd.probing.common.model.Signal;

import java.util.ArrayList;
import java.util.List;

/**
 * © Georgy Schukin
 * Class to hold and manage signals data
 */

public class SignalManager implements ObjectManager{
    private ArrayList<Signal> signals = new ArrayList<Signal>(); // list of signals

    public SignalManager() {}

    public synchronized final void addSignals(List<Signal> signals) {
        this.signals.addAll(signals);
    }

    public synchronized final void clearSignals() {
        signals.clear();
    }

    public synchronized Signal getSignal(GeoPoint geoPoint) {
        for(Signal s : signals) {
            if((s.getPosition().getLatitude() == geoPoint.getLatitudeE6()) &&
                    (s.getPosition().getLongitude() == geoPoint.getLongitudeE6()))
                return s;
        }
        return null;
    }

    @Override
    public synchronized void updateOverlay(MapItemOverlay overlay) {
        for(Signal signal : signals) {
            GeoPoint pos = new GeoPoint(signal.getPosition().getLatitude(), signal.getPosition().getLongitude());
                overlay.addItem(new SignalItem(pos, "", signal.getStrength()));
        }
    }
}
