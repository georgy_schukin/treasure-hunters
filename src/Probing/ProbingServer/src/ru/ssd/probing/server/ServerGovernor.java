package ru.ssd.probing.server;

import ru.ssd.probing.common.model.*;
import ru.ssd.probing.common.protocol.ServerResponse;
import ru.ssd.probing.server.communication.AuthorizationService;
import ru.ssd.probing.server.communication.CommunicationManager;
import ru.ssd.probing.server.communication.CommunicationManagerImpl;
import ru.ssd.probing.server.communication.CommunicationManagerListener;
import ru.ssd.probing.server.world.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * © Ruslan Mustakov
 */


public class ServerGovernor implements CommunicationManagerListener, WorldManagerListener {
    private WorldManager worldManager;
    private CommunicationManager communicationManager;

    public ServerGovernor() {
        communicationManager = new CommunicationManagerImpl(this);
        new Thread(new AuthorizationService()).start();
        try {

            worldManager = new NonDBWorldManager();
            worldManager.addListener(this);
        }
        catch (SQLException e) {
            System.err.println("Database manager initialization error. Server can not start.\n");
            System.exit(1);
        }
    }


    //CommunicationManagerListener implementation
    @Override
    public void onNewPlayer(Player player)
    {
        worldManager.addPlayerInWorld(player);
    }

    @Override
    public void onNewGroup(Group group)
    {
        //TODO
    }

    @Override
    public void onTreasurePutRequested(Player player, Position treasurePosition) {
        try {
            worldManager.putTreasureOnPosition(player.getName(), treasurePosition);
            communicationManager.sendResponse(player, new ServerResponse());
        }
        catch (ServerErrorException e) {
            communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.INNER_ERROR));
        }
    }

    @Override
    public void onLFGStateChangeRequested(Player player, boolean isLFG) {
        try {
            worldManager.setPlayerLFGState(player.getName(), isLFG);
            communicationManager.sendResponse(player, new ServerResponse());
        }
        catch (AlreadyInGroupException e) {
            communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.ALREADY_IN_GROUP_ERROR));
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onPositionChangeRequested(Player player, Position newPosition) {
        try {
            worldManager.setPlayerPosition(player.getName(), newPosition);
            communicationManager.sendResponse(player, new ServerResponse());
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onGroupPropertiesChangeRequested(Player leader, GroupProperties properties) {
        try {
            worldManager.setGroupProperties(leader.getName(), properties);
            communicationManager.sendResponse(leader, new ServerResponse());
        }
        catch (NoGroupException e) {
            communicationManager.sendResponse(leader, new ServerResponse(ServerResponse.ErrorCodes.NO_GROUP_ERROR));
        }
        catch (NotALeaderException e) {
            communicationManager.sendResponse(leader, new ServerResponse(ServerResponse.ErrorCodes.NOT_LEADER_ERROR));
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onFlagRequested(Player player, Position flagPosition) {
        try {
            worldManager.putFlag(player.getName(), flagPosition);
            communicationManager.sendResponse(player, new ServerResponse());
        }
        catch (NoGroupException e) {
            communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.NO_GROUP_ERROR));
        }
        catch (BadPositionException e) {
            communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.BAD_POSITION_ERROR));
        }
        catch (TooManyFlagsException e) {
            communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.TOO_MANY_FLAGS_ERROR));
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onFlagRemovalRequested(Player player, Position flagPosition) {
        try {
            worldManager.removeFlag(player.getName(), flagPosition);
            communicationManager.sendResponse(player, new ServerResponse());
        }
        catch (BadPositionException e) {
            communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.BAD_POSITION_ERROR));
        }
        catch (NoGroupException e) {
            communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.NO_GROUP_ERROR));
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onScanStateChangeRequested(Player player, boolean isScanning) {
        try {
            worldManager.setScanState(player.getName(), isScanning);
            communicationManager.sendResponse(player, new ServerResponse());
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onMemberKickRequested(Player leader, String memberName) {
        try {
            Group group = worldManager.getPlayerGroup(leader.getName());
            if (group != null) {
                if (leader.equals(group.getLeader())) {
                    worldManager.removeMemberFromGroup(group.getName(), memberName);
                    communicationManager.sendResponse(leader, new ServerResponse());
                    //TODO: kick message?
                }
                else {
                    communicationManager.sendResponse(leader, new ServerResponse(ServerResponse.ErrorCodes.NOT_LEADER_ERROR));
                }

            }
            else {
                communicationManager.sendResponse(leader, new ServerResponse(ServerResponse.ErrorCodes.NO_GROUP_ERROR));
            }
        }
        catch (NoGroupException e) {
            communicationManager.sendResponse(leader, new ServerResponse(ServerResponse.ErrorCodes.NO_GROUP_ERROR));
        }
        catch (BadGroupException e) {
            communicationManager.sendResponse(leader, new ServerResponse(ServerResponse.ErrorCodes.NOT_GROUP_MEMBER_ERROR));
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onLeadershipChangeRequested(Player currentLeader, Player newLeader) {
        try {
            Group group = worldManager.getPlayerGroup(currentLeader.getName());
            if (group != null) {
                if (currentLeader.equals(group.getLeader())) {
                    worldManager.setGroupLeader(group.getName(), newLeader.getName());
                    communicationManager.sendResponse(currentLeader, new ServerResponse());
                }
                else {
                    communicationManager.sendResponse(currentLeader, new ServerResponse(ServerResponse.ErrorCodes.NOT_LEADER_ERROR));
                }
            }
            else {
                communicationManager.sendResponse(currentLeader, new ServerResponse(ServerResponse.ErrorCodes.NO_GROUP_ERROR));
            }
        }
        catch (BadGroupException e) {
            communicationManager.sendResponse(currentLeader, new ServerResponse(ServerResponse.ErrorCodes.NOT_GROUP_MEMBER_ERROR));
        }
        catch (NoGroupException e) {
            communicationManager.sendResponse(currentLeader, new ServerResponse(ServerResponse.ErrorCodes.NO_GROUP_ERROR));
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onLeaveGroupRequested(Player player) {
        try {
            Group group = worldManager.getPlayerGroup(player.getName());
            if (group != null) {
                worldManager.removeMemberFromGroup(group.getName(), player.getName());
                communicationManager.sendResponse(player, new ServerResponse());
            }
            else {
                communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.NO_GROUP_ERROR));
            }
        }
        catch (BadGroupException e) {
            //should not be thrown
            communicationManager.sendResponse(player, new ServerResponse());
        }
        catch (NoGroupException e) {
            communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.NO_GROUP_ERROR));
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onTreasureRequested(Player player, Position treasurePosition) {
        Treasure treasure = worldManager.getTreasureOnPositionForPlayer(player.getName(), treasurePosition);
        if (treasure != null) {
            communicationManager.sendResponse(player, new ServerResponse());
            communicationManager.sendTreasure(player, treasure);
        }
        else {
            communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.NO_TREASURE_ERROR));
        }
    }

    @Override
    public void onPlayerDisconnected(Player player) {
        worldManager.removePlayerFromWorld(player.getName());
    }

    @Override
    public void onScanRadiusIndexChangeRequested(Player player, int newScanRadiusIndex) {
        try {
            worldManager.setScanRadiusIndex(player.getName(), newScanRadiusIndex);
            communicationManager.sendResponse(player, new ServerResponse());
        }
        catch (BadScanRadiusIndexException e) {
            communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.BAD_SCAN_RADIUS_INDEX_ERROR));
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onGroupCreationRequested(Player player) {
        try {
            worldManager.createGroup(player.getName());
            communicationManager.sendResponse(player, new ServerResponse());
        }
        catch (AlreadyInGroupException e) {
            communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.ALREADY_IN_GROUP_ERROR));
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onInvitationSent(Player sender, String whomToInvite) {
        try {
            worldManager.playerInvited(sender.getName(), whomToInvite);
            communicationManager.sendResponse(sender, new ServerResponse());
        }
        catch (AlreadyInvitedException e) {
            communicationManager.sendResponse(sender, new ServerResponse(ServerResponse.ErrorCodes.ALREADY_INVITED_ERROR));
        }
        catch (NoGroupException e) {
            communicationManager.sendResponse(sender, new ServerResponse(ServerResponse.ErrorCodes.NO_GROUP_ERROR));
        }
        catch (NotALeaderException e) {
            communicationManager.sendResponse(sender, new ServerResponse(ServerResponse.ErrorCodes.NOT_LEADER_ERROR));
        }
        catch (NotLFGException e) {
            communicationManager.sendResponse(sender, new ServerResponse(ServerResponse.ErrorCodes.NOT_LFG_ERROR));
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onInvitationAccepted(Player player, String groupName) {
        try {
            worldManager.invitationAccepted(player.getName(), groupName);
            communicationManager.sendResponse(player, new ServerResponse());
        }
        catch (NoInvitationException e) {
            communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.NO_INVITATION_ERROR));
        }
        catch (AlreadyInGroupException e) {
            communicationManager.sendResponse(player, new ServerResponse(ServerResponse.ErrorCodes.ALREADY_IN_GROUP_ERROR));
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    //WorldManagerListener implementation

    @Override
    public void onGroupFormed(Group group) {
        for (Player player : group.getPlayers()) {
            communicationManager.sendGroupAssigned(group, player);
        }
    }

    @Override
    public void onGroupPropertiesChanged(Group group) {
        //TODO: dispatch?
    }

    @Override
    public void onPositionChanged(Player player) {
        try {
            Group group = worldManager.getPlayerGroup(player.getName());
            if (group != null) {
                communicationManager.dispatchMemberPositionChanged(group, player);
            }
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onFlagPut(Group group, Flag flag) {
        communicationManager.dispatchFlagPutEvent(group, flag);
    }

    @Override
    public void onFlagRemoved(Group group, Position flagPosition) {
        communicationManager.dispatchFlagRemoved(group, flagPosition);
    }

    @Override
    public void onScanStateChanged(Player player) {
        try {
            Group group = worldManager.getPlayerGroup(player.getName());
            if (group != null) {
                communicationManager.dispatchMemberScanReadyStateChanged(group, player);
            }
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onScanRadiusChanged(Player player) {
        try {
            Group group = worldManager.getPlayerGroup(player.getName());
            if (group != null) {
                communicationManager.dispatchMemberScanRadiusChanged(group, player);
            }
        }
        catch (NoSuchPlayerException e) {
            //should not be thrown
        }
    }

    @Override
    public void onLeaderChanged(Group group, Player oldLeader) {
        communicationManager.dispatchLeaderChanged(group);
    }

    @Override
    public void onGroupMemberLeft(Group group, Player player) {
        communicationManager.sendLeftGroup(player);
        if (group != null) {
            communicationManager.dispatchMemberLeft(group, player);
        }
    }

    @Override
    public void onTreasureEarned(Group group, Treasure treasure) {
        for (Player player : group.getPlayers()) {
            communicationManager.sendTreasure(player, treasure);
        }
    }

    @Override
    public void onScanStarted(Group group) {
        communicationManager.dispatchGroupScanStarted(group);
    }

    @Override
    public void onScanFinished(Group group, Set<Signal> foundSignals) {
        communicationManager.dispatchFoundSignals(group, new ArrayList<Signal>(foundSignals));
    }

    @Override
    public void onMemberJoined(Group group, Player newMember) {
        communicationManager.sendGroupAssigned(group, newMember);
    }

    @Override
    public void onPlayerInvited(Group group, Player who) {
        Set<String> memberNames = new HashSet<String>();
        for (Player member : group.getPlayers()) {
            memberNames.add(member.getName());
        }
        communicationManager.sendInvitationReceived(who, group.getName(), memberNames);
    }

    @Override
    public void onLFGInterest(List<Group> groups, Player player) {
        //TODO: remove unnecessary parameter
        communicationManager.dispatchLFGPlayerEvent(groups, player, player.isLFG());
    }
}
