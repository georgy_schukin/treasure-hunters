package ru.ssd.probing.server.world;

/**
 * © Ruslan Mustakov
 */


public class TooManyFlagsException extends Exception {
    public TooManyFlagsException() {
        super();
    }

    public TooManyFlagsException(String message) {
        super(message);
    }
}
