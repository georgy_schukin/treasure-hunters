package ru.ssd.probing.server.world;

/**
 * © Ruslan Mustakov
 */


public class BadPositionException extends Exception {
    public BadPositionException() {
        super();
    }

    public BadPositionException(String message) {
        super(message);
    }
}
