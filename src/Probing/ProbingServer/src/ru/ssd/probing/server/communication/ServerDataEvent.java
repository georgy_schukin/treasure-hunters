package ru.ssd.probing.server.communication;

/**
 * Created with IntelliJ IDEA.
 * User: maxim
 * Date: 30.06.12
 * Time: 12:59
 * To change this template use File | Settings | File Templates.
 */
import java.nio.channels.SocketChannel;

class ServerDataEvent {
    public NioServer server;
    public SocketChannel socket;
    public byte[] data;

    public ServerDataEvent(NioServer server, SocketChannel socket, byte[] data) {
        this.server = server;
        this.socket = socket;
        this.data = data;
    }
}