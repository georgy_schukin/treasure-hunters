package ru.ssd.probing.server.world;

/**
 * © Ruslan Mustakov
 */


public class ServerErrorException extends Exception {
    public ServerErrorException() {
        super();
    }

    public ServerErrorException(String message) {
        super(message);
    }
}
