package ru.ssd.probing.server.world;

/**
 * © Ruslan Mustakov
 */


public class NotLFGException extends Exception {
    public NotLFGException() {
    }

    public NotLFGException(String message) {
        super(message);
    }
}
