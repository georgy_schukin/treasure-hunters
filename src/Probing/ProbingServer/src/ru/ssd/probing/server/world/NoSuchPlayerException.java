package ru.ssd.probing.server.world;

/**
 * © Ruslan Mustakov
 */


public class NoSuchPlayerException extends Exception {
    public NoSuchPlayerException() {
        super();
    }

    public NoSuchPlayerException(String message) {
        super(message);
    }
}
