package ru.ssd.probing.server.world;

/**
 * © Ruslan Mustakov
 */


public class NoGroupException extends Exception {
    public NoGroupException() {
        super();
    }

    public NoGroupException(String message) {
        super(message);
    }
}
