package ru.ssd.probing.server.world.geom;

/**
 * © Ruslan Mustakov
 */


public class Matrix {
    private double[][] data;

    public Matrix(int width, int height) {
        if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("width and height must be positive");
        }
        if (width != height) {
            throw new IllegalArgumentException("width and height must be the same");
        }
        data = new double[height][width];
    }

    public Matrix(double[][] data) {
        if (data.length == 0) {
            throw new IllegalArgumentException("matrix can't be empty");
        }
        if (data[0].length != data.length) {
            throw new IllegalStateException("width and height must be the same");
        }
        this.data = data.clone();
    }

    public double get(int i, int j) {
        return data[i][j];
    }

    public void set(int i, int j, double value) {
        data[i][j] = value;
    }

    public Vector3D multiply(Vector3D other) {
        int width = data[0].length;
        int height = data.length;
        if (width != 3 || height != 3) {
            throw new IllegalStateException();
        }
        double[] coords = new double[height];
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                double val = 0;
                switch (j) {
                    case 0:
                        val = other.getX();
                        break;
                    case 1:
                        val = other.getY();
                        break;
                    case 2:
                        val = other.getZ();
                        break;
                }
                coords[i] += data[i][j] * val;
            }
        }
        return new Vector3D(coords[0], coords[1], coords[2]);
    }

    public Matrix inverse() {
        if (data.length > 3) {
            throw new IllegalStateException("inverse is not supported for matrices with size more than 3");
        }
        double[][] newData = new double[data.length][data.length];
        double divisor = determinant();
        if (data.length == 1) {
            newData[0][0] = 1. / divisor;
        }
        else if (data.length == 2) {
            newData[0][0] = get(1,1) / divisor;
            newData[0][1] = -get(0,1) / divisor;
            newData[1][0] = -get(1,0) / divisor;
            newData[1][1] = get(0,0) / divisor;
        }
        else {
            double[][] tempData = new double[data.length - 1][data.length - 1];
            tempData[0][0] = get(1,1);
            tempData[0][1] = get(1,2);
            tempData[1][0] = get(2,1);
            tempData[1][1] = get(2,2);
            Matrix tempMatrix = new Matrix(tempData);
            newData[0][0] = tempMatrix.determinant() / divisor;

            tempData[0][0] = get(0,2);
            tempData[0][1] = get(0,1);
            tempData[1][0] = get(2,2);
            tempData[1][1] = get(2,1);
            tempMatrix = new Matrix(tempData);
            newData[0][1] = tempMatrix.determinant() / divisor;

            tempData[0][0] = get(0,1);
            tempData[0][1] = get(0,2);
            tempData[1][0] = get(1,1);
            tempData[1][1] = get(1,2);
            tempMatrix = new Matrix(tempData);
            newData[0][2] = tempMatrix.determinant() / divisor;

            tempData[0][0] = get(1,2);
            tempData[0][1] = get(1,0);
            tempData[1][0] = get(2,2);
            tempData[1][1] = get(2,0);
            tempMatrix = new Matrix(tempData);
            newData[1][0] = tempMatrix.determinant() / divisor;

            tempData[0][0] = get(0,0);
            tempData[0][1] = get(0,2);
            tempData[1][0] = get(2,0);
            tempData[1][1] = get(2,2);
            tempMatrix = new Matrix(tempData);
            newData[1][1] = tempMatrix.determinant() / divisor;

            tempData[0][0] = get(0,2);
            tempData[0][1] = get(0,0);
            tempData[1][0] = get(1,2);
            tempData[1][1] = get(1,0);
            tempMatrix = new Matrix(tempData);
            newData[1][2] = tempMatrix.determinant() / divisor;

            tempData[0][0] = get(1,0);
            tempData[0][1] = get(1,1);
            tempData[1][0] = get(2,0);
            tempData[1][1] = get(2,1);
            tempMatrix = new Matrix(tempData);
            newData[2][0] = tempMatrix.determinant() / divisor;

            tempData[0][0] = get(0,1);
            tempData[0][1] = get(0,0);
            tempData[1][0] = get(2,1);
            tempData[1][1] = get(2,0);
            tempMatrix = new Matrix(tempData);
            newData[2][1] = tempMatrix.determinant() / divisor;

            tempData[0][0] = get(0,0);
            tempData[0][1] = get(0,1);
            tempData[1][0] = get(1,0);
            tempData[1][1] = get(1,1);
            tempMatrix = new Matrix(tempData);
            newData[2][2] = tempMatrix.determinant() / divisor;
        }
        return new Matrix(newData);
    }

    public double determinant() {
        double result = 0;

        if(data.length == 1) {
            result = data[0][0];
            return result;
        }

        if(data.length == 2) {
            result = data[0][0] * data[1][1] - data[0][1] * data[1][0];
            return result;
        }

        for(int i = 0; i < data[0].length; i++) {
            double temp[][] = new double[data.length - 1][data[0].length - 1];

            for(int j = 1; j < data.length; j++) {
                System.arraycopy(data[j], 0, temp[j-1], 0, i);
                System.arraycopy(data[j], i+1, temp[j-1], i, data[0].length-i-1);
            }
            Matrix tempMatrix = new Matrix(temp);
            result += data[0][i] * Math.pow(-1, i) * tempMatrix.determinant();
        }

        return result;
    }
}
