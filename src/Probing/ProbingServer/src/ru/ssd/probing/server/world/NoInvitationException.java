package ru.ssd.probing.server.world;

/**
 * © Ruslan Mustakov
 */


public class NoInvitationException extends Exception {
    public NoInvitationException() {
    }

    public NoInvitationException(String message) {
        super(message);
    }
}
