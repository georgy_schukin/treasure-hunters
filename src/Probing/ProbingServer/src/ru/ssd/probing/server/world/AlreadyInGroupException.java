package ru.ssd.probing.server.world;

/**
 * © Ruslan Mustakov
 */


public class AlreadyInGroupException extends Exception {
    public AlreadyInGroupException() {
        super();
    }

    public AlreadyInGroupException(String message) {
        super(message);
    }
}
