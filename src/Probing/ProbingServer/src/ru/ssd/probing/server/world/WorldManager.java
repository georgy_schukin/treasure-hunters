package ru.ssd.probing.server.world;

import ru.ssd.probing.common.model.*;

/**
 * © Ruslan Mustakov
 */


public interface WorldManager {
    public void setPlayerPosition(String playerName, Position position) throws NoSuchPlayerException;
    public void setGroupProperties(String leaderName, GroupProperties properties) throws NoSuchPlayerException, NoGroupException, NotALeaderException;
    public Group getPlayerGroup(String playerName) throws NoSuchPlayerException;
    public Flag putFlag(String putPlayerName, Position position) throws NoSuchPlayerException, NoGroupException, TooManyFlagsException, BadPositionException;
    public void removeFlag(String whoRemoves, Position flagPosition) throws NoSuchPlayerException, NoGroupException, BadPositionException;
    public void setScanState(String playerName, boolean isScanning) throws NoSuchPlayerException;
    public void setScanRadiusIndex(String playerName, int index) throws NoSuchPlayerException, BadScanRadiusIndexException;
    public Player getPlayerByName(String name);
    public Group getGroupByName(String name);
    public void removeMemberFromGroup(String groupName, String memberName) throws NoSuchPlayerException, NoGroupException, BadGroupException;
    public void setGroupLeader(String groupName, String leaderName) throws NoGroupException, NoSuchPlayerException, BadGroupException;
    public Treasure getTreasureOnPositionForPlayer(String playerName, Position position);
    public void removePlayerFromWorld(String playerName);
    public void setPlayerLFGState(String playerName, boolean isLFG) throws NoSuchPlayerException, AlreadyInGroupException;
    public void addPlayerInWorld(Player player);

    public void createGroup(String leaderName) throws NoSuchPlayerException, AlreadyInGroupException;

    public void playerInvited(String groupLeaderName, String playerName) throws NoGroupException, NoSuchPlayerException, NotALeaderException, NotLFGException, AlreadyInvitedException;
    public void invitationAccepted(String playerName, String groupName) throws NoSuchPlayerException, NoInvitationException, AlreadyInGroupException;

    public void putTreasureOnPosition(String playerName, Position position) throws ServerErrorException;

    public void addListener(WorldManagerListener listener);
    public void removeListener(WorldManagerListener listener);
}
