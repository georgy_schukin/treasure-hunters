package ru.ssd.probing.server.world;

/**
 * © Ruslan Mustakov
 */


public class AlreadyInvitedException extends Exception {
    public AlreadyInvitedException() {
    }

    public AlreadyInvitedException(String message) {
        super(message);
    }
}
