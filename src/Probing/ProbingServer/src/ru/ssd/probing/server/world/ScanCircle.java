package ru.ssd.probing.server.world;

import ru.ssd.probing.common.model.Position;

/**
 * © Ruslan Mustakov
 */


public class ScanCircle {
    private Position center;
    private int radius;
    private int scanStrength;

    public ScanCircle(Position center, int radius, int scanStrength) {
        this.center = center;
        this.radius = radius;
        this.scanStrength = scanStrength;
    }

    public Position getCenter() {
        return center;
    }

    public int getRadius() {
        return radius;
    }

    public int getScanStrength() {
        return scanStrength;
    }

    public boolean contains(Position pointPosition) {
        if (center.getDistance(pointPosition) <= radius) {
            return true;
        }
        return false;
    }
}
