package ru.ssd.probing.server.world.geom;

import ru.ssd.probing.common.model.Position;

/**
 * © Ruslan Mustakov
 */


public class Vector3D {
    private double x;
    private double y;
    private double z;

    private static final double R = 6371009.;

    public Vector3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    Vector3D(Position position) {
        double radiansLatitude = position.getLatitude() / (1.e6 *180.) * Math.PI;
        double radiansLongitude = position.getLongitude() / (1.e6 * 180.) * Math.PI;
        x = R * Math.cos(radiansLatitude) * Math.cos(radiansLongitude);
        y = R * Math.cos(radiansLatitude) * Math.sin(radiansLongitude);
        z = R * Math.sin(radiansLatitude);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double length() {
        return Math.sqrt(x * x + y * y + z * z);
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + "," + z + ")";
    }
}
