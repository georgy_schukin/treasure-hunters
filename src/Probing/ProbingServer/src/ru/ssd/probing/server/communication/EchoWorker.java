package ru.ssd.probing.server.communication;

/**
 * Created with IntelliJ IDEA.
 * User: maxim
 * Date: 30.06.12
 * Time: 12:59
 * To change this template use File | Settings | File Templates.
 */

import org.apache.log4j.Logger;
import ru.ssd.probing.common.model.*;
import ru.ssd.probing.server.DBConnectionFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.nio.channels.SocketChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class EchoWorker implements Runnable {
    private List queue = new LinkedList();
    private CommunicationManagerListener cmListener = null;
    private CommunicationManagerImpl cm = null;
    private static final Logger communicationLogger = Logger.getLogger("Communication");

    public EchoWorker(CommunicationManagerListener cmListener, CommunicationManagerImpl cm)
    {
        this.cmListener = cmListener;
        this.cm = cm;
    }

    public void processSocketError(SocketChannel socketChannel)
    {
        cmListener.onPlayerDisconnected(cm.getPlayerBySocket(socketChannel));
    }
    private class DBUserCheckResult
    {
        public int status;
        public String nickName;
        DBUserCheckResult(int status, String nickName)
        {
            this.status = status;
            this.nickName = nickName;
        }
    }
    private DBUserCheckResult checkUserInDb(String google_id, String probe_id)
    {
        communicationLogger.debug("In checkUserInDb");
        Connection conn  = null;
        PreparedStatement pstmt = null;
        //-1 means user not found in the DB
        DBUserCheckResult result = new DBUserCheckResult(-1,"");
        ResultSet rs    = null;

        try {

            communicationLogger.debug("In checkUserInDb in try");
/*
            Properties connInfo = new Properties();

            connInfo.put("characterEncoding","UTF8");
            connInfo.put("user", "probing_user");
            connInfo.put("password", "probe105040");

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            communicationLogger.debug("Driver ok");
            conn  = DriverManager.getConnection("jdbc:mysql://ssd1.sscc.ru:6890/probing", connInfo);
*/
            conn = DBConnectionFactory.getConnection();
            if(null!=conn)
            {
                communicationLogger.debug("Connection OK");
                pstmt = conn.prepareStatement("select id, nick_name, google_id, status from players where google_id='"+google_id+"' and id='"+probe_id+"';");

                if(pstmt.execute()) {
                    communicationLogger.debug("Execute ok");
                    rs = pstmt.getResultSet();

                    if(rs.next()) {
                        result.status = rs.getInt("status");
                        result.nickName = rs.getString("nick_name");
                    }
                }
            }
        }
        catch (SQLException ex) {
            communicationLogger.error(ex.getMessage());
            //ex.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace(System.err);
        }
        finally {

            try {

                if (pstmt != null)
                    pstmt.close();

                if (conn != null)
                    conn.close();
            }
            catch (SQLException ex) {
                communicationLogger.error("On close: " + ex.getMessage());
                //ex.printStackTrace();
            }

        }
        if(-1!=result.status)
            communicationLogger.debug("Found in DB: probe_id = "+probe_id+", nick_name = " + result.nickName);
        else
            communicationLogger.error("!!! Not found in DB: probe_id = " + probe_id + ", google_id = " + google_id);
        return result;
    }
    //returns 0 if OK
    //returns -1 if failed. It means player has been dropped from communication manager tables
    // respective socket should be closed in the calling function
    public int processData(NioServer server, SocketChannel socket, byte[] data, int count) {

        //result to be returned. Will be changed to -1 in case of failure
        int processDataResult = 0;

        byte[] dataCopy = new byte[count];
        System.arraycopy(data, 0, dataCopy, 0, count);

        BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(dataCopy)));

        try
        {
            String s = br.readLine();
            communicationLogger.debug("Server received: "+s);
            if(s.equals("newPlayer")) {
                String google_id = br.readLine();
                System.err.println("google_id: "+google_id);
                String probe_id = br.readLine();
                System.err.println("probe_id: "+probe_id);
                String latitude = br.readLine();
                System.err.println("Latitude: "+latitude);
                String longitude = br.readLine();
                System.err.println("longitude: "+longitude);
                String radiusIndex = br.readLine();
                System.err.println("radius index: "+radiusIndex);
                String scanStrength = br.readLine();
                System.err.println("scan strength: "+scanStrength);

                DBUserCheckResult result = checkUserInDb(google_id, probe_id);
                if(-1 == result.status)
                {
                    communicationLogger.debug("EchoWorker: Security break attempt: someone accessed server with wrong (google_id, probe_id) pair");
                    processDataResult = -1;
                }
                else
                {
                    if(0 == result.status)
                    {
                        communicationLogger.debug("EchoWorker: Not-activated user connected");
                    }
                    if(1 == result.status)
                    {
                        communicationLogger.debug("EchoWorker: Activated user connected");
                    }
                    if(2 == result.status)
                    {
                        communicationLogger.debug("EchoWorker: Banned user connected");
                    }
                    //TODO remove probe_id and perhaps google_id too from the Player
                    Player player = new Player(result.nickName, new Position(Integer.parseInt(latitude), Integer.parseInt(longitude)), google_id, probe_id);
                    if(cm.checkPlayerInGame(player))
                    {
                        //TODO respective socket should be deleted from selector and closed
                        //TODO !!! in case of live socket, the warning should be send about multiple logins by the same user
                        cm.drop(player);
                        communicationLogger.debug("Player reconnected");
                        cmListener.onPlayerDisconnected(player);
                    }
                    cm.associatePlayerWithSocket(player, socket);
                    player.setProbeProperties(new ProbeProperties( Integer.parseInt(radiusIndex), Integer.parseInt(scanStrength)));
                    cmListener.onNewPlayer(player);
                }

            } else
            if(s.equals("newGroup"))
            {
                String groupName = br.readLine();
                String leaderGoogleID = br.readLine();
                String automaticallyAcceptingNewMembers = br.readLine();
                String interestedInNewMembers = br.readLine();
                Boolean aanm = null;
                if(automaticallyAcceptingNewMembers.equals("0"))
                    aanm = false;
                else
                    aanm = true;
                Boolean iinm = null;
                if(interestedInNewMembers.equals("0"))
                    iinm = false;
                else
                    iinm = true;
                Group group = new Group(groupName, cm.getPlayerBySocket(socket), new GroupProperties(aanm, iinm));
                cmListener.onNewGroup(group);
            } else
            if(s.equals("changeLFGState"))
            {
            //0|1
                String lfgState = br.readLine();
                Boolean val = null;
                if(lfgState.equals("0"))
                    val = false;
                else
                    val = true;
                cmListener.onLFGStateChangeRequested(cm.getPlayerBySocket(socket), val);
            } else
            if(s.equals("changePosition"))
            {
                communicationLogger.debug("In changePosition");
            //longitude
            //latitude
                String latitude = br.readLine();
                String longitude = br.readLine();
                communicationLogger.debug("Calling onPositionChangeRequested");
                cmListener.onPositionChangeRequested(cm.getPlayerBySocket(socket), new Position(Integer.parseInt(latitude), Integer.parseInt(longitude)));
                communicationLogger.debug("Done onPositionChangeRequested");
            } else
            if (s.equals("changeGroupProperties"))
            {
            //0|1 (automaticallyAcceptingNewMembers)
            //0|1 (interestedInNewMembers)
                String automaticallyAcceptingNewMembers = br.readLine();
                String interestedInNewMembers = br.readLine();
                Boolean aanm = null;
                if(automaticallyAcceptingNewMembers.equals("0"))
                    aanm = false;
                else
                    aanm = true;
                Boolean iinm = null;
                if(interestedInNewMembers.equals("0"))
                    iinm = false;
                else
                    iinm = true;
                cmListener.onGroupPropertiesChangeRequested(cm.getPlayerBySocket(socket), new GroupProperties(aanm, iinm));
            } else
            if(s.equals("putFlag"))
            {
            //longitude
            //latitude
                String latitude = br.readLine();
                String longitude = br.readLine();
                cmListener.onFlagRequested(cm.getPlayerBySocket(socket), new Position(Integer.parseInt(latitude), Integer.parseInt(longitude)));
            } else
            if (s.equals("changeScanState"))
            {
            //0|1
                String scanState = br.readLine();
                Boolean boolScanState = null;
                if(scanState.equals("0"))
                    boolScanState = false;
                else
                    boolScanState = true;
                Boolean iinm = null;
                cmListener.onScanStateChangeRequested(cm.getPlayerBySocket(socket), boolScanState);
            } else
            if(s.equals("changeScanRadiusIndex"))
            {
            //integer value
                String radiusIndex = br.readLine();
                cmListener.onScanRadiusIndexChangeRequested(cm.getPlayerBySocket(socket),Integer.parseInt(radiusIndex));
            } else
            if(s.equals("kickMember"))
            {
            //probe_id (as String)
            } else
            if (s.equals("changeLeader"))
            {
            //google_id (as String)
/*                String radiusIndex = br.readLine();
                cmListener.onLeadershipChangeRequested();
*/
            } else
            if (s.equals("leaveGroup"))
            {
                cmListener.onLeaveGroupRequested(cm.getPlayerBySocket(socket));
            } else
            if (s.equals("getTreasure"))
            {
            //longitude
            //latitude
                String latitude = br.readLine();
                String longitude = br.readLine();
                cmListener.onTreasureRequested(cm.getPlayerBySocket(socket), new Position(Integer.parseInt(latitude), Integer.parseInt(longitude)));
            } else
            if (s.equals("putTreasure"))
            {
                String latitude = br.readLine();
                String longitude = br.readLine();
                cmListener.onTreasurePutRequested(cm.getPlayerBySocket(socket), new Position(Integer.parseInt(latitude), Integer.parseInt(longitude)));
            } else
            if (s.equals("acceptInvitation"))
            {
                //acceptInvitation
                //groupName
                String groupName = br.readLine();
                String longitude = br.readLine();
                cmListener.onInvitationAccepted(cm.getPlayerBySocket(socket), groupName);
            } else
            if (s.equals("invitePlayerToGroup"))
            {
                //invitePlayerToGroup
                //name
                String name = br.readLine();
                cmListener.onInvitationSent(cm.getPlayerBySocket(socket), name);
            } else
            //createGroup
            if (s.equals("createGroup"))
            {
                cmListener.onGroupCreationRequested(cm.getPlayerBySocket(socket));
            } else
            if(s.equals("removeFlag"))
            {
            //removeFlag
            //latitude
            //longitude
                String latitude = br.readLine();
                String longitude = br.readLine();
                cmListener.onFlagRemovalRequested(cm.getPlayerBySocket(socket), new Position(Integer.parseInt(latitude), Integer.parseInt(longitude)));
            }
            else
            {
                processDataResult = -1;
                communicationLogger.error("Player sent wrong instruction world");
            }
        }
        catch(Throwable t)
        {
            t.printStackTrace(System.err);

            processDataResult = -1;
        }
/*
        synchronized(queue) {
            queue.add(new ServerDataEvent(server, socket, dataCopy));
            queue.notify();
        }
*/
        if(-1 == processDataResult)
        {
            communicationLogger.error("EchoWorker: error in processing input");
            Player player = cm.getPlayerBySocket(socket);
            if(null!=player)
            {
                communicationLogger.error("EchoWorker: error happened with existing player");
                cm.drop(player);
                cmListener.onPlayerDisconnected(player);
            }
            {
                communicationLogger.error("EchoWorker: error happened on new connection");
            }
        }
        return processDataResult;
    }

    public void run() {
        ServerDataEvent dataEvent;

        while(true) {
            // Wait for data to become available
            synchronized(queue) {
                while(queue.isEmpty()) {
                    try {
                        queue.wait();
                    } catch (InterruptedException e) {
                    }
                }
                dataEvent = (ServerDataEvent) queue.remove(0);
            }

            // Return to sender
            dataEvent.server.send(dataEvent.socket, dataEvent.data);
        }
    }
}