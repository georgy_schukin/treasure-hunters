package ru.ssd.probing.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: maxim
 * Date: 10.07.12
 * Time: 17:40
 * To change this template use File | Settings | File Templates.
 */
public class DBConnectionFactory {

    private static final String PROPERTIES_FILE_NAME = "db.properties";
    private static final String DB_ADDRESS_PROPERTY_NAME = "dbAddress";
    private static final String DB_USER_PROPERTY_NAME = "dbUser";
    private static final String DB_PASSWORD_PROPERTY_NAME = "dbPassword";
    private static final String DB_DRIVER_PROPERTY_NAME = "dbDriver";

    private static final Properties dbProperties;

    static {
        dbProperties = new Properties();
        FileInputStream propertiesFile = null;
        String propertiesPath = null;
        try {
            propertiesPath = new URL(DBConnectionFactory.class.getProtectionDomain().getCodeSource().getLocation(), PROPERTIES_FILE_NAME).getFile();
            propertiesPath = URLDecoder.decode(propertiesPath, "UTF-8");
        }
        catch (MalformedURLException e)
        {
        }
        catch (UnsupportedEncodingException e)
        {
        }

        try {
            propertiesFile = new FileInputStream(propertiesPath);
            dbProperties.load(propertiesFile);
        }
        catch (IOException e) {}
        finally {
            if (propertiesFile != null)
            {
                try {
                    propertiesFile.close();
                }
                catch (IOException e) {}
            }
        }

        try {
            Class.forName(dbProperties.getProperty(DB_DRIVER_PROPERTY_NAME));
        } catch (ClassNotFoundException e) {
            System.err.println("WARNING! No suitable SQL driver class found: "
                    + dbProperties.getProperty(DB_DRIVER_PROPERTY_NAME)
                    + " database may not work.");
        }
    }

    public static Connection getConnection()
    {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbProperties.getProperty(DB_ADDRESS_PROPERTY_NAME),
                    dbProperties.getProperty(DB_USER_PROPERTY_NAME), dbProperties.getProperty(DB_PASSWORD_PROPERTY_NAME));
        }
        catch(SQLException e)
        {
        }
        return connection;
    }
}
