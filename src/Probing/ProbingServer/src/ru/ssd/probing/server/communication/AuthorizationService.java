package ru.ssd.probing.server.communication;

import org.apache.log4j.Logger;
import ru.ssd.probing.server.DBConnectionFactory;

import java.io.*;
import java.net.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: maxim
 * Date: 23.06.12
 * Time: 16:40
 * To change this template use File | Settings | File Templates.
 */
public class AuthorizationService implements Runnable {

    private static final Logger authorizationLogger = Logger.getLogger("Authorization");

    public AuthorizationService()
    {
/*        saveLog();
        try
        {
            server();
        }
        catch (Throwable t) {
            System.err.println("Error server");
            t.printStackTrace(System.err);
        }
  */
    }


/*    public void saveLog() {
        File file = new File("log.txt");
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            fileWriter.write("string\n");
        }
        catch (IOException ex) {
            System.err.println("Error opening " + file);
        }
        finally {
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                }
                catch (IOException e) {}
            }
        }
    }
*/
    public void run()
    {
        try
        {
        ServerSocket ss = new ServerSocket(8080);
        while (true) {
            Socket s = ss.accept();
            authorizationLogger.debug("Client accepted");
            new Thread(new SocketProcessor(s)).start();
            authorizationLogger.debug("Socket processor thread started ok");
        }
        }catch (Throwable t)
        {
            authorizationLogger.error(t.getMessage());
            t.printStackTrace(System.err);
        }
    }

    private static class SocketProcessor implements Runnable {

        private Socket s;
        private InputStream is;
        private OutputStream os;
        //can be "granted", "authorization-failure",
        // "token-request-failure", "data-request-failure", "probe-db-failure"
        private String authorizationStatus = "granted";


        private SocketProcessor(Socket s) throws Throwable {

            this.s = s;
            authorizationLogger.debug("SocketProcessor constructor begin");
            this.is = s.getInputStream();
            this.os = s.getOutputStream();
            authorizationLogger.debug("SocketProcessor constructor ok");
        }

        public void run() {
            try {
                authorizationLogger.debug("in SocketProcessor run\n");

                //find the code in the request
                if(readInputHeaders())
                {
                    //if we have found a code in the headers then

                    if(goProcess())
                        //if account information was successfully fetched from the Google APIs then
                        dbCheck();
                }
                //redirect to the application with the result of authorization procedure
                writeResponse("<html><body><h1>Back to application</h1></body></html>");
            }
            catch (IOException e)
            {
                authorizationLogger.error(e.getMessage());
            }
            catch (Throwable t) {
                authorizationLogger.error(t.getMessage());
                t.printStackTrace(System.err);
                /*do nothing*/
            }
            authorizationLogger.debug("Client processing finished");
        }

        private void writeResponse(String s) throws Throwable {
            String responseString;
            if(authorizationStatus.equals("granted"))
                responseString = "displayname=" + URLEncoder.encode(displayName, "UTF-8") + "&granted=" + URLEncoder.encode("granted", "UTF-8") + "&googleid=" + URLEncoder.encode(id, "UTF-8")+ "&probeid="+URLEncoder.encode(probe_id, "UTF-8");
            else
                responseString = authorizationStatus;

            String response = "HTTP/1.1 302 Found\r\n" +
                    "Location: gnuc-tutorials-oauth://gplus?"+responseString+"\r\n\r\n";
            String result = response + s;
            BufferedWriter bwriter = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"), 8000);
            //os.write(result.getBytes());
            //os.flush();
            bwriter.write(result);
            bwriter.flush();
            bwriter.close();
        }

        private boolean readInputHeaders() throws Throwable {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            authorizationLogger.debug("Reading headers");
            boolean found = false;
            while(true) {
                String s = br.readLine();
                authorizationLogger.debug(s);

                if(s == null || s.trim().length() == 0) {
                    break;
                }
                else
                {
//                    fileWriter.write(s+"\n");
                    if(s.contains("Referer:"))
                    {
                        s = s.replace("Referer: http://ssd1.sscc.ru:6896/?code=", "");
                        code = s;
                        authorizationLogger.debug("Found code: " + code);
                        found = true;
                    }
                    if(s.startsWith("GET /?code"))
                    {
                        s = s.replace("GET /?code=", "");
                        s = s.replace(" HTTP/1.1","");
                        code = s;
                        authorizationLogger.debug("Found code: " + code);
                        found = true;
                    }
                }
            }
            authorizationLogger.debug("---- End of reading headers ----");
            return found;
        }

        private final static String    goID        = "432528404452-8mo088p8aaab4etjv0j6h26cvsj99fl1.apps.googleusercontent.com"; // You will provide your Client ID  from the Google APIs Console.
        private final static String    goSECRET    = "Xh4dwwu3XfJW0QZ4XGh_vQCO"; // You will provide your Client secret from the Google APIs Console.
        private final static String    goRSPURL    = "http://ssd1.sscc.ru:6896"; // You will provide the redirect  URI from the Google APIs Console.
        private final static String    goCALLBK    = "gnuc-tutorials-oauth://gplus"; // This is a user-defined url handle which will be called by the web-service. Will invoke the application automatically.
        private final static String    goSCOPE    = "https://www.googleapis.com/auth/plus.me"; // Read about scoping @ https://developers.google.com
        private final static String    goTOKEN    = "https://accounts.google.com/o/oauth2/token"; // Read more @ http://code.google.com/apis/accounts/docs/OAuth2.html
        private final static String    goACCESS    = "https://accounts.google.com/o/oauth2/auth"; // Read more @ http://code.google.com/apis/accounts/docs/OAuth2.html
        private final static String    goPLUS    = "https://www.googleapis.com/plus/v1/people/me";  // Read about end-point @ https://developers.google.com

        String                                id            = null, name = null;
        String probe_id = null;
        String displayName = null;
        //    Bitmap                                picture    = null;
        String code = null;
        String OAUTH_GO_TOKEN  = null;
/*
        public void oauthRedirect()
        {
            try
            {
                String provider = (String) getRequest().getAttributes().get("handler");
                if (provider.equalsIgnoreCase("GPLUS"))
                {
                    Map<String, String> params = getQuery().getValuesMap();
                    if (null != params && params.isEmpty())
                        redirectPermanent("gnuc-tutorials-oauth://gplus?fail");
                    else
                    {
                        //authorize with Google+ here and then redirect to the mobile phone with some Probing-specific key
                        code = URLEncoder.encode(params.get("code"), "UTF-8");

                        if(-1==goProcess())
                            throw new Exception();

                        redirectPermanent("gnuc-tutorials-oauth://gplus?oauth_code=" + URLEncoder.encode(name, "UTF-8") + "&granted=" +URLEncoder.encode("granted", "UTF-8"));
                    }
                }
            }
            catch (Exception e)
            {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                redirectPermanent("gnuc-tutorials-oauth://social?fail=" + sw.toString());
            }
        }

        void redirectPermanent(String s)
        {
        }
*/
        boolean goProcess()
        {
            try
            {
                String queryString = "code=" +
                        URLEncoder.encode(code, "UTF-8");
                queryString += "&client_id=" +
                        URLEncoder.encode(goID, "UTF-8");
                queryString += "&client_secret=" +
                        URLEncoder.encode(goSECRET, "UTF-8");
                queryString += "&redirect_uri=" +
                        URLEncoder.encode(goRSPURL, "UTF-8");
                queryString += "&grant_type=" +
                        URLEncoder.encode("authorization_code", "UTF-8");

                URL url = new URL(goTOKEN);

                URLConnection urlConnection = url.openConnection();

                ((HttpURLConnection)urlConnection).setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                urlConnection.setRequestProperty("Content-Length", ""+ queryString.length());

                OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8");

                // Write query string to request body
                out.write(queryString);
                out.flush();

                authorizationLogger.debug("QueryString = "+queryString);
                // Read the response
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(urlConnection.getInputStream()));
                authorizationLogger.debug("\nread: " + in.toString()+"\n");
                StringBuilder builder = new StringBuilder();
                int i = 0;
                String[] tokens = {null, null, null};
                for (String line = null; (line = in.readLine()) != null; i++) {
                    builder.append(line).append("\n");
                    if(i == 1)
                    {
                        String delims = "[ :\"]+";
                        tokens = line.split(delims);
                        name = tokens[0] + "-*-" + tokens[1] + "-*-" + tokens[2];
                        authorizationLogger.debug("Got tokens: "+name);
                    }
                }


                out.close();
                in.close();
                OAUTH_GO_TOKEN = tokens[2];

                queryString = "?access_token="+OAUTH_GO_TOKEN;
//                Logger log = Logger.getLogger(AuthorizationService.class.getName());
//                log.severe("addressing: "+goPLUS+queryString);
                authorizationLogger.debug("\naddressing: "+goPLUS+queryString + "\n");

                //requesting user account information
                url = new URL(goPLUS+queryString);
                // Read the response
    //        	in = new BufferedReader(
    //        	new InputStreamReader(urlConnection.getInputStream()));

                in = new BufferedReader( new InputStreamReader(url.openStream()));
                builder = new StringBuilder();

                boolean found = false;

                for (String line = null; (line = in.readLine()) != null; i++) {
                    builder.append(line).append("\n");
                    if(line.contains("\"id\""))
                    {
                        id = line.replace(" \"id\": \"", "");
                        id = id.replace("\",","");
                        authorizationLogger.debug("id: " + id);
                        found = true;
                    }
                    if(line.contains("\"displayName\""))
                    {
                        displayName = line.replace(" \"displayName\": \"", "");
                        displayName = displayName.replace("\",","");
                        authorizationLogger.debug("displayName: " + displayName);
                    }
                }

                if(!found)
                {
                    authorizationLogger.error("Authorization ok, data fetch failed");
                    authorizationStatus = "data-request-failure";
                    return false;
                }
                //just in case
                if(displayName==null)
                {
                    authorizationLogger.error("Suddenly: displayName = null");
                    authorizationStatus = "data-request-failure";
                    return false;
                }

                name = builder.toString();
                //log.severe(name);
                authorizationLogger.debug("Name: "+name);
                return true;

/*                JSONParser parser = new JSONParser();
                ContainerFactory containerFactory = new ContainerFactory(){
                    public List creatArrayContainer() {
                        return new LinkedList();
                    }

                    public Map createObjectContainer() {
                        return new LinkedHashMap();
                    }

                };
*/
/*                try{
                    Map json = (Map)parser.parse(name, containerFactory);

                    Iterator iter = json.entrySet().iterator();
                    System.out.println("==iterate result==");
                    while(iter.hasNext()){
                        Map.Entry entry = (Map.Entry)iter.next();
                        if("id"==entry.getKey())
                            fileWriter.write("id = " + entry.getValue() + "\n");
                        if("displayName"==entry.getKey())
                            fileWriter.write("displayName = " + entry.getValue() + "\n");
                    }
                }
                catch(ParseException pe){
                    fileWriter.write("Parsing error");
                    fileWriter.flush();
                    System.out.println(pe);
                }
                catch(Throwable t)
                {
                    fileWriter.write("failed json\n");
                    fileWriter.flush();
                    t.printStackTrace(System.err);
                }
*/
  /*              {
                    JSONObject me = new JSONObject();
                    me.
                    //
                    if (me.containsKey("id"))
                        id = (String) me.get("id");
                    //
                    if (me.containsKey("displayName"))
                        name = (String) me.get("displayName");
                    //
                }
                */
            }
            catch (Exception e)
            {
                authorizationLogger.error(e.getMessage());
                return false;
            }
        }

        public void dbCheck() {
            Connection conn  = null;
            PreparedStatement pstmt = null;
            ResultSet rs    = null;

            try {

                authorizationLogger.debug("In DB");
/*
                Properties connInfo = new Properties();

                connInfo.put("characterEncoding","UTF8");
                connInfo.put("user", "probing_user");
                connInfo.put("password", "probe105040");

                Class.forName("com.mysql.jdbc.Driver").newInstance();
                authorizationLogger.debug("Driver ok");
                conn  = DriverManager.getConnection("jdbc:mysql://ssd1.sscc.ru:6890/probing?characterEncoding=UTF-8", connInfo);
*/
                conn = DBConnectionFactory.getConnection();
                if(conn!=null)
                {
                    authorizationLogger.debug("Authorization: Connection OK");
                }
                else
                {
                    authorizationLogger.error("Authorization: Connection to DB failed");
                }
                pstmt = conn.prepareStatement("select id, nick_name, google_id from players where google_id='"+id+"';");

                if(pstmt.execute()) {
                    authorizationLogger.debug("Execute ok");
                    rs = pstmt.getResultSet();

                    if(rs.next()) {
                        probe_id = rs.getString("id");
                        authorizationLogger.debug("Found in DB: "+probe_id);
                    }
                    else
                    {
                        pstmt = conn.prepareStatement("select count(*) from players;");
                        pstmt.execute();
                        rs = pstmt.getResultSet();
                        if(!rs.next())
                        {
                            authorizationLogger.error("Select count (*) returned empty result");
                            authorizationStatus = "probe-db-failure";
                            return;
                        }
                        probe_id = Integer.toString(rs.getInt(1));
                        authorizationLogger.debug("New probe_id: "+probe_id);
                        pstmt = conn.prepareStatement("insert players values("+probe_id+"," +
                                                                             "'"+displayName+"'," +
                                                                             "'"+displayName+"'," +
                                                                             "'a@a'," +
                                                                             "1," +
                                                                             "'"+id+"'," +
                                                                             "0);");
                        int insertedRows = pstmt.executeUpdate();
                        authorizationLogger.debug("Insert finished, inserted rows" + String.valueOf(insertedRows));
                    }
                }
            }
            catch (SQLException ex) {
                System.err.println(ex.toString());
                //ex.printStackTrace();
            }
            catch (Throwable t)
            {
                t.printStackTrace(System.err);
            }
            finally {

                try {

                    if (pstmt != null)
                        pstmt.close();

                    if (conn != null)
                        conn.close();
                }
                catch (SQLException ex) {
                    authorizationLogger.error("On close: " + ex.getMessage());
                    //ex.printStackTrace();
                }

            }
        }

    }
}

