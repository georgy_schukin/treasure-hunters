package ru.ssd.probing.server.communication;

import ru.ssd.probing.common.model.Group;
import ru.ssd.probing.common.model.GroupProperties;
import ru.ssd.probing.common.model.Player;
import ru.ssd.probing.common.model.Position;

/**
 * © Ruslan Mustakov
 */


public interface CommunicationManagerListener {
    public void onLFGStateChangeRequested(Player player, boolean isLFG);
    public void onPositionChangeRequested(Player player, Position newPosition);
    public void onGroupPropertiesChangeRequested(Player leader, GroupProperties properties);
    public void onFlagRequested(Player player, Position flagPosition);
    public void onFlagRemovalRequested(Player player, Position flagPosition);
    public void onTreasurePutRequested(Player player, Position treasurePosition);
    public void onScanStateChangeRequested(Player player, boolean isScanning);
    public void onScanRadiusIndexChangeRequested(Player player, int newScanRadiusIndex);
    public void onMemberKickRequested(Player leader, String memberName);
    public void onLeadershipChangeRequested(Player currentLeader, Player newLeader);
    public void onLeaveGroupRequested(Player player);
    public void onTreasureRequested(Player player, Position treasurePosition);
    public void onPlayerDisconnected(Player player);
    public void onNewPlayer(Player player);
    public void onNewGroup(Group group);

    public void onInvitationSent(Player sender, String whomToInvite);
    public void onInvitationAccepted(Player player, String groupName);
    public void onGroupCreationRequested(Player player);
}
