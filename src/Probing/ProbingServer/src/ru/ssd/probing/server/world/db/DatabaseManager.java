package ru.ssd.probing.server.world.db;

import ru.ssd.probing.common.model.Position;
import ru.ssd.probing.common.model.Treasure;

import java.sql.SQLException;
import java.util.Set;

/**
 * © Ruslan Mustakov
 */


public interface DatabaseManager {
    public Treasure getTreasureIfAssigned(String playerName, Position position) throws SQLException;
    public void assignTreasureToGroup(Set<String> playerNames, Position position) throws SQLException;
    public Set<Position> getTreasurePositionsInRadius(Position center, int radius) throws SQLException;
    public void addTreasureToDatabase(Treasure treasure) throws SQLException;
}
