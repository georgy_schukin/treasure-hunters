package ru.ssd.probing.server.world;

import ru.ssd.probing.common.model.*;

import java.util.List;
import java.util.Set;

/**
 * © Ruslan Mustakov
 */

public interface WorldManagerListener {
    public void onGroupFormed(Group group);
    public void onMemberJoined(Group group, Player newMember);
    public void onPlayerInvited(Group group, Player who);
    public void onGroupPropertiesChanged(Group group);
    public void onPositionChanged(Player player);
    public void onFlagPut(Group group, Flag flag);
    public void onFlagRemoved(Group group, Position flagPosition);
    public void onScanStateChanged(Player player);
    public void onScanRadiusChanged(Player player);
    public void onLeaderChanged(Group group, Player oldLeader);
    public void onGroupMemberLeft(Group group, Player player);
    public void onTreasureEarned(Group group, Treasure treasure);
    public void onScanStarted(Group group);
    public void onScanFinished(Group group, Set<Signal> foundSignals);
    public void onLFGInterest(List<Group> groups, Player player);


}
