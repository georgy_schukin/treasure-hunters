package ru.ssd.probing.server.communication;

import ru.ssd.probing.common.model.*;
import ru.ssd.probing.common.protocol.ServerResponse;

import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.Set;

/**
 * © Ruslan Mustakov
 */

public interface CommunicationManager {
    public void dispatchFlagPutEvent(Group group, Flag flag);
    public void dispatchFlagRemoved(Group group, Position flagPosition);
    public void dispatchLFGPlayerEvent(List<Group> groups, Player player, boolean lfgState);
    public void dispatchMemberPositionChanged(Group group, Player member);
    public void dispatchMemberScanRadiusChanged(Group group, Player member);
    public void dispatchMemberScanReadyStateChanged(Group group, Player member);
    public void dispatchMemberLeft(Group group, Player leftMember);
    public void dispatchLeaderChanged(Group group);
    public void dispatchGroupScanStarted(Group group);
    //TODO: dispatchScanInterrupted
    public void dispatchFoundSignals(Group group, List<Signal> signals);
    public void sendGroupAssigned(Group group, Player player);

    public void sendResponse(Player player, ServerResponse response);

    public void sendTreasure(Player player, Treasure treasure);

    public void sendInvitationReceived(Player player, String groupName, Set<String> memberNames);
//
    public void sendLeftGroup(Player player);



    //candidates to be removed from the interface class
    public void associatePlayerWithSocket(Player player, SocketChannel socket);

    public void addListener(CommunicationManagerListener listener);
    public void removeListener(CommunicationManagerListener listener);

    //removed from the interface class
    //public Player getPlayerBySocket(SocketChannel socket);
    //public boolean checkPlayerInGame(Player player);
    //public void drop(Player player);
}
