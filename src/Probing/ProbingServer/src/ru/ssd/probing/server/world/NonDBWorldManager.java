package ru.ssd.probing.server.world;

import org.apache.log4j.Logger;
import ru.ssd.probing.common.model.*;
import ru.ssd.probing.server.world.db.DatabaseManager;
import ru.ssd.probing.server.world.db.DatabaseManagerImpl;
import ru.ssd.probing.server.world.geom.Matrix;
import ru.ssd.probing.server.world.geom.Utils;
import ru.ssd.probing.server.world.geom.Vector3D;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * © Ruslan Mustakov
 */


public class NonDBWorldManager implements WorldManager {
    private final Set<WorldManagerListener> listeners;

    private final Map<String, Group> groups;
    private final Map<String, Player> players;

    private final Map<Player, Group> playerGroupMap;

    private final Map<Group, Set<Player>> invitationMap;
    private final Map<Group, Set<Player>> shownLFGMap;

    private final Timer scanTimer;
    private final Timer invitationExpirationTimer;

    private final DatabaseManager databaseManager;

    private static final Logger worldLogger = Logger.getLogger("World");

    private AtomicInteger currentGroupId = new AtomicInteger();

    private static final Random random = new Random();

    private static final int INVITATION_EXPIRATION_DELAY = 1 * 60 * 1000; //1 minute

    public NonDBWorldManager() throws SQLException {
        listeners = Collections.synchronizedSet(new HashSet<WorldManagerListener>());

        groups = Collections.synchronizedMap(new HashMap<String, Group>());
        players = Collections.synchronizedMap(new HashMap<String, Player>());
        playerGroupMap = Collections.synchronizedMap(new HashMap<Player, Group>());
        invitationMap = Collections.synchronizedMap(new HashMap<Group, Set<Player>>());
        shownLFGMap = Collections.synchronizedMap(new HashMap<Group, Set<Player>>());

        scanTimer = new Timer();
        invitationExpirationTimer = new Timer();

        databaseManager = new DatabaseManagerImpl();

        worldLogger.debug("World manager successfully initialized");
    }

    @Override
    public void setPlayerPosition(String playerName, Position position) throws NoSuchPlayerException {
        Player player = players.get(playerName);
        if (player != null) {
            synchronized (player) {
                if (!position.equals(player.getPosition())) {
                    player.setPosition(position);

                    worldLogger.debug(playerName + " changed position to " + position);

                    for (WorldManagerListener listener : listeners) {
                        listener.onPositionChanged(player);
                    }
                }
                else {
                    worldLogger.debug(playerName + " is on the same position (" + position + ")");
                }
            }
        }
        else {
            throw new NoSuchPlayerException();
        }
    }

    @Override
    public void setGroupProperties(String leaderName, GroupProperties properties) throws NoSuchPlayerException, NoGroupException, NotALeaderException {
        Player leader = players.get(leaderName);
        if (leader != null) {
            Group group = playerGroupMap.get(leader);
            if (group != null) {
                synchronized (group) {
                    if (leader.equals(group.getLeader())) {
                        group.setProperties(properties);

                        for (WorldManagerListener listener : listeners) {
                            listener.onGroupPropertiesChanged(group);
                        }
                    }
                    else {
                        worldLogger.warn("Not leader player " + leaderName + "tried to change group properties for group " + group + ".");
                        throw new NotALeaderException();
                    }
                }
            }
            else {
                worldLogger.warn("Player without group " + leaderName + " tried to change group properties.");
                throw new NoGroupException();
            }
        }
        else {
            throw new NoSuchPlayerException();
        }
    }

    @Override
    public Group getPlayerGroup(String playerName) throws NoSuchPlayerException {
        Player player = players.get(playerName);
        if (player != null) {
            //TODO: clone?
            return playerGroupMap.get(player);
        }
        else {
            throw new NoSuchPlayerException();
        }
    }

    @Override
    public Flag putFlag(String putPlayerName, Position position) throws NoSuchPlayerException, NoGroupException, TooManyFlagsException, BadPositionException {
        Player player = players.get(putPlayerName);
        if (player != null) {
            Group group = playerGroupMap.get(player);
            if (group != null) {
                synchronized (group) {
                    Set<Flag> currentFlags = group.getFlags();
                    boolean isLeader = player.equals(group.getLeader());
                    ColorPicker colorPicker = new ColorPicker();
                    for (Flag flag : currentFlags) {
                        if (flag.isLeaderFlag() == isLeader) {
                            colorPicker.check(flag.getColor());
                        }
                        if (flag.getPosition().equals(position)) {
                            worldLogger.info("Player " + putPlayerName + "(group " + group + ") tried to put flag on position where another one is already staying.");
                            //TODO: check not exact position but area?
                            throw new BadPositionException();
                        }
                    }
                    Color newFlagColor = colorPicker.getFreeColor();
                    if (newFlagColor != null) {
                        Flag newFlag = new Flag(newFlagColor, position, isLeader);
                        group.addFlag(newFlag);
                        worldLogger.debug("Flag with color " + newFlagColor + " put on " + position + " by group " + group.getName());
                        for (WorldManagerListener listener : listeners) {
                            listener.onFlagPut(group, newFlag);
                        }
                        return newFlag;
                    }
                    else {
                        worldLogger.info("Maximum flag amount exceeded by group " + group);
                        throw new TooManyFlagsException();
                    }
                }
            }
            else {
                worldLogger.warn("Player without group " + player + " tried to put flag");
                throw new NoGroupException();
            }
        }
        else {
            throw new NoSuchPlayerException();
        }
    }

    @Override
    public void removeFlag(String whoRemoves, Position flagPosition) throws NoSuchPlayerException, NoGroupException, BadPositionException {
        Player player = players.get(whoRemoves);
        if (player != null) {
            synchronized (player) {
                Group group = playerGroupMap.get(player);
                if (group != null) {
                    boolean isLeader = group.getLeader().equals(player);
                    boolean flagRemoved = false;
                    for (Flag flag : group.getFlags()) {
                        if (flag.getPosition().equals(flagPosition)) {
                            if (flag.isLeaderFlag() && isLeader || !flag.isLeaderFlag()) {
                                group.removeFlag(flag);
                                for (WorldManagerListener listener : listeners) {
                                    listener.onFlagRemoved(group, flag.getPosition());
                                }
                                flagRemoved = true;
                                break;
                            }
                        }
                    }
                    if (!flagRemoved) {
                        throw new BadPositionException();
                    }
                }
                else {
                    throw new NoGroupException();
                }
            }
        }
        else {
            throw new NoSuchPlayerException();
        }
    }

    @Override
    public void setScanState(String playerName, boolean isScanning) throws NoSuchPlayerException {
        //TODO: remove the state if player moved
        Player player = players.get(playerName);
        if (player != null) {
            synchronized (player) {
                worldLogger.debug("Setting scan state for " + playerName + " to " + isScanning);
                if (player.isReadyToScan() != isScanning) {
                    player.setReadyToScan(isScanning);
                    for (WorldManagerListener listener : listeners) {
                        listener.onScanStateChanged(player);
                    }
                    if (isScanning) {
                        Group group = playerGroupMap.get(player);
                        if (group != null) {
                            synchronized (group) {
                                boolean  everyoneReady = true;
                                for (Player member : group.getPlayers()) {
                                    if (!member.isReadyToScan()) {
                                        everyoneReady = false;
                                        break;
                                    }
                                }
                                if (everyoneReady) {
                                    worldLogger.debug("scheduling group scan for group " + group);
                                    //TODO: fix it so it'll make sense
                                    scanTimer.schedule(new ScanWorker(group.getName()), ProbeProperties.SCAN_TIME);
                                    for (WorldManagerListener listener : listeners) {
                                        listener.onScanStarted(group);
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    worldLogger.warn("requested change to the same scan state for player" + player.getName() + ".");
                }
            }
        }
        else {
            throw new NoSuchPlayerException();
        }
    }

    @Override
    public void setScanRadiusIndex(String playerName, int index) throws NoSuchPlayerException, BadScanRadiusIndexException {
        if (index < 0 || index >= ProbeProperties.possibleRadius.size()) {
            throw new BadScanRadiusIndexException();
        }
        Player player = players.get(playerName);
        if (player != null) {
            synchronized (player) {
                ProbeProperties currentProperties = player.getProbeProperties();
                if (index != currentProperties.getScanRadiusIndex()) {
                    currentProperties.setScanRadiusIndex(index);
                    player.setProbeProperties(currentProperties);
                    for (WorldManagerListener listener : listeners) {
                        listener.onScanRadiusChanged(player);
                    }
                }
            }
        }
    }

    @Override
    public Player getPlayerByName(String name) {
        return players.get(name);
    }

    @Override
    public Group getGroupByName(String name) {
        return groups.get(name);
    }

    @Override
    public void removeMemberFromGroup(String groupName, String memberName) throws NoSuchPlayerException, NoGroupException, BadGroupException {
        Player player = players.get(memberName);
        if (player != null) {
            synchronized (player) {
                Group group = groups.get(groupName);
                if (group != null) {
                    if (group.equals(playerGroupMap.get(player))) {
                        synchronized (group) {
                            boolean removeMember = true;
                            playerGroupMap.remove(player);
                            if (player.equals(group.getLeader())) {
                                Set<Player> groupPlayers = group.getPlayersOrdered();
                                if (groupPlayers.size() > 1) {
                                    for (Player member : groupPlayers) {
                                        if (!member.equals(player)) {
                                            setGroupLeader(groupName, member.getName());
                                        }
                                    }
                                }
                                else {
                                    worldLogger.debug("Group " + group.getName() + " is disbanded");
                                    groups.remove(groupName);
                                    invitationMap.remove(group);
                                    shownLFGMap.remove(group);
                                    removeMember = false;
                                    for (WorldManagerListener listener : listeners) {
                                        listener.onGroupMemberLeft(null, player);
                                    }
                                }
                            }
                            if (removeMember) {
                                worldLogger.debug(player.getName() + " left group " + group.getName());
                                group.removePlayer(player);
                                for (WorldManagerListener listener : listeners) {
                                    listener.onGroupMemberLeft(group, player);
                                }
                            }
                        }
                    }
                    else {
                        throw new BadGroupException();
                    }
                }
                else {
                    throw new NoGroupException();
                }
            }
        }
        else {
            throw new NoSuchPlayerException();
        }
    }

    @Override
    public void setGroupLeader(String groupName, String leaderName) throws NoGroupException, NoSuchPlayerException, BadGroupException {
        Group group = groups.get(groupName);
        if (group != null) {
            Player player = players.get(leaderName);
            if (player != null) {
                synchronized (group) {
                    if (group.getPlayers().contains(player)) {
                        Player currentLeader = group.getLeader();
                        group.changeLeader(player);
                        for (WorldManagerListener listener : listeners) {
                            listener.onLeaderChanged(group, currentLeader);
                        }
                    }
                    else {
                        throw new BadGroupException();
                    }
                }
            }
            else {
                throw new NoSuchPlayerException();
            }
        }
        else {
            throw new NoGroupException();
        }
    }

    @Override
    public void createGroup(String leaderName) throws NoSuchPlayerException, AlreadyInGroupException {
        Player leader = players.get(leaderName);
        if (leader != null) {
            synchronized (leader) {
                Group group = playerGroupMap.get(leader);
                if (group == null) {
                    setPlayerLFGState(leaderName, false);
                    Group newGroup = new Group("Group-" + currentGroupId.addAndGet(1), leader);
                    synchronized (newGroup) {
                        //TODO: check LFG players
                        groups.put(newGroup.getName(), newGroup);
                        playerGroupMap.put(leader, newGroup);
                        invitationMap.put(newGroup, new HashSet<Player>());
                        shownLFGMap.put(newGroup, new HashSet<Player>());

                        worldLogger.debug("Created group " + newGroup.getName() + " by " + leader.getName());

                        for (WorldManagerListener listener : listeners) {
                            listener.onGroupFormed(newGroup);
                        }

                        List<Group> theGroupList = new ArrayList<Group>(1);
                        theGroupList.add(newGroup);
                        //TODO: optimize
                        synchronized (players) {
                            for (Player player : players.values()) {
                                if (player.isLFG() && player.getPosition().getDistance(leader.getPosition()) <= ProbeProperties.maxPossibleScanRadius()) {
                                    shownLFGMap.get(newGroup).add(player);
                                    worldLogger.debug(newGroup.getName() + " is interested in " + player.getName() + ". Adding to the list...");
                                    for (WorldManagerListener listener : listeners) {
                                        listener.onLFGInterest(theGroupList, player);
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    throw new AlreadyInGroupException();
                }
            }
        }
        else {
            throw new NoSuchPlayerException();
        }
    }

    @Override
    public void setPlayerLFGState(String playerName, boolean isLFG) throws NoSuchPlayerException, AlreadyInGroupException {
        /*Player player = players.get(playerName);
        if (player != null && player.isLFG() != isLFG) {
            if (isLFG) {
                Group group = playerGroupMap.get(player);
                if (group != null) {
                    throw new AlreadyInGroupException();
                }
                worldLogger.debug(playerName + " is now looking for group");
                int memberCount = 1;
                List<Player> possibleMembers = new ArrayList<Player>(MIN_PLAYERS_FOR_GROUP - 1);
                if (memberCount != MIN_PLAYERS_FOR_GROUP) {
                    synchronized (players) {
                        for (Player otherPlayer : players.values()) {
                            if (otherPlayer.getPosition().getDistance(otherPlayer.getPosition()) <= MAX_GROUP_ADD_DISTANCE) {
                                if (otherPlayer.isLFG()) {
                                    possibleMembers.add(otherPlayer);
                                    ++memberCount;
                                    if (memberCount == MIN_PLAYERS_FOR_GROUP) { break; }
                                }
                            }
                        }
                    }
                }
                if (memberCount == MIN_PLAYERS_FOR_GROUP) {
                    Group newGroup = new Group("Group-" + currentGroupId.addAndGet(1), player);
                    groups.put(newGroup.getName(), newGroup);
                    playerGroupMap.put(player, newGroup);
                    worldLogger.debug(player + " assigned to group " + newGroup);
                    for (Player otherPlayer : possibleMembers) {
                        otherPlayer.setLFG(false);
                        newGroup.addPlayer(otherPlayer);
                        playerGroupMap.put(otherPlayer, newGroup);
                        worldLogger.debug(otherPlayer + " assigned to group " + newGroup);
                    }
                    for (WorldManagerListener listener : listeners) {
                        listener.onGroupFormed(newGroup);
                    }
                }
                else {
                    player.setLFG(true);
                }
            }
            else {
                player.setLFG(false);
            }
        }
        else {
            throw new NoSuchPlayerException();
        }*/
        //TODO: auto accept member
        Player player = players.get(playerName);
        if (player != null) {
            synchronized (player) {
                if (player.isLFG() != isLFG) {
                    if (isLFG) {
                        Group possibleGroup = playerGroupMap.get(player);
                        if (possibleGroup == null) {
                            player.setLFG(isLFG);
                            worldLogger.debug(playerName + " is now looking for group");
                            //TODO: optimize
                            List<Group> groupsOfInterest = new ArrayList<Group>();
                            synchronized (groups) {
                                for (Group group : groups.values()) {
                                    synchronized (group) {
                                        if (group.getLeader().getPosition().getDistance(player.getPosition()) <= ProbeProperties.maxPossibleScanRadius() &&
                                            group.getGroupProperties().isInterestedInNewMembers()) {
                                            shownLFGMap.get(group).add(player);
                                            worldLogger.debug(group.getName() + " is interested in " + playerName + ". Adding to the list...");
                                            groupsOfInterest.add(group);
                                        }
                                    }
                                }
                            }
                            for (WorldManagerListener listener : listeners) {
                                listener.onLFGInterest(groupsOfInterest, player);
                            }
                        }
                        else {
                            throw new AlreadyInGroupException();
                        }
                    }
                    else {
                        player.setLFG(isLFG);
                        worldLogger.debug(playerName + " is finished looking for group");
                        //TODO: optimize
                        List<Group> groupsOfInterest = new ArrayList<Group>();
                        synchronized (groups) {
                            for (Group group : groups.values()) {
                                synchronized (group) {
                                    if (shownLFGMap.get(group).remove(player)) {
                                        groupsOfInterest.add(group);
                                    }
                                }
                            }
                        }
                        for (WorldManagerListener listener : listeners) {
                            listener.onLFGInterest(groupsOfInterest, player);
                        }
                    }
                }
            }
        }
        else {
            throw new NoSuchPlayerException();
        }
    }

    @Override
    public Treasure getTreasureOnPositionForPlayer(String playerName, Position position) {
        Treasure treasure = null;
        try {
            worldLogger.debug("Player " + playerName + " requested treasure on position " + position);
            treasure = databaseManager.getTreasureIfAssigned(playerName, position);
        }
        catch (SQLException e) {
            worldLogger.error("SQL Error: " + e.getMessage());
        }
        return treasure;
    }

    @Override
    public void addPlayerInWorld(Player player) {
        worldLogger.debug("Player " + player + " joined to world");
        players.put(player.getName(), player);
    }

    @Override
    public void removePlayerFromWorld(String playerName) {
        //TODO: consistency problems?
        Player player = players.get(playerName);
        worldLogger.debug("removePlayerFromWorld called for player " + player);
        if (player != null) {
            synchronized (player) {
                Group group = playerGroupMap.get(player);
                if (group != null) {
                    try {
                        removeMemberFromGroup(group.getName(), player.getName());
                    }
                    catch (Exception e) {
                        //will not be thrown
                    }
                }
                players.remove(playerName);
            }
            worldLogger.debug("Removed player " + playerName + " from world");
        }
    }

    @Override
    public void playerInvited(String groupLeaderName, String playerName) throws NoGroupException, NoSuchPlayerException, NotALeaderException, NotLFGException, AlreadyInvitedException {
        Player leader = players.get(groupLeaderName);
        if (leader != null) {
            Group group = playerGroupMap.get(leader);
            if (group != null) {
                Player invitedPlayer = players.get(playerName);
                //isLFG must guarantee that the player is not already in a group
                if (invitedPlayer != null) {
                    synchronized (invitedPlayer) {
                        if (invitedPlayer.isLFG()) {
                            synchronized (group) {
                                worldLogger.debug(groupLeaderName + "invited " + playerName + " to their group " + group.getName());
                                Set<Player> invitedPlayers = invitationMap.get(group);
                                invitationExpirationTimer.schedule(new InvitationExpirationWorker(group.getName(), playerName), INVITATION_EXPIRATION_DELAY);
                                if (invitedPlayers.add(invitedPlayer)) {
                                    for (WorldManagerListener listener : listeners) {
                                        listener.onPlayerInvited(group, invitedPlayer);
                                    }
                                }
                                else {
                                    throw new AlreadyInvitedException();
                                }
                            }
                        }
                        else {
                            worldLogger.warn(groupLeaderName + "invited " + playerName + " who was not LFG");
                            throw new NotLFGException();
                        }
                    }
                }
                else {
                    throw new NotLFGException();
                }
            }
            else {
                throw new NoGroupException();
            }
        }
        else {
            throw new NoSuchPlayerException();
        }
    }

    @Override
    public void invitationAccepted(String playerName, String groupName) throws NoSuchPlayerException, NoInvitationException, AlreadyInGroupException {
        Player player = players.get(playerName);
        if (player != null) {
            Group group = groups.get(groupName);
            if (group != null) {
                synchronized (player) {
                    Group playerGroup = playerGroupMap.get(player);
                    if (playerGroup == null) {
                        synchronized (group) {
                            worldLogger.debug(playerName + " has accepted invitation to group " + groupName);
                            Set<Player> invitedPlayers = invitationMap.get(group);
                            if (invitedPlayers.remove(player)) {
                                try {
                                    setPlayerLFGState(player.getName(), false);
                                }
                                catch (AlreadyInGroupException e) {
                                    //won't be thrown as we pass "false" parameter
                                }
                                catch (NoSuchPlayerException e) {
                                    //won't be thrown
                                }
                                group.addPlayer(player);
                                playerGroupMap.put(player, group);
                                for (WorldManagerListener listener : listeners) {
                                    listener.onMemberJoined(group, player);
                                }
                            }
                            else {
                                throw new NoInvitationException();
                            }
                        }
                    }
                    else {
                        throw new AlreadyInGroupException();
                    }
                }
            }
            else {
                throw new NoInvitationException();
            }
        }
        else {
            throw new NoSuchPlayerException();
        }
    }

    @Override
    public void putTreasureOnPosition(String playerName, Position position) throws ServerErrorException {
        Treasure treasure = new Treasure("Congratulations! You've opened test treasure!", position);
        worldLogger.debug("Adding test treasure on position " + position + " to database...");
        try {
            databaseManager.addTreasureToDatabase(treasure);
            worldLogger.debug("Player " + playerName + " put test treasure on position " + position);
        }
        catch (SQLException e) {
            worldLogger.error("while trying to put treasure (by player " + playerName + " on position " + position + "): " + e.getMessage());
            throw new ServerErrorException();
        }
    }

    @Override
    public void addListener(WorldManagerListener listener) {
        if (listener != null) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeListener(WorldManagerListener listener) {
        listeners.remove(listener);
    }

    private static class ColorPicker {
        private static final List<Color> colorList;
        private boolean[] isChecked;

        static {
            colorList = new ArrayList<Color>();
            colorList.add(Color.RED);
            colorList.add(Color.GREEN);
            colorList.add(Color.BLUE);
            colorList.add(Color.PINK);
            colorList.add(Color.AQUA);
            colorList.add(Color.SILVER);
            colorList.add(Color.BROWN);
            colorList.add(Color.BLACK);
            colorList.add(Color.WHITE);
        }

        public ColorPicker() {
            isChecked = new boolean[colorList.size()];
            Arrays.fill(isChecked, false);
        }

        public void check(Color color) {
            if (color == null) { return; }

            for (int i = 0; i < colorList.size(); ++i) {
                if (colorList.get(i).equals(color)) {
                    isChecked[i] = true;
                    break;
                }
            }
        }

        public void uncheck(Color color) {
            if (color == null) { return; }

            for (int i = 0; i < colorList.size(); ++i) {
                if (colorList.get(i).equals(color)) {
                    isChecked[i] = false;
                    break;
                }
            }
        }

        public Color getFreeColor() {
            Color result = null;
            for (int i = 0; i < isChecked.length; ++i) {
                if (!isChecked[i]) {
                    result = colorList.get(i);
                    break;
                }
            }
            return result;
        }
    }

    private class ScanWorker extends TimerTask {
        private final String groupName;

        private static final double LATITUDE_MICRODEGREES_PER_METER = 6.371009; //fix?

        private static final int MAX_SIGNAL_PERCENTAGE = 100;

        private static final int SCAN_CIRCLE_RADIUS_FIXATION = 10;

        public ScanWorker(String groupName) {
            this.groupName = groupName;
        }

        @Override
        public void run() {
            Group group = groups.get(groupName);
            if (group != null) {
                synchronized (group) {
                    worldLogger.debug("Initializing signal generation for group " + group + "...");
                    ScanCircleOverlapHelper overlapHelper = new RasterScanCircleOverlapHelper();
                    List<ScanCircle> scanCircles = new ArrayList<ScanCircle>(group.getPlayers().size());
                    Set<Position> possibleTreasurePositions = new HashSet<Position>();
                    Set<String> memberNames = new HashSet<String>();
                    int maxScanRadius = 0;
                    for (Player player : group.getPlayers()) {
                        ScanCircle scanCircle = new ScanCircle(player.getPosition(),
                                                               player.getProbeProperties().getScanRadius(),
                                                               player.getProbeProperties().getScanStrength());
                        if (scanCircle.getRadius() > maxScanRadius) {
                            maxScanRadius = scanCircle.getRadius();
                        }
                        scanCircles.add(scanCircle);
                        try {
                            Set<Position> treasuresAroundPlayer =
                                databaseManager.getTreasurePositionsInRadius(player.getPosition(), scanCircle.getRadius());
                            possibleTreasurePositions.addAll(treasuresAroundPlayer);
                        }
                        catch (SQLException e) {
                            worldLogger.error("SQL Error: " + e.getMessage());
                        }
                        memberNames.add(player.getName());
                        try {
                            setScanState(player.getName(), false);
                        }
                        catch (NoSuchPlayerException e) {}
                    }
                    Set<Signal> signals = new HashSet<Signal>();
                    for (Position possiblePosition : possibleTreasurePositions) {
                        ScanCircleOverlapHelper.OverlapInfo overlapInfo =
                                overlapHelper.getOverlapInfo(scanCircles, possiblePosition);
                        if (overlapInfo.getOverlapCount() != 0) {
                            worldLogger.debug("Treasure on " + possiblePosition + "; group " + group +
                                    "\nOverlap area: " + overlapInfo.getOverlapArea() +
                                    "\nOverlap count: " + overlapInfo.getOverlapCount() +
                                    "\nDistance from leader: " + possiblePosition.getDistance(group.getLeader().getPosition()));
                            int meterDispersion = (int)(Math.sqrt(overlapInfo.getOverlapArea() / Math.PI) / Math.pow(overlapInfo.getOverlapCount(), 2) - ProbeProperties.minPossibleScanRadius());
                            if (meterDispersion < 0) {
                                meterDispersion = 0;
                            }
                            worldLogger.debug("Calculated signal meter dispersion " + meterDispersion + " for group " + group);
                            int percentage = 100 - (int)(((double)meterDispersion / maxScanRadius) * 100);
                            if (percentage == MAX_SIGNAL_PERCENTAGE - 1) {
                                percentage = MAX_SIGNAL_PERCENTAGE;
                            }

                            if (percentage == MAX_SIGNAL_PERCENTAGE) {
                                meterDispersion = 0;
                            }

                            int signalLatitude = (int)(random.nextGaussian() * (meterDispersion * LATITUDE_MICRODEGREES_PER_METER) + possiblePosition.getLatitude());
                            double longitudeMicrodegreesPerMeter = Math.cos(Math.toRadians(possiblePosition.getLatitude() / 1e6)) * 1e-6 * Utils.EARTH_RADIUS;
                            int signalLongitude = (int)(random.nextGaussian() * (meterDispersion * longitudeMicrodegreesPerMeter) + possiblePosition.getLongitude());
                            Position signalPosition = fixSignalPosition(new Position(signalLatitude, signalLongitude), scanCircles);
                            worldLogger.debug("Generated signal for group " + group.getName() +
                                              "\nwith strength " + percentage +
                                              "\non position " + signalPosition +
                                              "\n(" + signalPosition.getDistance(group.getLeader().getPosition()) + " meters from leader)");
                            Signal signal = new Signal(percentage, signalPosition, Signal.SignalType.UNKNOWN);
                            signals.add(signal);
                            if (percentage == MAX_SIGNAL_PERCENTAGE) {
                                try {
                                    databaseManager.assignTreasureToGroup(memberNames, signal.getPosition());
                                }
                                catch (SQLException e) {
                                    worldLogger.error("SQL Error: " + e.getMessage());
                                }
                            }
                        }
                    }
                    worldLogger.debug("Total " + signals.size() + " signals generated for group " + group + ". Sending...");
                    for (WorldManagerListener listener : listeners) {
                        listener.onScanFinished(group, signals);
                    }
                }
            }
        }

        private Position fixSignalPosition(Position signalPosition, List<ScanCircle> scanCircles) {
            boolean first = true;
            ScanCircle closestCircle = null;
            int closestDistance = 0;
            for (ScanCircle scanCircle : scanCircles) {
                if (scanCircle.contains(signalPosition)) {
                    return signalPosition;
                }
                if (first) {
                    closestCircle = scanCircle;
                    closestDistance = closestCircle.getCenter().getDistance(signalPosition) - closestCircle.getRadius();
                    first = false;
                }
                else {
                    if (scanCircle.getCenter().getDistance(signalPosition) - scanCircle.getRadius() < closestDistance) {
                        closestCircle = scanCircle;
                        closestDistance = closestCircle.getCenter().getDistance(signalPosition) - closestCircle.getRadius();
                    }
                }
            }
            Vector3D centerVector = Utils.vectorFromPosition(closestCircle.getCenter());
            double twoLen = Math.sqrt(centerVector.getX() * centerVector.getX() +
                    centerVector.getY() * centerVector.getY());
            double xOne = centerVector.getX() / twoLen;
            double yOne = centerVector.getY() / twoLen;
            double[][] xzMatrixData = {{xOne,  yOne, 0},
                    {-yOne, xOne, 0},
                    {0,     0,    1}};
            double zOne = centerVector.getZ() / centerVector.length();
            double twoOne = twoLen / centerVector.length();
            double[][] zMatrixData = {{zOne,   0, -twoOne},
                    {0,      1, 0},
                    {twoOne, 0, zOne}};
            Matrix xzMatrix = new Matrix(xzMatrixData);
            Matrix zMatrix = new Matrix(zMatrixData);
            Vector3D resultCenter = zMatrix.multiply(xzMatrix.multiply(centerVector));
            Vector3D signalVector = Utils.vectorFromPosition(signalPosition);
            Vector3D resultSignalVector = zMatrix.multiply(xzMatrix.multiply(signalVector));

            double x1 = resultCenter.getX();
            double y1 = resultCenter.getY();

            double x2 = resultSignalVector.getX();
            double y2 = resultSignalVector.getY();

            double dx = x2 - x1;
            double dy = y2 - y1;
            double dr = Math.sqrt(dx * dx + dy * dy);
            double D = x1 * y2 - x2 * y1;
            double r = closestCircle.getRadius() - SCAN_CIRCLE_RADIUS_FIXATION;

            double discriminant = r * r * dr * dr - D * D;

            double sign = dy < 0 ? -1. : 1.;
            double interX1 = (D * dy + sign * dx * Math.sqrt(discriminant)) / (dr * dr);
            double interY1 = (-D * dx + Math.abs(dy) * Math.sqrt(discriminant)) / (dr * dr);

            double interX2 = (D * dy - sign * dx * Math.sqrt(discriminant)) / (dr * dr);
            double interY2 = (-D * dx - Math.abs(dy) * Math.sqrt(discriminant)) / (dr * dr);

            double resX;
            double resY;
            if (Utils.distance(x2, y2, interX1, interY1) < Utils.distance(x2, y2, interX2, interY2)) {
                resX = interX1;
                resY = interY1;
            }
            else {
                resX = interX2;
                resY = interY2;
            }

            Vector3D resultSignalRotated = new Vector3D(resX, resY, resultSignalVector.getZ());
            Matrix inverseXZMatrix = xzMatrix.inverse();
            Matrix inverseZMatrix = zMatrix.inverse();
            Vector3D resultSignalOriginal = inverseXZMatrix.multiply(inverseZMatrix.multiply(resultSignalRotated));

            return Utils.positionFromVector(resultSignalOriginal);
        }
    }

    private class InvitationExpirationWorker extends TimerTask {
        private final String groupName;
        private final String playerName;

        private InvitationExpirationWorker(String groupName, String playerName) {
            this.groupName = groupName;
            this.playerName = playerName;
        }

        @Override
        public void run() {
            Player player = players.get(playerName);
            Group group = groups.get(groupName);
            if (player != null && group != null) {
                synchronized (player) {
                    synchronized (group) {
                        if (invitationMap.get(group).remove(player)) {
                            worldLogger.debug("Invitation to group " + group.getName() + " for player " + player.getName() + " has expired.");
                        }
                    }
                }
            }
        }
    }
}
