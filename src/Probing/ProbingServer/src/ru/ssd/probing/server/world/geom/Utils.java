package ru.ssd.probing.server.world.geom;

import ru.ssd.probing.common.model.Position;

/**
 * © Ruslan Mustakov
 */


public class Utils {

    public static final double EARTH_RADIUS = 6371009.;

    public static Vector3D vectorFromPosition(Position position) {
        //TODO: make it more accurate
        double radiansLatitude = Math.toRadians(position.getLatitude() / 1e6);
        double radiansLongitude = Math.toRadians(position.getLongitude() / 1e6);
        double x = EARTH_RADIUS * Math.cos(radiansLatitude) * Math.cos(radiansLongitude);
        double y = EARTH_RADIUS * Math.cos(radiansLatitude) * Math.sin(radiansLongitude);
        double z = EARTH_RADIUS * Math.sin(radiansLatitude);
        return new Vector3D(x,y,z);
    }

    public static Position positionFromVector(Vector3D vector3D) {
        //TODO: make it more accurate
        int latitude = (int)(Math.toDegrees(Math.asin(vector3D.getZ() / EARTH_RADIUS)) * 1e6);
        int longitude = (int)(Math.toDegrees(Math.atan2(vector3D.getY(),  vector3D.getX())) * 1e6);
        return new Position(latitude, longitude);
    }

    public static double distance(double x1, double y1, double x2, double y2) {
        return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }
}
