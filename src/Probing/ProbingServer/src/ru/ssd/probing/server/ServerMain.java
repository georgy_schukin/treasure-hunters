package ru.ssd.probing.server;

import org.apache.log4j.PropertyConfigurator;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * © Ruslan Mustakov
 */


public class ServerMain {
    private static final String LOG_PROPERTIES_FILE_NAME = "log.properties";

    public static void main(String[] args) {
        try {
            PropertyConfigurator.configure(new URL(ServerMain.class.getProtectionDomain().getCodeSource().getLocation(), LOG_PROPERTIES_FILE_NAME).getFile());
        }
        catch (MalformedURLException e) {
            //will not be thrown
        }
        new ServerGovernor();
    }
}
