package ru.ssd.probing.server.world;

import ru.ssd.probing.common.model.Position;
import ru.ssd.probing.server.world.geom.Matrix;
import ru.ssd.probing.server.world.geom.Utils;
import ru.ssd.probing.server.world.geom.Vector3D;

import java.awt.*;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * © Ruslan Mustakov
 */


public class RasterScanCircleOverlapHelper implements ScanCircleOverlapHelper {
    @Override
    public OverlapInfo getOverlapInfo(List<ScanCircle> circles, Position pointPosition) {
        final int BORDER_MARKER = -1;
        OverlapInfo result = null;
        if (circles != null && pointPosition != null) {
            List<ScanCircle> neededCircles = new ArrayList<ScanCircle>(circles.size());
            int maxRadius = 0;
            for (ScanCircle circle : circles) {
                if (circle.contains(pointPosition)) {
                    neededCircles.add(circle);
                    if (circle.getRadius() > maxRadius) {
                        maxRadius = circle.getRadius();
                    }
                }
            }
            int overlapArea = 0;
            int overlapCount = neededCircles.size();
            if (overlapCount != 0) {
                Vector3D centerVector = Utils.vectorFromPosition(pointPosition);
                double twoLen = Math.sqrt(centerVector.getX() * centerVector.getX() +
                        centerVector.getY() * centerVector.getY());
                double xOne = centerVector.getX() / twoLen;
                double yOne = centerVector.getY() / twoLen;
                double[][] xzMatrixData = {{xOne,  yOne, 0},
                        {-yOne, xOne, 0},
                        {0,     0,    1}};
                double zOne = centerVector.getZ() / centerVector.length();
                double twoOne = twoLen / centerVector.length();
                double[][] zMatrixData = {{zOne,   0, -twoOne},
                        {0,      1, 0},
                        {twoOne, 0, zOne}};
                Matrix xzMatrix = new Matrix(xzMatrixData);
                Matrix zMatrix = new Matrix(zMatrixData);
                Vector3D resultCenter = zMatrix.multiply(xzMatrix.multiply(centerVector));
                int[][] pseudoImage = new int[maxRadius * 4 + 100][maxRadius * 4 + 100];
                for (ScanCircle circle : neededCircles) {
                    Vector3D circleCenter = Utils.vectorFromPosition(circle.getCenter());
                    Vector3D resultCircleCenter = zMatrix.multiply(xzMatrix.multiply(circleCenter));
                    int movedX = (int)resultCircleCenter.getX() + pseudoImage[0].length / 2;
                    int movedY = pseudoImage.length - ((int)resultCircleCenter.getY() + pseudoImage.length / 2);
                    int[][] copyImage = pseudoImage.clone();
                    drawCircle(pseudoImage, movedX, movedY, circle.getRadius(), BORDER_MARKER);
                    bfs(pseudoImage, movedX, movedY, BORDER_MARKER);
                    drawCircle(pseudoImage, movedX, movedY, circle.getRadius(), copyImage);
                }
                for (int i = 0; i < pseudoImage.length; ++i) {
                    for (int j = 0; j < pseudoImage[0].length; ++j) {
                        if (pseudoImage[i][j] == overlapCount) {
                            ++overlapArea;
                        }
                    }
                }
            }
            result = new OverlapInfo(overlapArea, overlapCount);
        }
        return result;
    }

    private static void drawCircle(int[][] pseudoImage, int x0, int y0, int radius, int marker) {
        int x = 0;
        int y = radius;
        int delta = 2 - 2 * radius;
        int error = 0;
        while(y >= 0) {
            if (y0 + y >= 0 && y0 + y < pseudoImage.length && x0 + x >= 0 && x0 + x < pseudoImage[0].length) {
                pseudoImage[y0 + y][x0 + x] = marker;
            }
            if (y0 - y >= 0 && y0 - y < pseudoImage.length && x0 + x >= 0 && x0 + x < pseudoImage[0].length) {
                pseudoImage[y0 - y][x0 + x] = marker;
            }
            if (y0 + y >= 0 && y0 + y < pseudoImage.length && x0 - x >= 0 && x0 - x < pseudoImage[0].length) {
                pseudoImage[y0 + y][x0 - x] = marker;
            }
            if (y0 - y >= 0 && y0 - y < pseudoImage.length && x0 - x >= 0 && x0 - x < pseudoImage[0].length) {
                pseudoImage[y0 - y][x0 - x] = marker;
            }
            error = 2 * (delta + y) - 1;
            if(delta < 0 && error <= 0) {
                ++x;
                delta += 2 * x + 1;
                continue;
            }
            error = 2 * (delta - x) - 1;
            if(delta > 0 && error > 0) {
                --y;
                delta += 1 - 2 * y;
                continue;
            }
            ++x;
            delta += 2 * (x - y);
            --y;
        }
    }

    private static void drawCircle(int[][] pseudoImage, int x0, int y0, int radius, int[][] copyFrom) {
        int x = 0;
        int y = radius;
        int delta = 2 - 2 * radius;
        int error = 0;
        while(y >= 0) {
            if (y0 + y >= 0 && y0 + y < pseudoImage.length && x0 + x >= 0 && x0 + x < pseudoImage[0].length) {
                pseudoImage[y0 + y][x0 + x] = copyFrom[y0 + y][x0 + x];
            }
            if (y0 - y >= 0 && y0 - y < pseudoImage.length && x0 + x >= 0 && x0 + x < pseudoImage[0].length) {
                pseudoImage[y0 - y][x0 + x] = copyFrom[y0 - y][x0 + x];
            }
            if (y0 + y >= 0 && y0 + y < pseudoImage.length && x0 - x >= 0 && x0 - x < pseudoImage[0].length) {
                pseudoImage[y0 + y][x0 - x] = copyFrom[y0 + y][x0 - x];
            }
            if (y0 - y >= 0 && y0 - y < pseudoImage.length && x0 - x >= 0 && x0 - x < pseudoImage[0].length) {
                pseudoImage[y0 - y][x0 - x] = copyFrom[y0 - y][x0 - x];
            }
            error = 2 * (delta + y) - 1;
            if(delta < 0 && error <= 0) {
                ++x;
                delta += 2 * x + 1;
                continue;
            }
            error = 2 * (delta - x) - 1;
            if(delta > 0 && error > 0) {
                --y;
                delta += 1 - 2 * y;
                continue;
            }
            ++x;
            delta += 2 * (x - y);
            --y;
        }
    }

    private static void bfs(int[][] pseudoImage, int x0, int y0, int borderMarker) {
        boolean [][] visited = new boolean[pseudoImage.length][pseudoImage[0].length];
        if (x0 < 0) { x0 = 0; }
        if (y0 < 0) { y0 = 0; }
        if (x0 >= pseudoImage[0].length) { x0 = pseudoImage[0].length - 1; }
        if (y0 >= pseudoImage.length) { y0 = pseudoImage.length - 1; }
        visited[y0][x0] = true;
        Queue<Point> bfsQueue = new ArrayDeque<Point>();
        bfsQueue.add(new Point(x0, y0));
        while (!bfsQueue.isEmpty()) {
            Point point = bfsQueue.remove();
            int x = (int)point.getX();
            int y = (int)point.getY();
            pseudoImage[y][x]++;
            if (x - 1 >= 0 && !visited[y][x - 1] && pseudoImage[y][x - 1] != borderMarker) {
                visited[y][x - 1] = true;
                bfsQueue.add(new Point(x - 1, y));
            }
            if (x + 1 < pseudoImage[0].length && !visited[y][x + 1] && pseudoImage[y][x + 1] != borderMarker) {
                visited[y][x + 1] = true;
                bfsQueue.add(new Point(x + 1, y));
            }
            if (y + 1 < pseudoImage.length && !visited[y + 1][x] && pseudoImage[y + 1][x] != borderMarker) {
                visited[y + 1][x] = true;
                bfsQueue.add(new Point(x, y + 1));
            }
            if (y - 1 >= 0 && !visited[y - 1][x] && pseudoImage[y - 1][x] != borderMarker) {
                visited[y - 1][x] = true;
                bfsQueue.add(new Point(x, y - 1));
            }
        }
    }

}
