package ru.ssd.probing.server.world;

/**
 * © Ruslan Mustakov
 */


public class NotALeaderException extends Exception {
    public NotALeaderException() {
        super();
    }

    public NotALeaderException(String message) {
        super(message);
    }
}
