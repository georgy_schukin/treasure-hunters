package ru.ssd.probing.server.world;

/**
 * © Ruslan Mustakov
 */


public class BadGroupException extends Exception {
    public BadGroupException() {
        super();
    }

    public BadGroupException(String message) {
        super(message);
    }
}
