package ru.ssd.probing.server.communication;

import ru.ssd.probing.common.model.*;
import ru.ssd.probing.common.protocol.ServerResponse;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.*;

public class CommunicationManagerImpl implements CommunicationManager {
    private Map<Player, SocketChannel> playerSocketMap;
    private Map<SocketChannel, Player> socketPlayerMap;
    private NioServer nioServer;

    private static final String SERVER_PROPERTIES_FILE_NAME = "server.properties";
    private static final String PORT_PROPERTY_NAME = "port";

    public CommunicationManagerImpl(CommunicationManagerListener cmListener)
    {
        Properties properties = new Properties();
        FileInputStream propertiesFile = null;
        String propertiesPath = null;
        try {
            propertiesPath = new URL(this.getClass().getProtectionDomain().getCodeSource().getLocation(), SERVER_PROPERTIES_FILE_NAME).getFile();
            propertiesPath = URLDecoder.decode(propertiesPath, "UTF-8");
        }
        catch (MalformedURLException e)
        {
        }
        catch (UnsupportedEncodingException e)
        {
        }
        try {
            propertiesFile = new FileInputStream(propertiesPath);
            properties.load(propertiesFile);
        }
        catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            System.exit(1);
        }
        finally {
            if (propertiesFile != null)
            {
                try {
                    propertiesFile.close();
                }
                catch (IOException e) {}
            }
        }
        int port = 0;
        if (properties.getProperty(PORT_PROPERTY_NAME) == null) {
            System.err.println("Property 'port' was not found. Please review " + SERVER_PROPERTIES_FILE_NAME + "file.");
            System.exit(1);
        }
        try {
            port = Integer.parseInt(properties.getProperty(PORT_PROPERTY_NAME));
        }
        catch (NumberFormatException e) {
            System.err.println("Bad port format - expected number between 1000 and 65535");
            System.exit(1);
        }
        if (port < 1000 || port > 65535) {
            System.err.println("Bad port format - expected number between 1000 and 65535");
            System.exit(1);
        }

        try {
            playerSocketMap = new HashMap<Player, SocketChannel>();
            socketPlayerMap = new HashMap<SocketChannel, Player>();

            EchoWorker worker = new EchoWorker(cmListener, this);

            new Thread(worker).start();
            nioServer = new NioServer(null, port, worker);
            new Thread(nioServer).start();

/*
//test client
            Socket clientSocket = new Socket("127.0.0.1", 9970);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            writer.write("newPlayer\r\n100604643708656298753\r\n0\r\n10\r\n12");
            writer.flush();
*/

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

/*
    public void server() throws Throwable
    {
        ServerSocket ss = new ServerSocket(9070);
        while (true) {
            Socket s = ss.accept();

            InputStream is = s.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String name = br.readLine();

            String latitude = br.readLine();
            String longitude = br.readLine();
            Position position = new Position(Integer.parseInt(latitude), Integer.parseInt(longitude));
            String google_id = br.readLine();
            String probe_id = br.readLine();


            playerSocketMap.put(new Player(name, position, google_id, probe_id), s);
            System.err.println("Game client accepted\n");
            //TODO
            //processing threads for every connected player

//            new Thread(new SocketProcessor(s)).start();
            System.err.println("Game Thread ok\n");
        }
    }
*/

    @Override
    public void dispatchFlagPutEvent(Group group, Flag flag) {
        Set<Player> players = group.getPlayers();
        String isLeaderString = (true == flag.isLeaderFlag())?"1":"0";
        for (Player player : players) {
            SocketChannel socketChannel = playerSocketMap.get(player);
            nioServer.send(socketChannel, ("FlagPut\r\n"
                    + String.valueOf(flag.getPosition().getLatitude()) + "\r\n"
                    + String.valueOf(flag.getPosition().getLongitude()) + "\r\n"
                    + String.valueOf(flag.getColor().getRGB()) +"\r\n"
                    + isLeaderString + "\r\n").getBytes(Charset.forName("UTF-8")));
            //send something here
        }
    }

    @Override
    public void dispatchLFGPlayerEvent(List<Group> groups, Player player, boolean lfgState) {
        String lfgStateString = (lfgState)?"1":"0";
        for(Group group: groups)
        {
            for(Player p:group.getPlayers())
            {
                SocketChannel socketChannel = playerSocketMap.get(p);
                //LFGPlayer
                //google_id
                //name (string)
                //latitude
                //longitude
                //0|1
                nioServer.send(socketChannel, ("LFGPlayer\r\n"
                        + String.valueOf(player.getGoogleId()) + "\r\n"
                        + player.getName() + "\r\n"
                        + String.valueOf(player.getPosition().getLatitude()) + "\r\n"
                        + String.valueOf(player.getPosition().getLongitude()) + "\r\n"
                        + lfgStateString + "\r\n").getBytes(Charset.forName("UTF-8")));
            }

        }
    }

    @Override
    public void dispatchMemberPositionChanged(Group group, Player member) {
        //To change body of implemented methods use File | Settings | File Templates.
        Set<Player> players = group.getPlayers();
        for (Player player : players) {
            SocketChannel socketChannel = playerSocketMap.get(player);
            nioServer.send(socketChannel, ("TeammatePositionChanged\r\n"
                    + String.valueOf(member.getGoogleId()) + "\r\n"
                    + String.valueOf(member.getPosition().getLatitude()) + "\r\n"
                    + String.valueOf(member.getPosition().getLongitude()) + "\r\n").getBytes(Charset.forName("UTF-8")));
        }
    }

    @Override
    public void dispatchMemberScanRadiusChanged(Group group, Player member) {
        //To change body of implemented methods use File | Settings | File Templates.
        Set<Player> players = group.getPlayers();
        for (Player player : players) {
            SocketChannel socketChannel = playerSocketMap.get(player);
            nioServer.send(socketChannel, ("TeammateScanRadiusChanged\r\n"
                    + String.valueOf(member.getGoogleId()) + "\r\n"
                    //+ String.valueOf(member.getPosition().getLatitude()) + "\r\n"
                    + String.valueOf(member.getProbeProperties().getScanRadiusIndex()) + "\r\n").getBytes(Charset.forName("UTF-8")));
        }
    }

    @Override
    public void dispatchMemberScanReadyStateChanged(Group group, Player member) {
        //TeammateScanReadyStateChanged
        //google_id
        //0|1
        String scanStateString = (member.isReadyToScan())?"1":"0";
        Set<Player> players = group.getPlayers();
        for (Player player : players) {
            SocketChannel socketChannel = playerSocketMap.get(player);
            nioServer.send(socketChannel, ("TeammateScanReadyStateChanged\r\n"
                    + String.valueOf(member.getGoogleId()) + "\r\n"
                    + scanStateString + "\r\n").getBytes(Charset.forName("UTF-8")));
        }

    }

    @Override
    public void dispatchMemberLeft(Group group, Player member) {
        //TeammateLeft
        //google_id
        String scanStateString = (member.isReadyToScan())?"1":"0";
        Set<Player> players = group.getPlayers();
        for (Player player : players) {
            SocketChannel socketChannel = playerSocketMap.get(player);
            nioServer.send(socketChannel, ("TeammateLeft\r\n"
                    + String.valueOf(member.getGoogleId()) + "\r\n").getBytes(Charset.forName("UTF-8")));
        }
    }

    @Override
    public void dispatchLeaderChanged(Group group) {
        //LeaderChanged
        //google_id
        Set<Player> players = group.getPlayers();
        for (Player player : players) {
            SocketChannel socketChannel = playerSocketMap.get(player);
            nioServer.send(socketChannel, ("LeaderChanged\r\n"
                    + String.valueOf(group.getLeader().getGoogleId()) + "\r\n").getBytes(Charset.forName("UTF-8")));
        }
    }

    @Override
    public void dispatchGroupScanStarted(Group group) {
        //To change body of implemented methods use File | Settings | File Templates.
        Set<Player> players = group.getPlayers();
        for (Player player : players) {
            SocketChannel socketChannel = playerSocketMap.get(player);
            //GroupScanStarted
            nioServer.send(socketChannel, "GroupScanStarted\r\n".getBytes(Charset.forName("UTF-8")));
        }
    }

    @Override
    public void dispatchFoundSignals(Group group, List<Signal> signals) {
        //FoundSignals
        //integer (number of signals)
        //strength(int)
        //latitude
        //longitude
        //...
        Set<Player> players = group.getPlayers();
        for (Player player : players) {
            SocketChannel socketChannel = playerSocketMap.get(player);
            //GroupScanStarted
            nioServer.send(socketChannel, (
                    "FoundSignals\r\n"
                  + String.valueOf(signals.size()) + "\r\n"
                            ).getBytes(Charset.forName("UTF-8")));
            for(Signal signal: signals)
            {
                nioServer.send(socketChannel, (
                        String.valueOf(signal.getStrength()) + "\r\n"
                      + String.valueOf(signal.getPosition().getLatitude()) + "\r\n"
                      + String.valueOf(signal.getPosition().getLongitude()) + "\r\n"
                ).getBytes(Charset.forName("UTF-8")));
            }
        }
    }

    @Override
    public void sendGroupAssigned(Group group, Player newPlayer) {
        //To change body of implemented methods use File | Settings | File Templates.
        Set<Player> players = group.getPlayers();
        SocketChannel newPlayerSocketChannel = playerSocketMap.get(newPlayer);

        //GroupAssigned
        //string (group name)
        //0|1 (automaticallyAcceptingNewMembers)
        //0|1 (interestedInNewMembers)
        //google_id (leader)
        //integer (total number of players in the group with leader)

        nioServer.send(newPlayerSocketChannel, ("GroupAssigned\r\n"
                        + group.getName() + "\r\n"
                        + ((false == group.getGroupProperties().isAutomaticallyAcceptingNewMembers())?"0":"1") +"\r\n"
                        + ((false == group.getGroupProperties().isInterestedInNewMembers())?"0":"1") +"\r\n"
                        + group.getLeader().getGoogleId() + "\r\n"
                        + (players.size() - 1) + "\r\n").getBytes(Charset.forName("UTF-8")));

        for (Player player : players) {
            SocketChannel socketChannel = playerSocketMap.get(player);
            if(!player.equals(newPlayer))
            {
                //NewTeammate
                //google_id
                //nick_name
                //latitude
                //longitude
                //radius index
                //scan strength
                nioServer.send(socketChannel, ("NewTeammate\r\n"
                    + newPlayer.getGoogleId() + "\r\n"
                    + newPlayer.getName() + "\r\n"
                    + String.valueOf(newPlayer.getPosition().getLatitude()) + "\r\n"
                    + String.valueOf(newPlayer.getPosition().getLongitude()) + "\r\n"
                    + String.valueOf(newPlayer.getProbeProperties().getScanRadiusIndex()) + "\r\n"
                    + String.valueOf(newPlayer.getProbeProperties().getScanStrength()) + "\r\n").getBytes(Charset.forName("UTF-8")));
                //google_id
                //name (string)
                //latitude
                //longitude
                //radius index
                //scan strength
                nioServer.send(newPlayerSocketChannel, (
                          player.getGoogleId() + "\r\n"
                        + player.getName()+"\r\n"
                        + String.valueOf(player.getPosition().getLatitude()) + "\r\n"
                        + String.valueOf(player.getPosition().getLongitude()) + "\r\n"
                        + String.valueOf(player.getProbeProperties().getScanRadiusIndex()) + "\r\n"
                        + String.valueOf(player.getProbeProperties().getScanStrength()) + "\r\n").getBytes(Charset.forName("UTF-8")));
            }
        }
        Set<Flag> flags = group.getFlags();
        //integer (number of flags)
        nioServer.send(newPlayerSocketChannel,
                (String.valueOf(group.getFlags().size())+ "\r\n").getBytes(Charset.forName("UTF-8")));
        for (Flag flag : flags) {
            //latitude
            //longitude
            //integer (color)
            nioServer.send(newPlayerSocketChannel, (
                     String.valueOf(flag.getPosition().getLatitude()) + "\r\n"
                   + String.valueOf(flag.getPosition().getLongitude()) + "\r\n"
                   + String.valueOf(flag.getColor().getRGB()) + "\r\n"
                   + (flag.isLeaderFlag() ? "1" : "0") + "\r\n").getBytes(Charset.forName("UTF-8")));
        }
    }

    @Override
    public void sendTreasure(Player player, Treasure treasure) {
        //To change body of implemented methods use File | Settings | File Templates.
        SocketChannel playerSocketChannel = playerSocketMap.get(player);
        //TODO implement normal treasure send
        //text below should contain one line only now
        byte[] contents = new byte[treasure.getContents().remaining()];
        treasure.getContents().get(contents);
        String text = new String(contents);
        nioServer.send(playerSocketChannel, ("TreasureFound\r\n"
                + String.valueOf(treasure.getPosition().getLatitude()) + "\r\n"
                + String.valueOf(treasure.getPosition().getLongitude()) + "\r\n"
                + text + "\r\n").getBytes(Charset.forName("UTF-8")));
    }

    @Override
    public void addListener(CommunicationManagerListener listener) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void removeListener(CommunicationManagerListener listener) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void sendInvitationReceived(Player player, String groupName, Set<String> memberNames)
    {
        //InvitationReceived
        //groupName
        //memberAmount (integer)
        //name
        //...
        SocketChannel socketChannel = playerSocketMap.get(player);
        nioServer.send(socketChannel, (
                "InvitationReceived\r\n"
                        + groupName + "\r\n"
                        + String.valueOf(memberNames.size()) + "\r\n"
        ).getBytes(Charset.forName("UTF-8")));
        for(String memberName: memberNames)
        {
            nioServer.send(socketChannel, (
                    memberName + "\r\n"
            ).getBytes(Charset.forName("UTF-8")));
        }
    }

    @Override
    public void sendLeftGroup(Player player) {
        SocketChannel socketChannel = playerSocketMap.get(player);
        nioServer.send(socketChannel, (
                       "LeftGroup\r\n"
                       ).getBytes(Charset.forName("UTF-8")));
    }

    @Override
    public void sendResponse(Player player, ServerResponse response) {
        SocketChannel sc = playerSocketMap.get(player);
        nioServer.send(sc, ("Response\r\n" +
                String.valueOf(response.getErrorCode()) + "\r\n").getBytes(Charset.defaultCharset()));
    }
    @Override
    public void associatePlayerWithSocket(Player player, SocketChannel socket)
    {
        playerSocketMap.put(player, socket);
        socketPlayerMap.put(socket, player);
    }

    public Player getPlayerBySocket(SocketChannel socket)
    {
        return socketPlayerMap.get(socket);
    }

    public boolean checkPlayerInGame(Player player)
    {
        return playerSocketMap.containsKey(player);
    }
    public void drop(Player player)
    {
        SocketChannel socket = playerSocketMap.get(player);
        playerSocketMap.remove(player);
        socketPlayerMap.remove(socket);
    }

    public void dispatchFlagRemoved(Group group, Position flagPosition)
    {
        //FlagRemoved
        //latitude
        //longitude
        Set<Player> players = group.getPlayers();
        for (Player player : players) {
            SocketChannel socketChannel = playerSocketMap.get(player);
            nioServer.send(socketChannel, ("FlagRemoved\r\n"
                    + String.valueOf(flagPosition.getLatitude()) + "\r\n"
                    + String.valueOf(flagPosition.getLongitude()) + "\r\n"
                    ).getBytes(Charset.forName("UTF-8")));
        }
    }
}
