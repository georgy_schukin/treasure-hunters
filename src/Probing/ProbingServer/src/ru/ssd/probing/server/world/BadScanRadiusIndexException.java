package ru.ssd.probing.server.world;

/**
 * © Ruslan Mustakov
 */


public class BadScanRadiusIndexException extends Exception {
    public BadScanRadiusIndexException() {
        super();
    }

    public BadScanRadiusIndexException(String message) {
        super(message);
    }
}
