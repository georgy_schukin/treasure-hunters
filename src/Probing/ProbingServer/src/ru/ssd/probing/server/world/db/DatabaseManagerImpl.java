package ru.ssd.probing.server.world.db;

import org.apache.log4j.Logger;
import ru.ssd.probing.common.model.Position;
import ru.ssd.probing.common.model.Treasure;
import ru.ssd.probing.server.DBConnectionFactory;

import java.io.ByteArrayInputStream;
import java.sql.*;
import java.util.HashSet;
import java.util.Set;

/**
 * © Ruslan Mustakov
 */


public class DatabaseManagerImpl implements DatabaseManager {

    /*private static final String TREASURES_TABLE_NAME = "treasures";
    private static final String PLAYERS_TABLE_NAME = "players";
    private static final String TREASURE_ASSIGNMENTS_TABLE_NAME = "treasure_assignments";

    private static final String ID_COLUMN_NAME = "id";
    private static final String LONGITUDE_COLUMN_NAME = "longitude";
    private static final String LATITUDE_COLUMN_NAME = "latitude";
    private static final String TYPE_ID_COLUMN_NAME = "type_id";
    private static final String CONTENTS_COLUMN_NAME = "contents";
    private static final String IS_TAKEN_COLUMN_NAME = "is_taken";

    private static final String NICKNAME_COLUMN_NAME = "nick_name";

    private static final String TREASURE_ID_COLUMN_NAME = "treasure_id";
    private static final String PLAYER_ID_COLUMN_NAME = "player_id";*/

    private static final String GET_ASSIGNED_TREASURE_STATEMENT_STRING =
            "SELECT treasures.contents as contents, treasure_types.type_name as type_name\n" +
            "FROM treasure_assignments, players, treasures, treasure_types\n" +
            "WHERE treasure_assignments.player_id = players.id AND treasure_assignments.treasure_id = treasures.id AND\n" +
            "treasures.type_id = treasure_types.id AND players.nick_name = ? AND treasures.latitude = ? AND\n" +
            "treasures.longitude = ?";

    private static final String SET_TREASURE_TAKEN_STATEMENT_STRING =
            "UPDATE treasures SET is_taken = 1 WHERE latitude = ? AND longitude = ?";

    private static final String ASSIGN_TREASURE_STATEMENT_STRING =
            "INSERT INTO treasure_assignments (treasure_id, player_id) VALUES (?, ?)";

    private static final String GET_TREASURE_AND_PLAYER_ID_STATEMENT_STRING =
            "SELECT treasures.id, players.id\n" +
            "FROM treasures, players\n" +
            "WHERE players.nick_name = ? AND treasures.latitude = ? AND treasures.longitude = ?";

    private static final String GET_TREASURES_STATEMENT_STRING =
            "SELECT latitude, longitude FROM treasures WHERE is_taken = 0";

    private static final String ADD_TREASURE_STATEMENT_STRING =
            "INSERT INTO treasures (longitude, latitude, type_id, contents, is_taken) VALUES (?,?,?,?,'0')";

    private Connection connection = null;

    private PreparedStatement getAssignedTreasureStatement;
    private PreparedStatement setTreasureTakenStatement;
    private PreparedStatement assignTreasureStatement;
    private PreparedStatement getTreasureAndPlayerIdStatement;
    private PreparedStatement getTreasuresStatement;
    private PreparedStatement addTreasureStatement;

    private final Object connectionLock = new Object();

    public DatabaseManagerImpl() throws SQLException
    {
        establishConnection();
    }

    private void establishConnection() throws SQLException {
        if (connection != null) {
            try {
                connection.close();
            }
            catch (SQLException e) {}
        }
        connection = DBConnectionFactory.getConnection();
        if (connection == null) {
            throw new SQLException("Can't get connection to database");
        }
        connection.setAutoCommit(false);

        getAssignedTreasureStatement = connection.prepareStatement(GET_ASSIGNED_TREASURE_STATEMENT_STRING);
        setTreasureTakenStatement = connection.prepareStatement(SET_TREASURE_TAKEN_STATEMENT_STRING);
        assignTreasureStatement = connection.prepareStatement(ASSIGN_TREASURE_STATEMENT_STRING);
        getTreasureAndPlayerIdStatement = connection.prepareStatement(GET_TREASURE_AND_PLAYER_ID_STATEMENT_STRING);
        getTreasuresStatement = connection.prepareStatement(GET_TREASURES_STATEMENT_STRING);
        addTreasureStatement = connection.prepareStatement(ADD_TREASURE_STATEMENT_STRING);
    }

    @Override
    synchronized public Treasure getTreasureIfAssigned(String playerName, Position position) throws SQLException {
        if (connection.isClosed()) { establishConnection(); }

        getAssignedTreasureStatement.setString(1, playerName);
        getAssignedTreasureStatement.setInt(2, position.getLatitude());
        getAssignedTreasureStatement.setInt(3, position.getLongitude());

        Logger.getLogger("World").debug("Executing query: " + getAssignedTreasureStatement);

        ResultSet resultSet = getAssignedTreasureStatement.executeQuery();
        Treasure result = null;
        if (resultSet.next()) {
            Treasure.TreasureType treasureType = Treasure.TreasureType.valueOf(resultSet.getString(2));
            Blob contentsBlob = resultSet.getBlob(1);
            result = new Treasure(treasureType, contentsBlob.getBytes(1, (int)contentsBlob.length()), position);
        }
        return result;
    }

    @Override
    synchronized public void assignTreasureToGroup(Set<String> playerNames, Position position) throws SQLException {
        if (connection.isClosed()) { establishConnection(); }

        setTreasureTakenStatement.setInt(1, position.getLatitude());
        setTreasureTakenStatement.setInt(2, position.getLongitude());

        Logger.getLogger("World").debug("Executing query:\n" + setTreasureTakenStatement);

        setTreasureTakenStatement.executeUpdate();
        for (String playerName : playerNames) {
            getTreasureAndPlayerIdStatement.setString(1, playerName);
            getTreasureAndPlayerIdStatement.setInt(2, position.getLatitude());
            getTreasureAndPlayerIdStatement.setInt(3, position.getLongitude());

            Logger.getLogger("World").debug("Executing query:\n" + getTreasureAndPlayerIdStatement);

            ResultSet resultSet = getTreasureAndPlayerIdStatement.executeQuery();

            if (resultSet.next()) {
                Logger.getLogger("World").debug("getTreasureAndPlayerIdStatement returned non-empty resultSet");
                assignTreasureStatement.setInt(1, resultSet.getInt(1));
                assignTreasureStatement.setInt(2, resultSet.getInt(2));
                assignTreasureStatement.execute();
            }
            else {
                Logger.getLogger("World").debug("getTreasureAndPlayerIdStatement returned EMPTY resultSet");
            }
        }
        connection.commit();
    }

    @Override
    synchronized public Set<Position> getTreasurePositionsInRadius(Position center, int radius) throws SQLException {
        if (connection.isClosed()) { establishConnection(); }

        Set<Position> result = new HashSet<Position>();

        Logger.getLogger("World").debug("Executing query:\n" + getTreasuresStatement);

        ResultSet resultSet = getTreasuresStatement.executeQuery();
        //TODO: optimize
        Logger.getLogger("World").debug("Query finished. Iterating over results...");
        while (resultSet.next()) {
            int latitude = resultSet.getInt(1);
            int longitude = resultSet.getInt(2);
            Position position = new Position(latitude, longitude);
            if (position.getDistance(center) <= radius) {
                result.add(position);
            }
        }
        return result;
    }

    @Override
    synchronized public void addTreasureToDatabase(Treasure treasure) throws SQLException {
        if (connection.isClosed()) { establishConnection(); }

        addTreasureStatement.setInt(1, treasure.getPosition().getLongitude());
        addTreasureStatement.setInt(2, treasure.getPosition().getLatitude());
        addTreasureStatement.setInt(3, treasure.getType().ordinal());
        byte[] contents = new byte[treasure.getContents().remaining()];
        treasure.getContents().get(contents);
        addTreasureStatement.setBlob(4, new ByteArrayInputStream(contents));
        Logger.getLogger("World").debug("Executing SQL statement:\n" + addTreasureStatement);
        addTreasureStatement.executeUpdate();
        Logger.getLogger("World").debug("Commiting...");
        connection.commit();
    }
}
