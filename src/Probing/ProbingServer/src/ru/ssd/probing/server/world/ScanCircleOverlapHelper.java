package ru.ssd.probing.server.world;

import ru.ssd.probing.common.model.Position;

import java.util.List;

/**
 * © Ruslan Mustakov
 */


public interface ScanCircleOverlapHelper {
    public OverlapInfo getOverlapInfo(List<ScanCircle> circles, Position pointPosition);

    public static class OverlapInfo {
        private int overlapArea;
        private int overlapCount;

        public OverlapInfo(int overlapArea, int overlapCount) {
            this.overlapArea = overlapArea;
            this.overlapCount = overlapCount;
        }

        public int getOverlapArea() {
            return overlapArea;
        }

        public int getOverlapCount() {
            return overlapCount;
        }
    }
}
