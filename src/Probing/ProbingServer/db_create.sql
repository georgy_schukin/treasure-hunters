-- Designed for MySQL

DROP TABLE IF EXISTS treasure_assignments;
DROP TABLE IF EXISTS treasures;
DROP TABLE IF EXISTS treasure_types;
DROP TABLE IF EXISTS groups_signals;
DROP TABLE IF EXISTS groups_sessions;
DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS signals;
DROP TABLE IF EXISTS sessions;
DROP TABLE IF EXISTS players;

CREATE TABLE treasure_types(
id integer NOT NULL PRIMARY KEY,
type_name VARCHAR(8) NOT NULL UNIQUE
)
COLLATE utf8_unicode_ci;

CREATE TABLE treasures(
id integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
longitude integer NOT NULL,
latitude integer NOT NULL,
type_id integer NOT NULL REFERENCES treasure_types(id),
contents BLOB(1048576) NOT NULL, -- should be changed to dir or something...
is_taken TINYINT(1) NOT NULL,
CONSTRAINT unique_position UNIQUE (longitude, latitude)
)
COLLATE utf8_unicode_ci;

CREATE TABLE players(
id integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
nick_name VARCHAR(255) UNIQUE,
display_name VARCHAR(255) NOT NULL,
e_mail VARCHAR(255) NOT NULL UNIQUE,
probe_strength integer NOT NULL,
google_id VARCHAR(255) NOT NULL UNIQUE,
status integer NOT NULL
)
COLLATE utf8_unicode_ci;

CREATE TABLE treasure_assignments(
treasure_id integer NOT NULL REFERENCES treasures(id),
player_id integer NOT NULL REFERENCES players(id)
)
COLLATE utf8_unicode_ci;

CREATE TABLE sessions(
id integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
uuid BINARY(16) NOT NULL,
player_id integer NOT NULL REFERENCES players(id),
scan_radius integer NOT NULL,
latitude integer NOT NULL,
longitude integer NOT NULL,
is_lfg TINYINT(1)
)
COLLATE utf8_unicode_ci;

CREATE TABLE groups (
id integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(255) NOT NULL UNIQUE,
leader_session_id integer NOT NULL REFERENCES sessions(id)
)
COLLATE utf8_unicode_ci;

CREATE TABLE signals (
id integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
strength integer NOT NULL,
latitude integer NOT NULL,
longitude integer NOT NULL
)
COLLATE utf8_unicode_ci;

CREATE TABLE groups_signals (
group_id integer NOT NULL REFERENCES groups(id),
signal_id integer NOT NULL REFERENCES signals(id)
)
COLLATE utf8_unicode_ci;

CREATE TABLE groups_sessions (
group_id integer NOT NULL REFERENCES groups(id),
session_id integer NOT NULL REFERENCES sessions(id)
)
COLLATE utf8_unicode_ci;

delimiter |

CREATE TRIGGER set_session_uuid BEFORE INSERT ON sessions
FOR EACH ROW
BEGIN
  SET NEW.uuid = UUID();
END;
|
CREATE TRIGGER remove_lfg BEFORE INSERT ON groups_sessions
FOR EACH ROW
BEGIN
  UPDATE sessions SET is_lfg = 0 WHERE NEW.session_id = id;
END;
|

delimiter ;

INSERT INTO treasure_types VALUES ('0', 'SCORE');
INSERT INTO treasure_types VALUES ('1', 'TEXT');
INSERT INTO treasure_types VALUES ('2', 'IMAGE');
INSERT INTO treasure_types VALUES ('3', 'SOUND');
INSERT INTO treasure_types VALUES ('4', 'VIDEO');
