package ru.ssd.probing.common.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * © Ruslan Mustakov
 */


public class ProbeProperties {
    public static final int MAX_SCAN_STRENGTH = 100;
    public static final int SCAN_TIME = 2000; //milliseconds

    public static final int DEFAULT_SCAN_RADIUS_INDEX = 4;
    //public static final int DEFAULT_SCAN_RADIUS = 1000;
    public static final int DEFAULT_SCAN_STRENGTH = 50;
    
    private int scanRadiusIndex;
    private int scanStrength;

    public static final List<Integer> possibleRadius; //meters

    static {
        List<Integer> possibleRadiusMutable = new ArrayList<Integer>();
        //TODO: fix to values that make sense
        possibleRadiusMutable.add(50);
        possibleRadiusMutable.add(100);
        possibleRadiusMutable.add(300);
        possibleRadiusMutable.add(500);
        possibleRadiusMutable.add(1000);
        possibleRadius = Collections.unmodifiableList(possibleRadiusMutable);
    }

    public ProbeProperties() {
        this(DEFAULT_SCAN_RADIUS_INDEX, DEFAULT_SCAN_STRENGTH);
    }
    
    public ProbeProperties(int scanRadiusIndex, int scanStrength) {
        if (scanRadiusIndex < 0 || scanRadiusIndex >= possibleRadius.size()) {
            throw new IllegalArgumentException("bad scanRadiusIndex");
        }
        this.scanRadiusIndex = scanRadiusIndex;
        this.scanStrength = scanStrength;
    }

    public void setScanRadiusIndex(int scanRadiusIndex) {
        this.scanRadiusIndex = scanRadiusIndex;
    }

    public int getScanRadiusIndex() {
        return scanRadiusIndex;
    }

    public int getScanRadius() {
        return possibleRadius.get(scanRadiusIndex);
    }

    public int getScanStrength() {
        return scanStrength;
    }

    public static int maxPossibleScanRadius() {
        return possibleRadius.get(possibleRadius.size() - 1);
    }

    public static int minPossibleScanRadius() {
        return possibleRadius.get(0);
    }

    public static int maxPossibleScanRadiusIndex() {
        return possibleRadius.size() - 1;
    }
}
