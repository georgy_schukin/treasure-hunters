package ru.ssd.probing.common.model;

/**
 * © Ruslan Mustakov
 */


public class Signal {
    public enum SignalType {
        UNKNOWN,
        SCORE,
        TEXT,
        IMAGE,
        SOUND,
        VIDEO,
    }

    private int strength;
    private Position position;
    private SignalType type;

    public Signal(int strength, Position position) {
        this(strength, position, SignalType.UNKNOWN);
    }

    public Signal(int strength, Position position, SignalType type) {
        if (position == null) {
            throw new IllegalArgumentException("Position is expected to be non-null");
        }
        if (type == null) {
            type = SignalType.UNKNOWN;
        }
        this.strength = strength;
        this.position = position;
        this.type = type;
    }

    public int getStrength() {
        return strength;
    }

    public Position getPosition() {
        return position;
    }

    public SignalType getType() {
        return type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Signal) {
            Signal other = (Signal)obj;
            return (strength == other.getStrength() && position.equals(other.getPosition()) && type.equals(other.getType()));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = strength * 31;
        hash = hash * 31 + position.hashCode();
        hash = hash * 31 + type.hashCode();
        return hash;
    }
}
