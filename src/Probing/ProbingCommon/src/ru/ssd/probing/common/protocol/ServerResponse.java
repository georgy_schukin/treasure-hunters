package ru.ssd.probing.common.protocol;

/**
 * © Ruslan Mustakov
 */


public class ServerResponse {
    private int errorCode;

    public ServerResponse() {
        this.errorCode = ErrorCodes.NO_ERROR;
    }

    public int getErrorCode()
    {
        return errorCode;
    }

    public ServerResponse (int errorCode) {
        if (errorCode < 0 || errorCode > ErrorCodes.maxError()) {
            throw new IllegalArgumentException("bad error code");
        }
        this.errorCode = errorCode;
    }
    
    public boolean isOk() {
        return (errorCode == ErrorCodes.NO_ERROR);
    }
    
    public static class ErrorCodes {
        public static final int NO_ERROR = 0;
        public static final int NO_GROUP_ERROR = NO_ERROR + 1;
        public static final int NOT_LEADER_ERROR = NO_GROUP_ERROR + 1;
        public static final int NO_TREASURE_ERROR = NOT_LEADER_ERROR + 1;
        public static final int BAD_POSITION_ERROR = NO_TREASURE_ERROR + 1;
        public static final int TOO_MANY_FLAGS_ERROR = BAD_POSITION_ERROR + 1;
        public static final int NOT_GROUP_MEMBER_ERROR = TOO_MANY_FLAGS_ERROR + 1;
        public static final int BAD_SCAN_RADIUS_INDEX_ERROR = NOT_GROUP_MEMBER_ERROR + 1;
        public static final int ALREADY_IN_GROUP_ERROR = BAD_SCAN_RADIUS_INDEX_ERROR + 1;
        public static final int INNER_ERROR = ALREADY_IN_GROUP_ERROR + 1;
        public static final int ALREADY_INVITED_ERROR = INNER_ERROR + 1;
        public static final int NOT_LFG_ERROR = ALREADY_INVITED_ERROR + 1;
        public static final int NO_INVITATION_ERROR = NOT_LFG_ERROR + 1;

        private static int maxError() {
            return NOT_LFG_ERROR;
        }
    }
}
