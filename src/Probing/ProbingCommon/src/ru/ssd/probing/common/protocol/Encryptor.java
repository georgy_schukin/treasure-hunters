package ru.ssd.probing.common.protocol;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * © Ruslan Mustakov
 */


public interface Encryptor {
    public void writeEncryptedTo(byte[] message, OutputStream outputStream) throws IOException;
    public byte[] readFrom(InputStream inputStream) throws IOException;
}
