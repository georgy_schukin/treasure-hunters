package ru.ssd.probing.common.model;

import java.util.HashSet;
import java.util.Set;

/**
 * © Ruslan Mustakov
 */


public class Player implements Comparable<Player> {
    private String name;
    private String googleId;
    private String probeId;
    private ProbeProperties probeProperties;
    private Position position;
    private Set<Listener> listeners = new HashSet<Listener>();
    private boolean readyToScan;
    private boolean isLFG;
    
    public Player(String name, Position position, String googleId, String probeId) {
        this(name, position, googleId, probeId, new ProbeProperties());
    }

    public Player(String name, Position position, String googleId, String probeId, ProbeProperties probeProperties) {
        if (name == null) {
            throw new NullPointerException("Player name mustn't be null");
        }
        if (position == null) {
            throw new NullPointerException("Player position mustn't be null");
        }
        if (googleId == null) {
            throw new NullPointerException("Player google_id mustn't be null");
        }
        if (probeId == null) {
            throw new NullPointerException("Player probe_id mustn't be null");
        }
        if (probeProperties == null) {
            probeProperties = new ProbeProperties();
        }
        listeners = new HashSet<Listener>();
        this.name = name;
        this.googleId = googleId;
        this.probeId = probeId;
        this.position = position;
        this.probeProperties = probeProperties;
        this.isLFG = false;
    }

    public String getName() {
        return name;
    }

    public String getGoogleId() {
        return googleId;
    }

    public String getProbeId() {
        return probeId;
    }

    public ProbeProperties getProbeProperties() {
        return probeProperties;
    }

    public Position getPosition() {
        return position;
    }

    public boolean isReadyToScan() {
        return readyToScan;
    }

    public boolean isLFG() {
        return isLFG;
    }

    public void setLFG(boolean LFG) {
        if (isLFG != LFG) {
            isLFG = LFG;
            for (Listener listener : listeners) {
                listener.onLFGStateChanged(isLFG, this);
            }
        }
    }

    public void setReadyToScan(boolean readyToScan) {
        if (this.readyToScan != readyToScan) {
            this.readyToScan = readyToScan;
            for (Listener listener: listeners) {
                listener.onScanReadyStateChanged(readyToScan, this);
            }
        }
    }

    public void setProbeProperties(ProbeProperties probeProperties) {
        this.probeProperties = probeProperties;
        for (Listener listener : listeners) {
            listener.onProbePropertiesChanged(probeProperties, this);
        }
    }

    public void setPosition(Position position) {
        this.position = position;
        for (Listener listener : listeners) {
            listener.onPositionChanged(position, this);
        }
    }

    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    public void removeListener(Listener listener) {
        listeners.remove(listener);
    }

    @Override
    public int hashCode() {
        return googleId.hashCode();
    }

    @Override
    public int compareTo(Player otherPlayer) { // compare by Google Id !!!
        return googleId.compareTo(otherPlayer.getGoogleId());
    }

    @Override
    public boolean equals(Object obj) { // compare by Google Id !!!
        if (obj instanceof Player) {
            Player otherPlayer = (Player)obj;
            return googleId.equals(otherPlayer.getGoogleId());
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        return name;
    }

    public interface Listener {
        public void onProbePropertiesChanged(ProbeProperties newProperties, Player sender);
        public void onPositionChanged(Position newPosition, Player sender);
        public void onScanReadyStateChanged(boolean isReadyToScan, Player sender);
        public void onLFGStateChanged(boolean isLFG, Player sender);
    }
}
