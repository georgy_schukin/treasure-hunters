package ru.ssd.probing.common.model;

/**
 * © Ruslan Mustakov
 */


public class Flag {
    private Color color;
    private Position position;
    private boolean leaderFlag;

    public Flag(Color color, Position position, boolean isLeaderFlag) {
        if (color == null) {
            throw new IllegalArgumentException("Color is null");
        }
        if (position == null) {
            throw new IllegalArgumentException("Position is null");
        }
        this.color = color;
        this.position = position;
        this.leaderFlag = isLeaderFlag;
    }

    public Color getColor() {
        return color;
    }

    public Position getPosition() {
        return position;
    }

    public boolean isLeaderFlag() {
        return leaderFlag;
    }
}
