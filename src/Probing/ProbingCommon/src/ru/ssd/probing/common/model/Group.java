package ru.ssd.probing.common.model;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * © Ruslan Mustakov
 */


public class Group implements Comparable<Group> {
    private String name;
    private Set<Player> players;
    private GroupProperties properties;
    private Player leader = null;
    private Set<Flag> flags;
    
    private Set<Listener> listeners;
    
    public Group(String name) {
        this(name, null);
    }
    
    public Group(String name, Player leader) {
        this(name, leader, new GroupProperties());    
    }
    
    public Group(String name, Player leader, GroupProperties properties) {
        if (name == null) {
            throw new IllegalArgumentException("Group name mustn't be null");
        }
        this.name = name;
        if (properties != null) {
            this.properties = properties;
        }
        else {
            this.properties = new GroupProperties();
        }
        players = new HashSet<Player>();
        if (leader != null) {
            players.add(leader);
            this.leader = leader;
        }
        else {
            this.leader = null;
        }
        listeners = new HashSet<Listener>();
        flags = new HashSet<Flag>();
    }
    
    public String getName() {
        return name;
    }
    
    public Set<Player> getPlayers() {
        return new HashSet<Player>(players);
    }
    
    public Set<Player> getPlayersOrdered() {
        return new TreeSet<Player>(players);
    }
    
    public GroupProperties getGroupProperties() {
        return properties;
    }

    public Player getLeader() {
        return leader;
    }
    
    public void addPlayer(Player player) {
        if (player == null) {
            return;
        }
        players.add(player);
        for (Listener listener : listeners) {
            listener.onPlayerAdded(player, this);
        }
    }
    
    public void removePlayer(Player player) {
        if (player == null) {
            return;
        }
        if (player.equals(leader)) {
            throw new IllegalStateException("Cannot remove leader");
        }
        players.remove(player);
        for (Listener listener : listeners) {
            listener.onPlayerRemoved(player, this);
        }
    }
    
    public void changeLeader(Player newLeader) {
        if (newLeader == null) {
            throw new IllegalArgumentException("newLeader is expected to be non-null");
        }
        if (!players.contains(newLeader)) {
            throw new IllegalStateException("New leader must be one of group members");
        }
        Player oldLeader = leader;
        if ((oldLeader == null) || !oldLeader.equals(newLeader)) {
            leader = newLeader;
            for (Listener listener : listeners) {
                listener.onLeaderChanged(newLeader, oldLeader, this);
            }
        }
    }

    public void setProperties(GroupProperties properties) {
        this.properties = properties;
        for (Listener listener : listeners) {
            listener.onPropertiesChanged(properties, this);
        }
    }
    
    public void addListener(Listener listener) {
        listeners.add(listener);
    }
    
    public void removeListener(Listener listener) {
        listeners.remove(listener);
    }

    public void addFlag(Flag flag) {
        if (flag != null) {
            boolean flagAdded = flags.add(flag);
            if (flagAdded) {
                for (Listener listener : listeners) {
                    listener.onFlagAdded(flag, this);
                }
            }
        }
    }

    public void removeFlag(Flag flag) {
        if (flag != null) {
            boolean flagRemoved = flags.remove(flag);
            if (flagRemoved) {
                for (Listener listener : listeners) {
                    listener.onFlagRemoved(flag, this);
                }
            }
        }
    }

    public Set<Flag> getFlags() {
        return new HashSet<Flag>(flags);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public int compareTo(Group otherGroup) {
        return name.compareTo(otherGroup.getName());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Group) {
            Group otherGroup;
            otherGroup = (Group)obj;
            return name.equals(otherGroup.getName());
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        return name;
    }

    public interface Listener {
        public void onLeaderChanged(Player newLeader, Player oldLeader, Group sender);
        public void onPlayerAdded(Player player, Group sender);
        public void onPlayerRemoved(Player player, Group sender);
        public void onPropertiesChanged(GroupProperties newProperties, Group sender);
        public void onFlagAdded(Flag flag, Group sender);
        public void onFlagRemoved(Flag flag, Group sender);
    }
}
