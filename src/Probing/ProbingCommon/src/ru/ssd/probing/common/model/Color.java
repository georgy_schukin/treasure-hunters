package ru.ssd.probing.common.model;

/**
 * © Ruslan Mustakov
 */


public class Color {
    private int rgb;

    public Color(int rgb) {
        this.rgb = rgb;
    }

    public Color(int red, int green, int blue) {
        rgb = blue & 0xFF | (green << 8) & 0xFF00 | (red << 16) & 0xFF0000;
    }

    public int getRed() {
        return (rgb >> 16) & 0xFF;
    }

    public int getGreen() {
        return (rgb >> 8) & 0xFF;
    }

    public int getBlue() {
        return rgb & 0xFF;
    }

    public int getRGB() {
        return rgb;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Color) {
            return (rgb == ((Color) obj).getRGB());
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "#" + Integer.toHexString(rgb);
    }

    public static final Color BLACK = new Color(0, 0, 0);
    public static final Color WHITE = new Color(0xFF, 0xFF, 0xFF);
    public static final Color RED = new Color(0xFF, 0, 0);
    public static final Color GREEN = new Color(0, 0xFF, 0);
    public static final Color BLUE = new Color(0, 0, 0xFF);
    public static final Color PINK = new Color(0xFF, 0xC0, 0xCB);
    public static final Color MAGENTA = new Color(0xFF, 0x00, 0xFF);
    public static final Color BROWN = new Color(0xA5, 0x2A, 0x2A);
    public static final Color AQUA = new Color(0x00, 0xFF, 0xFF);
    public static final Color GRAY = new Color(0xA9, 0xA9, 0xA9);
    public static final Color ORANGE = new Color(0xFF, 0xA5, 0x00);
    public static final Color SILVER = new Color(0xC0, 0xC0, 0xC0);
}
