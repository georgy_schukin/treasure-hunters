package ru.ssd.probing.common.model;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * © Ruslan Mustakov
 */


public class Treasure {
    public enum TreasureType {
        SCORE,
        TEXT,
        IMAGE,
        SOUND,
        VIDEO
    }

    private TreasureType type;
    private ByteBuffer contents;
    private Position position;

    public Treasure(TreasureType type, byte[] contents, Position position) {
        if (type == null) {
            throw new IllegalArgumentException("Type is expected to be non-null");
        }
        if (contents == null) {
            throw new IllegalArgumentException("Contents are expected to be non-null");
        }
        this.type = type;
        this.contents = ByteBuffer.wrap(Arrays.copyOf(contents, contents.length)).asReadOnlyBuffer();
        this.position = position;
    }
    
    public Treasure(int score, Position position) {
        this.type = TreasureType.SCORE;
        this.position = position;
        ByteBuffer scoreBuffer = ByteBuffer.allocate(4);
        scoreBuffer.putInt(score);
        scoreBuffer.rewind();
        contents = scoreBuffer.asReadOnlyBuffer();
    }

   /* public Treasure(RenderedImage image) {
        if (image == null) {
            throw new IllegalArgumentException("Image is expected to be non-null");
        }
        this.type = TreasureType.IMAGE;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, "png", outputStream);
        }
        catch (IOException e) {
            //never thrown by ByteArrayOutputStream
        }
        contents = ByteBuffer.wrap(outputStream.toByteArray()).asReadOnlyBuffer();
    }  */
    
    public Treasure(String text, Position position) {
        if (text == null) {
            throw new IllegalArgumentException("Text is expected to be non-null");
        }
        this.type = TreasureType.TEXT;
        this.position = position;
        try {
            contents = ByteBuffer.wrap(text.getBytes("UTF-8")).asReadOnlyBuffer();
        }
        catch (UnsupportedEncodingException e) {
            //UTF-8 is always supported
        }
    }

    public TreasureType getType() {
        return type;
    }

    public ByteBuffer getContents() {
        return contents;
    }

    public Position getPosition() {
        return position;
    }
}
