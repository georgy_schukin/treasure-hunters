package ru.ssd.probing.common.model;

/**
 * © Ruslan Mustakov
 */


public class GroupProperties {
    private boolean automaticallyAcceptingNewMembers;
    private boolean interestedInNewMembers;

    public GroupProperties() {
        this(true, true);
    }

    public GroupProperties(boolean automaticallyAcceptingNewMembers, boolean interestedInNewMembers) {
        this.automaticallyAcceptingNewMembers = automaticallyAcceptingNewMembers;
        this.interestedInNewMembers = interestedInNewMembers;
    }

    public boolean isAutomaticallyAcceptingNewMembers() {
        return automaticallyAcceptingNewMembers;
    }

    public boolean isInterestedInNewMembers() {
        return interestedInNewMembers;
    }
}
